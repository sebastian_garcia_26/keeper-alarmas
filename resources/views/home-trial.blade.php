@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div>
            <div class="clearfix"></div>
            <div class="bs-example" data-example-id="simple-jumbotron">
                <div class="jumbotron">
                    <h1>Aviso Importante!</h1>
                    <p>El período de prueba ha caducado, aunque no se pueda seguir utilizando el sistema sus datos estan seguros.</p>
                </div>
            </div>
            
        </div>
        
    </div>
    <!-- /page content -->

    
@endsection