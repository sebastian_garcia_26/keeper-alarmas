@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de Abonos Mensuales</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('abonos/create')}}"><i class="fa fa-plus"></i> Crear Abono Mensual</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Buscar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="GET" class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <input type="text" name="name" class="form-control" value="{{$request->get('name')}}" id="name" placeholder="Descripción">
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary reset-btn" type="reset"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Resultados ({{$abonos->total()}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Descripci&oacute;n </th>
                                <th class="column-title">Importe </th>
                                <th class="column-title">Importe adicional por 1º vencimiento </th>
                                <th class="column-title">Importe adicional por 2º vencimiento </th>
                                <th class="column-title">Al&iacute;cuota IVA </th>
                                <th class="column-title no-link last"><span class="nobr"><i class="fa fa-cog" aria-hidden="true"></i></span></th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($abonos))
                            @foreach($abonos as $key => $abono)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" "><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> {{ $abono->name }}</td>
                                <td class=" ">
                                    <div class="badge">$ {{ $abono->price }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-purple">$ {{ $abono->price_vencimiento_1 }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-orange">$ {{ $abono->price_vencimiento_2 }}</div>
                                </td>
                                <td class=" ">{{ $abono->iva }}</td>
                                <td class=" last">
                                    <a class="btn btn-primary btn-xs" href="{{URL::to('abonos/'.$abono->id.'/edit')}}"><i class="fa fa-edit"></i></a>
                                    @if($abono->id != 1)
                                    <a class="btn btn-danger btn-xs delete-modal-trigger" data-abono="{{$abono->id}}" href="#"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="5">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif




                        </tbody>
                    </table>
                </div>
                @if(count($abonos))
                <div class="pull-right">
                    {{ $abonos->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>

</div>
<!-- /page content -->

@include('abonos/partials/modalDelete')
@endsection
@push('scripts')
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $abono = $(this).data('abono');
        $modalObj.find('#btnDelete').data('abono', $abono);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $abono = $this.data('abono');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("abonos")}}/' + $abono,
            data: {
                id: $abono,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });
</script>
@endpush