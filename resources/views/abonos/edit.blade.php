@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Editar Abono Mensual</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('abonos')}}"><i class="fa fa-list"></i> &nbsp;Ver Listado</a>
                <a class="btn btn-primary" href="{{URL::to('abonos/create')}}"><i class="fa fa-plus"></i> &nbsp;Crear Abono Mensual</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>    
            </div>    
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>    
            </div>    
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                    {{ Form::model($abono, array('route' => array('abonos.update', $abono->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal form-label-left input_mask')) }}
                            

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Descripci&oacute;n <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="name" name="name" required="required" value="{{ $abono->name }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Importe <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="price" name="price" required="required" value="{{ $abono->price }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price_vencimiento_1">Importe por 1º vencimiento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="price_vencimiento_1" name="price_vencimiento_1" required="required" value="{{ $abono->price_vencimiento_1 }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price_vencimiento_2">Importe por 2º vencimiento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="price_vencimiento_2" name="price_vencimiento_2" required="required" value="{{ $abono->price_vencimiento_2 }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="iva" class="control-label col-md-3 col-sm-3 col-xs-12">Al&iacute;cuota IVA</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="iva" class="form-control col-md-7 col-xs-12" type="text" value="{{ $abono->iva }}" name="iva">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $abono->id }}">
                            <input type="hidden" name="delete-image" id="delete-image" value="0" />
                        {{ Form::close() }}
                    </div>
                      
                </div>
            </div>  
        </div>    


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>
$('#removeImage').on('click', function (e){
        var $this = $(this);
            $this.hide();
            $('#delete-image').val(1);
            $('#img-thumbnail').hide();
        
});

</script> 
@endpush