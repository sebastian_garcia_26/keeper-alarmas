@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de Artículos</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('articulos/create')}}"><i class="fa fa-plus"></i> Crear Artículo</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Buscar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="GET" class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="col-xs-6 form-group">
                                <input type="text" name="code" class="form-control" value="{{$request->get('code')}}" id="code" placeholder="Código">
                            </div>
                            <div class="col-xs-6 form-group">
                                <input type="text" name="name" class="form-control" value="{{$request->get('name')}}" id="name" placeholder="Detalle">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary reset-btn" type="reset"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Resultados ({{$articulos->total()}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">C&oacute;digo </th>
                                <th class="column-title">Detalle </th>
                                <th class="column-title">Importe </th>
                                <th class="column-title">Al&iacute;cuota IVA </th>
                                <th class="column-title no-link last"><span class="nobr"><i class="fa fa-cog" aria-hidden="true"></i></span></th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($articulos))
                            @foreach($articulos as $key => $articulo)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $articulo->code }}</div>
                                </td>
                                <td class=" "><i class="fa fa-product-hunt"></i> {{ $articulo->name }}</td>
                                <td class=" ">
                                    <div class="badge">$ {{ $articulo->price }}</div>
                                </td>
                                <td class=" ">{{ $articulo->iva }}</td>
                                <td class=" last">
                                    <a class="btn btn-primary btn-xs" href="{{URL::to('articulos/'.$articulo->id.'/edit')}}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger btn-xs delete-modal-trigger" data-articulo="{{$articulo->id}}" href="#"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="5">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif




                        </tbody>
                    </table>
                </div>
                @if(count($articulos))
                <div class="pull-right">
                    {{ $articulos->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>

</div>
<!-- /page content -->
@include('articulos/partials/modalDelete')
@endsection
@push('scripts')
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $articulo = $(this).data('articulo');
        $modalObj.find('#btnDelete').data('articulo', $articulo);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $articulo = $this.data('articulo');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("articulos")}}/' + $articulo,
            data: {
                id: $articulo,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });
</script>
@endpush