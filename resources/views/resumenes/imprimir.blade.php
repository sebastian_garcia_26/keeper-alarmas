@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Impresión resúmenes abonados x mes</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('resumenes')}}"><i class="fa fa-list"></i> Ver Listado</a>
        </div>

    </div>
    @if (count($errors) > 0)
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4><i class="fa fa-warning"></i> Error</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    @if (session()->has('info'))
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-info alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-info-circle"></i>
                {{ session()->get('info') }}
            </div>
        </div>
    </div>
    @endif

    @if (session()->has('message'))
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-check-circle-o"></i>
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
    @endif



    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Datos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    {{Form::open(array('url' => '/resumenes/imprimir', 'files' => true, 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
                            <label>Mes *</label>
                            <select class="form-control" name="mes" id="mes">
                                <option value="">- Seleccione un Mes -</option>
                                <option value="01" @if(old('mes')=="01" || isset($request) && $request->get('mes') == "01") selected @endif>Enero</option>
                                <option value="02" @if(old('mes')=="02" || isset($request) && $request->get('mes') == "02") selected @endif>Febrero</option>
                                <option value="03" @if(old('mes')=="03" || isset($request) && $request->get('mes') == "03") selected @endif>Marzo</option>
                                <option value="04" @if(old('mes')=="04" || isset($request) && $request->get('mes') == "04") selected @endif>Abril</option>
                                <option value="05" @if(old('mes')=="05" || isset($request) && $request->get('mes') == "05") selected @endif>Mayo</option>
                                <option value="06" @if(old('mes')=="06" || isset($request) && $request->get('mes') == "06") selected @endif>Junio</option>
                                <option value="07" @if(old('mes')=="07" || isset($request) && $request->get('mes') == "07") selected @endif>Julio</option>
                                <option value="08" @if(old('mes')=="08" || isset($request) && $request->get('mes') == "08") selected @endif>Agosto</option>
                                <option value="09" @if(old('mes')=="09" || isset($request) && $request->get('mes') == "09") selected @endif>Septiembre</option>
                                <option value="10" @if(old('mes')=="10" || isset($request) && $request->get('mes') == "10") selected @endif>Octubre</option>
                                <option value="11" @if(old('mes')=="11" || isset($request) && $request->get('mes') == "11") selected @endif>Noviembre</option>
                                <option value="12" @if(old('mes')=="12" || isset($request) && $request->get('mes') == "12") selected @endif>Diciembre</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Año *</label>
                            <input type="text" name="anio" value="@if(!isset($request)){{ old('anio') }}@else{{$request->get('anio')}}@endif" id="anio" class="form-control">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>&nbsp;</label>
                            <div class="checkbox">
                                <label class="">
                                    <div class="icheckbox_flat-green" style="position: relative;"><input class="flat" @if(old('con_iva') || isset($request) && $request->get('con_iva')) checked @endif name="con_iva" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> <b>Clientes con IVA</b> <small>(Únicamente generará listado de resúmenes de clientes con IVA si esta marcado)</small>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Tipo de resumen</label>
                            <select name="venta" id="venta" class="form-control">
                                <!-- <option value=""> - Tipo de resumen - </option> -->
                                <option value="0" @if(old('venta')=="0" || isset($request) && $request->get('venta') == "0") selected @endif> Liquidación mensual </option>
                                <!-- <option value="1" @if(old('venta')=="1" || isset($request) && $request->get('venta') == "1") selected @endif> Venta / Novedad </option> -->
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Estado</label>
                            <select name="pagado" id="pagado" class="form-control">
                                <option value=""> - Estado - </option>
                                <option value="1" @if($request->get('pagado') == '1') selected @endif> Pagado </option>
                                <option value="0" @if($request->get('pagado') == '0') selected @endif> No pagado </option>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Abono Mensual *</label>
                            <select name="abono_id" id="abono_id" class="form-control">
                                <option value=""> - Seleccione una opción -</option>
                                @foreach(App\Abono::all() as $abono)
                                <option value="{{ $abono->id }}" @if($request->get('abono_id') ==$abono->id) selected @endif>{{ $abono->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Buscar</button>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
    @if(isset($request))
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Resúmenes ({{count($resumenes)}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {{Form::open(array('url' => '/resumenes/imprimir-preview', 'files' => true, 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask', 'target' => '_blank' ))}}
                    @if($request->get('con_iva'))
                    <input name="iva" value="1" type="hidden" />
                    @else
                    <input name="iva" value="0" type="hidden" />
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">
                                    <div class="checkbox">
                                        <label class="">
                                            <div class="icheckbox_flat-green" style="position: relative;"><input class="flat" checked name="todos-check" id="todos-check" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Todos / Nada
                                        </label>
                                    </div>
                                </th>
                                <th class="column-title">Nro. Resumen </th>
                                <th class="column-title">Cuenta Abonado </th>
                                <th class="column-title">Titular </th>
                                <th class="column-title">Tipo </th>
                                <th class="column-title">Fecha </th>
                                <th class="column-title">Mes </th>
                                <th class="column-title">Total </th>
                                <th class="column-title">1º Venc.</th>
                                <th class="column-title">2º Venc.</th>
                                <th class="column-title">Pagado </th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($resumenes))
                            @foreach($resumenes as $key => $resumen)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td>
                                    <div class="checkbox">
                                        <label class="">
                                            <div class="icheckbox_flat-green" style="position: relative;"><input class="flat checkbox-lista" checked name="check[]" value="{{ $resumen->id }}" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        </label>
                                    </div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $resumen->id }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue">@if($resumen->cliente){{ $resumen->cliente->cuenta_abonado }} @else - @endif</div>
                                </td>
                                <td class=" "><i class="fa fa-user" aria-hidden="true"></i> @if($resumen->cliente){{ $resumen->cliente->titular_responsable }} @else - @endif</td>
                                <td class=" ">
                                    <div class="badge bg-green">Cliente<br />Abonado</div>
                                </td>
                                <td class=" "><i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($resumen->fecha)) }}</td>
                                <td class="text-uppercase">
                                    @if(date('m', strtotime($resumen->fecha)) =='01')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Enero
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='02')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Febrero
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='03')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Marzo
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='04')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Abril
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='05')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Mayo
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='06')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Junio
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='07')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Julio
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='08')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Agosto
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='09')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Septiembre
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='10')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Octubre
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='11')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Noviembre
                                    @endif
                                    @if(date('m', strtotime($resumen->fecha)) =='12')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Diciembre
                                    @endif
                                </td>
                                <td class=" ">
                                    <div class="badge">${{ $resumen->monto }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-purple">${{ $resumen->monto_1_vencimento }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-orange">${{ $resumen->monto_2_vencimento }}</div>
                                </td>
                                <td class=" ">@if($resumen->pagado) <div class="badge bg-green">Si</div> @else <div class="badge bg-red">No</div>@endif</td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen resúmenes para imprimir en la fecha seleccionada.</div>
                                </td>
                            </tr>
                            @endif




                        </tbody>
                    </table>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="pull-right">
                                @if(count($resumenes) > 0)
                                <button type="submit" class="btn btn-success"><i class="fa fa-print"></i> Imprimir</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>

            </div>

        </div>

    </div>
    @endif

</div>
<!-- /page content -->
@endsection
@push('scripts')
<script>
    $('#todos-check').on('ifUnchecked', function() {
        $('.checkbox-lista').iCheck('uncheck');
    });

    $('#todos-check').on('ifChecked', function() {
        $('.checkbox-lista').iCheck('check');
    });
</script>
@endpush