@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
    <style>
        .special-opt {
            font-size: 14px;
        }
    </style>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Crear Venta</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('resumenes')}}"><i class="fa fa-list"></i> Ver Listado</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>    
            </div>    
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                        @if(session()->has('id'))
                            <a class="btn btn-warning btn-xs" href="{{URL::to('resumenes/'. session()->get('id') .'/pdf')}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a>
                            <a class="btn btn-primary btn-xs" href="{{URL::to('resumenes/'.session()->get('id').'/imprimir')}}" target="_blank"><i class="fa fa-print"></i> Imprimir</a>
                        @endif
                    </div>
                </div>    
            </div>    
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        {{Form::open(array('url' => '/resumenes', 'files' => true, 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Fecha *</label>
                                <input value="{{ old('fecha') }}" type="text" class="form-control has-feedback-left" required="required" id="fecha" name="fecha"  aria-describedby="fechaIngreso">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaIngreso" class="sr-only">(success)</span>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Primer Vencimiento *</label>
                                <input value="{{ old('primer_vencimiento') }}" type="text" class="form-control has-feedback-left" required="required" id="primer_vencimiento" name="primer_vencimiento"  aria-describedby="primerVencimiento">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="primerVencimiento" class="sr-only">(success)</span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Segundo Vencimiento *</label>
                                <input value="{{ old('segundo_vencimiento') }}" type="text" class="form-control has-feedback-left" required="required" id="segundo_vencimiento" name="segundo_vencimiento"  aria-describedby="segundoVencimiento">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="segundoVencimiento" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                                <label>Cliente</label>
                                <select name="cliente_id" id="cliente_id" class="selectpicker form-control" data-live-search="true" title="- Seleccione un Cliente -">
                                    @foreach($clientes as $cliente)
                                    <option @if(old('cliente_id') == $cliente->id) selected @endif class="special-opt" value="{{$cliente->id}}" data-tokens="{{$cliente->cuenta_abonado}} {{$cliente->titular_responsable}}" data-content="<b>Titular: </b>{{$cliente->titular_responsable}}, <b>Cuenta Abonado: </b>{{$cliente->cuenta_abonado}}"><b>Titular: </b>{{$cliente->titular_responsable}}, <b>Cuenta Abonado: </b>{{$cliente->cuenta_abonado}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div> 
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <label>Observaciones</label>
                                <input value="{{ old('observacion') }}" type="text" name="observacion" id="observacion" class="form-control" >
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div> 
                        
                        <div class="row" >
                            <div class="col-md-10 col-sm-10 col-xs-12 has-feedback">
                                <label>Artículos/Productos</label><br>
                                <select name="articulo" id="articulo" class="selectpicker " data-live-search="true" title="- Seleccione un Artículo -">
                                    @foreach($articulos as $articulo)
                                    <option class="special-opt" value="{{$articulo->id}}" data-tokens="{{$articulo->code}} {{$articulo->name}}" data-content="<b>Artículo: </b>{{$articulo->name}}, <b>Código: </b>{{$articulo->code}}"><b>Artículo: </b>{{$articulo->name}}, <b>Código: </b>{{$articulo->code}}</option>
                                    @endforeach
                                </select>
                                <div id="btn-agregar-articulo" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <table class="table table-hover" style="margin-top:10px;">
                                    <col width="10%">
                                    <col width="50%">
                                    <col width="20%">
                                    <col width="20%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Detalle</th>
                                            <th>Precio $</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="articulos-table-holder">
                                        @if(old('detalle'))
                                        @foreach(old('detalle') as $i => $detalle)
                                            <tr>
                                                <td><input class="form-control cantidad" type="text" name="cantidad[]" value="{{old('cantidad.'.$i)}}"/></td>
                                                <td><input class="form-control" type="text" name="detalle[]" value="{{old('detalle.'.$i)}}" readonly/></td>
                                                <td><input class="form-control monto" type="text" name="monto[]" value="{{old('monto.'.$i)}}" readonly/></td>
                                                <td><div class="btn btn-danger btn-sm borrar-articulo"><i class="fa fa-trash"></i> Eliminar</div><input class="artid" type="hidden" name="artid[]" value="{{old('artid.'.$i)}}"/></td>
                                            </tr>
                                        @endforeach
                                        @endif
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="font-size: 16px !important;">Total: <b >$ <span id="total-resumen">0</span></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Pago *</label>
                                
                                <div class="radio">
                                    <label class="">
                                        <div class="iradio_flat-green" style="position: relative;"><input class="flat pagado" @if(old('pagado') == '1') checked @endif  name="pagado" value="1" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> <b>Pagado</b> 
                                        <small>(Si se paga la venta con un solo pago en el momento de cargarla)</small>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="">
                                        <div class="iradio_flat-green" style="position: relative;"><input class="flat pagado" @if(old('pagado') == '0') checked @endif name="pagado" value="0" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> <b>No pagado</b>
                                        <small>(Se procede a crear un recibo y se lo asocia al resumen en caso de que el monto abonado no cubra el total a pagar)</small>
                                    </label>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div id="pago-info"  @if(old('pagado') != '1') class="hide" @endif>
                            <div class="row" >
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                    <label>Fecha del pago</label>
                                    <input value="{{ old('fecha_pago') }}" type="text" class="form-control has-feedback-left" required="required" id="fecha_pago" name="fecha_pago"  aria-describedby="fechaPago">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="fechaPago" class="sr-only">(success)</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Comentarios</label>
                                    <input value="{{ old('observaciones_pago') }}" type="text" name="observaciones_pago" id="observaciones_pago" class="form-control" >
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                    <label>Pago en efectivo</label>
                                    <input value="{{ old('monto_efectivo') }}" type="text" name="monto_efectivo" id="monto_efectivo" class="form-control" >
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                    <label>Pago con debito</label>
                                    <input value="{{ old('monto_debito') }}" type="text" name="monto_debito" id="monto_debito" class="form-control" >
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                    <label>Pago con transferencia bancaria</label>
                                    <input value="{{ old('monto_cheque') }}" type="text" name="monto_cheque" id="monto_cheque" class="form-control" >
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                                    <label>Pago con tarjeta</label>
                                    <input value="{{ old('monto_tarjeta') }}" type="text" name="monto_tarjeta" id="monto_tarjeta" class="form-control" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{ Form::close() }}
                    </div>
                      
                </div>
            </div>  
        </div>    


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>
var articulosJson = '<?= addcslashes(json_encode($articulosArr), "'"); ?>',
    articulos = JSON.parse(articulosJson) || $.parseJSON(articulosJson)
    montoTotal = 0;
$('.selectpicker').selectpicker();

$('.monto').each(function(){
    montoTotal += parseFloat($(this).val());
});
$('#total-resumen').text(montoTotal);

$("#fecha, #primer_vencimiento, #segundo_vencimiento, #fecha_pago").daterangepicker({
    singleDatePicker: !0,
    singleClasses: "picker_3",
    locale: {
        format: "DD/MM/YYYY",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "May",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(a, b, c) {
    console.log(a.toISOString(), b.toISOString(), c)
});

$('#btn-agregar-articulo').on('click', function () {
    var articuloID = $('#articulo').val();
    var nuevoArticulo = '<tr>';
    nuevoArticulo += '<td><input class="form-control cantidad" type="text" name="cantidad[]" value="1"/></td>';
    nuevoArticulo += '<td><input readonly class="form-control" type="text" name="detalle[]" value="'+articulos[articuloID].name+'"/></td>';
    nuevoArticulo += '<td><input readonly class="form-control monto" type="text" name="monto[]" value="'+articulos[articuloID].price+'"/></td>';
    nuevoArticulo += '<td><div class="btn btn-danger btn-sm borrar-articulo"><i class="fa fa-trash"></i> Eliminar</div><input class="artid" type="hidden" name="artid[]" value="'+articuloID+'"/></td>';
    nuevoArticulo += '</tr>';
    $('#articulos-table-holder').prepend(nuevoArticulo); 
    var nuevoMonto = parseFloat($('#total-resumen').text()) + parseFloat(articulos[articuloID].price);
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on("click",".borrar-articulo", function(e){ 
    e.preventDefault();
    var nuevoMonto = parseFloat($('#total-resumen').text()) - parseFloat($(this).closest('tr').find('input.monto').val());
    $(this).closest('tr').remove(); 
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on('keydown','input.monto, input.cantidad', function (event){
    if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
});

$('#articulos-table-holder').on('keyup','input.monto', function (event){
    var nuevoMonto = 0;
    $('.monto').each(function(){
        nuevoMonto += parseFloat($(this).val());
    });
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on('keyup','input.cantidad', function (event){
    var nuevoMonto = 0;
    var actualizarMonto = $(this).closest('tr').find('input.monto');
    var artID =  $(this).closest('tr').find('input.artid').val();
    if(artID){
        actualizarMonto.val(parseFloat($(this).val()) * parseFloat(articulos[artID].price));
    } else {
        actualizarMonto.val(parseFloat(actualizarMonto.val()) * parseFloat($(this).val()));
    }
    $('.monto').each(function(){
        nuevoMonto += parseFloat($(this).val());
    });
    $('#total-resumen').text(nuevoMonto);
});


$('input[name=pagado]').on('ifChecked', function (){
    var esPagado = $('input[name=pagado]:checked').val();
    if(esPagado == '1'){
        $('#pago-info').removeClass('hide');
    }else{
        $('#pago-info').addClass('hide');
    }
});


</script> 
@endpush