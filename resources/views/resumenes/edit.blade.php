@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
    <style>
        .special-opt {
            font-size: 14px;
        }
    </style>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Editar Venta Abonado</h3>
            </div>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('resumenes/create')}}"><i class="fa fa-plus"></i> Crear Venta</a>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('resumenes')}}"><i class="fa fa-list"></i> Ver Listado</a>
            </div>
            
        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>    
            </div>    
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>    
            </div>    
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        {{Form::model($resumen, array('route' => array('resumenes.update', $resumen->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                        <input type="hidden" name="id" value="{{$resumen->id}}" />
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <span>NRO. DE RESUMEN:  <b style="font-size:18px !important;  font-weight: bold;">{{$resumen->id}}</b></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Fecha *</label>
                                <input value="{{ date_format(\DateTime::createFromFormat("Y-m-d", trim($resumen->fecha)), 'd/m/Y') }}" type="text" class="form-control has-feedback-left" required="required" id="fecha" name="fecha"  aria-describedby="fechaIngreso">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaIngreso" class="sr-only">(success)</span>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Primer Vencimiento *</label>
                                <input value="{{ date_format(\DateTime::createFromFormat("Y-m-d", trim($resumen->primer_vencimiento)), 'd/m/Y') }}" type="text" class="form-control has-feedback-left" required="required" id="primer_vencimiento" name="primer_vencimiento"  aria-describedby="primerVencimiento">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="primerVencimiento" class="sr-only">(success)</span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Segundo Vencimiento *</label>
                                <input value="{{ date_format(\DateTime::createFromFormat("Y-m-d", trim($resumen->segundo_vencimiento)), 'd/m/Y') }}" type="text" class="form-control has-feedback-left" required="required" id="segundo_vencimiento" name="segundo_vencimiento"  aria-describedby="segundoVencimiento">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="segundoVencimiento" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                                <label>Cliente</label>
                                <select name="cliente_id" id="cliente_id" class="selectpicker form-control" data-live-search="true" title="- Seleccione un Cliente -">
                                    @foreach($clientes as $cliente)
                                    <option @if($resumen->cliente_id == $cliente->id) selected @endif class="special-opt" value="{{$cliente->id}}" data-tokens="{{$cliente->cuenta_abonado}} {{$cliente->titular_responsable}}" data-content="<b>Titular: </b>{{$cliente->titular_responsable}}, <b>Cuenta Abonado: </b>{{$cliente->cuenta_abonado}}"><b>Titular: </b>{{$cliente->titular_responsable}}, <b>Cuenta Abonado: </b>{{$cliente->cuenta_abonado}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div> 
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <label>Observaciones</label>
                                <input value="{{ $resumen->observacion }}" type="text" name="observacion" id="observacion" class="form-control" >
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div> 
                        
                        <div class="row" >
                            <div class="col-md-10 col-sm-10 col-xs-12 has-feedback">
                                <label>Artículos/Productos</label><br>
                                <select name="articulo" id="articulo" class="selectpicker " data-live-search="true" title="- Seleccione un Artículo -">
                                    @foreach($articulos as $articulo)
                                    <option class="special-opt" value="{{$articulo->id}}" data-tokens="{{$articulo->code}} {{$articulo->name}}" data-content="<b>Artículo: </b>{{$articulo->name}}, <b>Código: </b>{{$articulo->code}}"><b>Artículo: </b>{{$articulo->name}}, <b>Código: </b>{{$articulo->code}}</option>
                                    @endforeach
                                </select>
                                <div id="btn-agregar-articulo" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <table class="table table-hover" style="margin-top:10px;">
                                    <col width="10%">
                                    <col width="50%">
                                    <col width="20%">
                                    <col width="20%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Detalle</th>
                                            <th>Precio $</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="articulos-table-holder">
                                        @if(count($resumen->lineas()))
                                        @foreach($resumen->lineas() as $linea)
                                            <tr>
                                                <td><input class="form-control cantidad" type="text" name="cantidad[]" value="{{$linea->cantidad}}"/></td>
                                                <td><input class="form-control" type="text" name="detalle[]" value="{{$linea->descripcion}}" readonly/></td>
                                                <td><input class="form-control monto" type="text" name="monto[]" value="{{$linea->monto}}" readonly/></td>
                                                <td><div class="btn btn-danger btn-sm borrar-articulo"><i class="fa fa-trash"></i> Eliminar</div><input class="artid"  type="hidden" name="artid[]" @if($linea->articulo) value="{{$linea->articulo->id}}" @endif /></td>
                                            </tr>
                                        @endforeach
                                        @endif
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="font-size: 16px !important;">Total: <b >$ <span id="total-resumen">0</span></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-warning" href="{{URL::to('resumenes/'. $resumen->id .'/pdf')}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary" href="{{URL::to('resumenes/'.$resumen->id.'/imprimir')}}" target="_blank"><i class="fa fa-print"></i> Imprimir</a>
                                </div>
                          
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{ Form::close() }}
                    </div>
                      
                </div>
            </div>  
        </div>    


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>
var articulosJson = '<?= addcslashes(json_encode($articulosArr), "'"); ?>',
    articulos = JSON.parse(articulosJson) || $.parseJSON(articulosJson)
    montoTotal = 0;
$('.selectpicker').selectpicker();

$('.monto').each(function(){
    montoTotal += parseFloat($(this).val());
});
$('#total-resumen').text(montoTotal);

$("#fecha, #primer_vencimiento, #segundo_vencimiento, #fecha_pago").daterangepicker({
    singleDatePicker: !0,
    singleClasses: "picker_3",
    locale: {
        format: "DD/MM/YYYY",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "May",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(a, b, c) {
    console.log(a.toISOString(), b.toISOString(), c)
});

$('#btn-agregar-articulo').on('click', function () {
    var articuloID = $('#articulo').val();
    var nuevoArticulo = '<tr>';
    nuevoArticulo += '<td><input class="form-control cantidad" type="text" name="cantidad[]" value="1"/></td>';
    nuevoArticulo += '<td><input readonly class="form-control" type="text" name="detalle[]" value="'+articulos[articuloID].name+'"/></td>';
    nuevoArticulo += '<td><input readonly class="form-control monto" type="text" name="monto[]" value="'+articulos[articuloID].price+'"/></td>';
    nuevoArticulo += '<td><div class="btn btn-danger btn-sm borrar-articulo"><i class="fa fa-trash"></i> Eliminar</div><input class="artid" type="hidden" name="artid[]" value="'+articuloID+'"/></td>';
    nuevoArticulo += '</tr>';
    $('#articulos-table-holder').prepend(nuevoArticulo); 
    var nuevoMonto = parseFloat($('#total-resumen').text()) + parseFloat(articulos[articuloID].price);
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on("click",".borrar-articulo", function(e){ 
    e.preventDefault();
    var nuevoMonto = parseFloat($('#total-resumen').text()) - parseFloat($(this).closest('tr').find('input.monto').val());
    $(this).closest('tr').remove(); 
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on('keydown','input.monto, input.cantidad', function (event){
    if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
});

$('#articulos-table-holder').on('keyup','input.monto', function (event){
    var nuevoMonto = 0;
    $('.monto').each(function(){
        nuevoMonto += parseFloat($(this).val());
    });
    $('#total-resumen').text(nuevoMonto);
});

$('#articulos-table-holder').on('keyup','input.cantidad', function (event){
    var nuevoMonto = 0;
    var actualizarMonto = $(this).closest('tr').find('input.monto');
    var artID =  $(this).closest('tr').find('input.artid').val();
    if(artID){
        actualizarMonto.val(parseFloat($(this).val()) * parseFloat(articulos[artID].price));
    } else {
        actualizarMonto.val(parseFloat(actualizarMonto.val()) * parseFloat($(this).val()));
    }
    $('.monto').each(function(){
        nuevoMonto += parseFloat($(this).val());
    });
    $('#total-resumen').text(nuevoMonto);
});

$('input[name=pagado]').on('ifChecked', function (){
    var esPagado = $('input[name=pagado]:checked').val();
    if(esPagado == '1'){
        $('#pago-info').removeClass('hide');
    }else{
        $('#pago-info').addClass('hide');
    }
});


</script> 
@endpush