@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')
@php
$total_del_dia = 0;
@endphp
<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h1>RESUMEN DIARIO: {{$todayDate = date("d/m/Y")}}</h1>
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de cobros y pagos del día</h3>
            <br />
            <br />
        </div>

    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cobros de hoy por ventas de productos - Resultados</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Nro. Venta </th>
                                <th class="column-title">Cuenta </th>
                                <th class="column-title">Titular abonado </th>
                                <th class="column-title">Persona </th>
                                <th class="column-title">Total </th>
                                <th class="column-title">Fecha </th>
                                <th class="column-title">Pago en efectivo </th>
                                <th class="column-title">Pago con transferencia bancaria </th>
                                <th class="column-title">Pago con debito </th>
                                <th class="column-title">Pago con tarjeta </th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($cobros))
                            @foreach($cobros as $key => $cobro)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $cobro->id }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue">@if($cobro->cliente_id != null){{ $cobro->cli_abo }} @else {{ $cobro->com_abo }} @endif</div>
                                </td>
                                <td class=" "><i class="fa fa-user" aria-hidden="true"></i> @if($cobro->cliente_id != null){{ $cobro->cli_titu }} @else {{ $cobro->com_titu }} @endif</td>
                                <td class=" ">@if($cobro->cliente_id != null) <div class="badge bg-orange">Cliente</div> @else <div class="badge bg-purple">Comprador</div> @endif</td>
                                @php
                                $total_del_dia += $cobro->monto_tarjeta + $cobro->monto_efectivo + $cobro->monto_debito + $cobro->monto_cheque;
                                @endphp
                                <td class=" ">
                                    <div class="badge">${{ $cobro->monto_tarjeta + $cobro->monto_efectivo + $cobro->monto_debito + $cobro->monto_cheque  }}</div>
                                </td>
                                <td class=" "><i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($cobro->fecha)) }}</td>
                                <td class=" ">
                                    <div class="badge bg">$ {{$cobro->monto_efectivo}}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-green">$ {{$cobro->monto_cheque}}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-orange">$ {{$cobro->monto_debito}}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-purple">$ {{$cobro->monto_tarjeta}}</div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                @if(count($cobros))
                <div class="pull-right" style="display: none;">
                    {{ $cobros->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Pagos de hoy de abonos - Resultados</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Nro. Resumen </th>
                                <th class="column-title">Cuenta Abonado </th>
                                <th class="column-title">Titular </th>
                                <th class="column-title">Total </th>
                                <th class="column-title">Fecha </th>
                                <th class="column-title">Pago en efectivo </th>
                                <th class="column-title">Pago con transferencia bancaria </th>
                                <th class="column-title">Pago con debito </th>
                                <th class="column-title">Pago con tarjeta </th>

                            </tr>
                        </thead>
                        <tbody>
                            @if(count($pagos))

                            @php
                            $sub_checker_dia_total_del_dia = 0;
                            @endphp

                            @foreach($pagos as $key => $pago)

                            @php
                            $compare_day = date('d/m/Y', strtotime($pago->fecha));
                            @endphp

                            @if($todayDate == $compare_day)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">
                                    <div class="badge bg-blue-sky">
                                        {{ $pago->resumen_id }}
                                    </div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue">
                                        @if($pago->resumen->cliente){{ $pago->resumen->cliente->cuenta_abonado }} @else - @endif
                                    </div>
                                </td>
                                <td class=" "><i class="fa fa-user" aria-hidden="true"></i> @if($pago->resumen->cliente){{ $pago->resumen->cliente->titular_responsable }} @else - @endif</td>
                                <td class=" ">
                                    <div class="badge">
                                        $ {{ $pago->monto_tarjeta + $pago->monto_efectivo + $pago->monto_debito + $pago->monto_cheque  }}
                                    </div>
                                </td>
                                @php
                                $total_del_dia += $pago->monto_tarjeta + $pago->monto_efectivo + $pago->monto_debito + $pago->monto_cheque;
                                $sub_checker_dia_total_del_dia += $pago->monto_tarjeta + $pago->monto_efectivo + $pago->monto_debito + $pago->monto_cheque;
                                @endphp
                                <td class=" ">
                                    <i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($pago->fecha)) }}
                                </td>
                                <td class=" ">
                                    <div class="badge">
                                        $ {{$pago->monto_efectivo}}
                                    </div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-green">
                                        $ {{$pago->monto_cheque}}
                                    </div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-orange">
                                        $ {{$pago->monto_debito}}
                                    </div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-purple">
                                        $ {{$pago->monto_tarjeta}}
                                    </div>
                                </td>
                            </tr>
                            @endif

                            @endforeach

                            @if($sub_checker_dia_total_del_dia == 0)
                            <tr>
                                <td colspan="10">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif

                            @else
                            <tr>
                                <td colspan="10">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                @if(count($pagos))
                <div class="pull-right" style="display: none;">
                    {{ $pagos->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Total: ${{$total_del_dia}}</h3>

                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="pull-right">
                <button onclick="window.print()" class="btn btn-success"><i class="fa fa-print"></i> Imprimir</button>
            </div>
        </div>
    </div>

    @include('pagos/partials/modalDelete')
</div>
<!-- /page content -->

@endsection
@push('scripts')
<link rel="stylesheet" media="print" href="{{ asset("css/print.css") }}" />
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.find('select').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $pago = $(this).data('pago');
        $modalObj.find('#btnDelete').data('pago', $pago);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $pago = $this.data('pago');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("pagos")}}/' + $pago,
            data: {
                id: $pago,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });



    $("#fecha").daterangepicker({
        singleDatePicker: 0,
        singleClasses: "picker_3",
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c)
    });

    $('#fecha').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('#fecha').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#export-excel').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to("pagos/exportar-excel")}}' + query);
    });

    $('#export-pdf').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to("pagos/exportar-pdf")}}' + query);
    });
</script>
@endpush