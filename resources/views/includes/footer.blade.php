<!-- footer content -->
<footer>
    <div class="pull-right">
        <strong>&#9400; KEEPER 2021 - Seguridad Privada - v.1.0.8</strong>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->

<style type="text/css">
    .badge {
        font-weight: normal !important;
    }
</style>