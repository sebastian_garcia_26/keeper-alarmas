<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="{{ url('/') }}" class="site_title"><i class="fa fa-briefcase fa-x2" style="color:#73879C; border:1px solid #73879C;"></i> <span style="font-size: 17px;color:#73879C;">Resumen Diario</span></a>
                </li>


            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->