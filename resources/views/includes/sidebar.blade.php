<div class="sidebar-left-col col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="clearfix"></div>

        <style type="text/css">
            #sidebar-menu h3 {
                border-bottom: 1px solid #E7E7E7;
                padding-bottom: 15px;
                color: #E7E7E7;
            }
        </style>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="/pictures/logo.jpg" alt="Logo " class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <h2 style="font-weight: bold;font-size: 22px; margin-bottom: 5px;">KEEPER</h2>
                <h2>Seguridad Privada</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu Principal</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-dollar"></i> Abonos mensuales <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('abonos/create')}}"><i class="fa fa-plus"></i>Crear abono</a></li>
                            <li><a href="{{URL::to('abonos')}}"><i class="fa fa-list"></i>Listado de abonos</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-dropbox"></i> Artículos / Productos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('articulos/create')}}"><i class="fa fa-plus"></i>Crear artículo</a></li>
                            <li><a href="{{URL::to('articulos')}}"><i class="fa fa-list"></i>Listado de artículos</a></li>
                        </ul>
                    </li>
                </ul>

                <br />
                <h3>Clientes / Abonados</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-users"></i> Clientes abonados <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('clientes/create')}}"><i class="fa fa-plus"></i>Crear cliente abonado</a></li>
                            <li><a href="{{URL::to('clientes')}}"><i class="fa fa-list"></i>Listado de clientes abonados</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-calculator"></i> Resúmenes abonados <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('resumenes/generar')}}"><i class="fa fa-gear"></i>Generar resúmenes abonados x mes</a></li>
                            <li><a href="{{URL::to('resumenes')}}"><i class="fa fa-list"></i>Listado de resúmenes abonados de todos los meses</a></li>
                            <li><a href="{{URL::to('resumenes/imprimir')}}"><i class="fa fa-print"></i>Impresión resúmenes abonados x mes</a></li>
                        </ul>
                    </li>
                </ul>

                <br />
                <h3>Ventas / Productos</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-user-plus"></i> Compradores <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('compradores/create')}}"><i class="fa fa-plus"></i>Crear comprador</a></li>
                            <li><a href="{{URL::to('compradores')}}"><i class="fa fa-list"></i>Listado de compradores</a></li>
                        </ul>
                    </li>

                    <li>
                        <a><i class="fa fa-cart-plus"></i> Ventas productos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('ventas/create')}}"><i class="fa fa-plus"></i>Crear venta de producto</a></li>
                            <li><a href="{{URL::to('ventas')}}"><i class="fa fa-list"></i>Listado de todas las ventas de productos</a></li>
                        </ul>
                    </li>
                </ul>

                <br />
                <h3>Cobranza / Deudores</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-credit-card"></i> Pago cliente abonado <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('pagos/create')}}"><i class="fa fa-plus"></i>Crear pago de un abono</a></li>
                            <li><a href="{{URL::to('pagos')}}"><i class="fa fa-list"></i>Listado de pagos de abonados</a></li>
                            <li><a href="{{URL::to('pagos-deudores')}}"><i class="fa fa-users"></i>Listado de abonados deudores</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-money"></i> Cobro venta producto <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{URL::to('cobros/create')}}"><i class="fa fa-plus"></i>Crear cobro de venta de producto</a></li>
                            <li><a href="{{URL::to('cobros')}}"><i class="fa fa-list"></i>Listado de cobros de ventas de productos</a></li>
                            <li><a href="{{URL::to('cobros-deudores')}}"><i class="fa fa-users"></i>Listado de deudores de productos</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
        <!-- /sidebar menu -->

    </div>
</div>