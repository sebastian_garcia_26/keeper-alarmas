<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            table.striped tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td width="100">
                    <img src="{{public_path()}}/pictures/logo.jpg" width="100">
                </td>
                <td>
                    <h2>Pagos - KEEPER, Seguridad Privada</h2><b>Generado: {{$date}}</b>
                </td>
                
            </tr>
            @if(count($params))
                @foreach($params as $index => $value)
                    @if($value != '')
                    <tr>
                        <td colspan="2">
                            <b>{{$parseSearch[$index]}}: {{$value}}</b>
                        </td>
                    </tr>
                    @endif
                @endforeach
            @endif
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
        
        <table class="striped">
          <tr>
            <th>Nro. Resumen</th>
            <th>Cuenta Abonado</th>
            <th>Titular - Subscriptor</th>
            <th>Total</th>
            <th>Fecha</th>
            
            <th>Pago en efectivo</th>
            <th>Pago con transferencia bancaria</th>
            <th>Pago con debito</th>
            <th>Pago con tarjeta</th>
          </tr>
          @if(count($pagos))
          <?php $total = 0; ?>
            @foreach($pagos as $pago)
                <tr>
                  <td>{{$pago->resumen_id}}</td>
                  <td>{{$pago->resumen->cliente->cuenta_abonado}}</td>
                  <td>{{$pago->resumen->cliente->titular_responsable}}</td>
                  <td>{{$pago->monto_efectivo + $pago->monto_cheque + $pago->monto_debito + $pago->monto_tarjeta}}</td>
                  <td>{{date_format(\DateTime::createFromFormat('Y-m-d', $pago->fecha), "d/m/Y")}}</td>
                  <td>{{$pago->monto_efectivo}}</td>
                  <td>{{$pago->monto_cheque}}</td>
                  <td>{{$pago->monto_debito}}</td>
                  <td>{{$pago->monto_tarjeta}}</td>
                </tr>
                <?php $total += $pago->monto_efectivo + $pago->monto_cheque + $pago->monto_debito + $pago->monto_tarjeta; ?>
            @endforeach
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL</b></td>
                  <td><?php echo $total; ?></td>
                </tr>
          @endif
          
        </table>

    </body>
</html>