<!DOCTYPE html>

<head>

    <style>
        @page {
            margin: 0in;
        }


        body {
            background-image: url(<?php echo '/pictures/resumen-bg.jpg'; ?>);
            background-position: top left;
            background-repeat: no-repeat;
            width: 100%;
            height: 100%;
            font-size: 16px !important;
            line-height: 20px !important;
        }
    </style>
</head>

<body>


    <table style="padding-top: 85px;">
        <tr>
            <td width="300">

            </td>
            <td>
                Fecha de Vencimiento {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->primer_vencimiento), "d/m/Y")}}
            </td>

        </tr>
        <tr>
            <td width="300">
                <br /><br /><br />
            </td>
            <td>
                Proximo Vencimiento {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->segundo_vencimiento), "d/m/Y")}}
            </td>

        </tr>
    </table>
    <table style="padding-top: 56px;">
        <tr>
            <td width="300" style="padding-left: 40px;">
                @if($resumen->cliente)
                {{mb_strtoupper($resumen->cliente->titular_responsable)}}<br>
                {{mb_strtoupper($resumen->cliente->calle. ' '.$resumen->cliente->numero) }}<br>
                {{mb_strtoupper($resumen->cliente->codigo_postal. ' - '.$resumen->cliente->ciudad.' - ' .$resumen->cliente->provincia) }}<br>
                {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->cliente->fecha_alta), "d/m/Y")}} - {{mb_strtoupper($resumen->cliente->cuenta_abonado)}}
                @else
                <br><br><br><br>
                @endif
            </td>
            <td style="padding-left: 60px; padding-top:-59px;">
                @if($resumen->cliente)
                <strong>{{$resumen->cliente->id}}</strong>
                @endif
            </td>

        </tr>

    </table>
    <table>
        <tr>
            <td width="300">

            </td>
            <td style="padding-left: 180px; padding-top:-38px;">
                <strong>{{$resumen->id}}</strong>
            </td>

        </tr>

    </table>
    <table style="padding-top: 22px;">
        <tr>
            <td width="300">

            </td>
            <td style="padding-left: 240px; ">
                <strong>${{number_format($resumen->monto,2)}}</strong>
            </td>

        </tr>

    </table>
    <table style="padding-top: 40px; padding-left: 40px;">

        <tr>
            <td width="100"><strong style="text-transform: uppercase;">Cantidad</strong></td>
            <td width="300"><strong style="text-transform: uppercase;">Detalle</strong></td>
            <td width="100"><strong style="text-transform: uppercase;">Importe</strong></td>
        </tr>
        @foreach($resumen->lineas() as $linea)
        <tr>
            <td width="100">{{number_format($linea->cantidad,2)}}</td>
            <td width="300">{{mb_strtoupper($linea->descripcion)}}</td>
            <td width="100"><strong>${{number_format($linea->monto,2)}}</strong></td>
        </tr>
        @endforeach

    </table>

    <div style="position:absolute;width:150px;top:630px;left:170px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->primer_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:630px;left:270px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:680px;left:22px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:690px;left:276px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto_1_vencimento,2)}}</strong>
    </div>
    <div style="position:absolute;top:705px;left:22px;text-align: left; width: 224px;">
        Nro. Resumen {{$resumen->id}}
    </div>
    <div style="position:absolute;top:788px;left:80px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras1}}
    </div>


    <div style="position:absolute;width:150px;top:630px;left:540px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->primer_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:630px;left:640px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:680px;left:410px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:690px;left:646px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto_1_vencimento,2)}}</strong>
    </div>
    <div style="position:absolute;top:705px;left:410px;text-align: left; width: 224px;">
        Nro. Resumen {{$resumen->id}}
    </div>
    <div style="position:absolute;top:788px;left:450px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras1}}
    </div>

    <div style="position:absolute;width:150px;top:876px;left:170px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->segundo_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:878px;left:276px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:930px;left:22px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:935px;left:276px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto_2_vencimento,2)}}</strong>
    </div>
    <div style="position:absolute;top:950px;left:22px;text-align: left; width: 224px;">
        Nro. Resumen {{$resumen->id}}
    </div>
    <div style="position:absolute;top:1034px;left:80px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras2}}
    </div>


    <div style="position:absolute;width:150px;top:876px;left:540px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->segundo_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:878px;left:646px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:930px;left:410px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:935px;left:646px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto_2_vencimento,2)}}</strong>
    </div>
    <div style="position:absolute;top:950px;left:410px;text-align: left; width: 224px;">
        Nro. Resumen {{$resumen->id}}
    </div>
    <div style="position:absolute;top:1034px;left:450px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras2}}
    </div>


</body>

</html>