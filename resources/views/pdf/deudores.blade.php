<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            table.striped tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td width="100">
                    <img src="{{public_path()}}/pictures/logo.jpg" width="100">
                </td>
                <td>
                    <h2>Deudores - KEEPER, Seguridad Privada</h2><b>Generado: {{$date}}</b>
                </td>
                
            </tr>
            @if(count($params))
                @foreach($params as $index => $value)
                    @if($value != '')
                    <tr>
                        <td colspan="2">
                            <b>{{$parseSearch[$index]}}: {{$value}}</b>
                        </td>
                    </tr>
                    @endif
                @endforeach
            @endif
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
        
        <table class="striped">
          <tr >
              <th>Nro. Cliente </th>
              <th>Cuenta Abonado </th>
              <th>Titular - Subscriptor </th>
              <th>Dirección </th>
              <th>Teléfono </th>
              <th>Debe </th>

          </tr>
          @if(count($deudores))
          <?php $total = 0; ?>
            @foreach($deudores as $deudor)
                <tr>
                    <td>{{ $deudor->id }} </td>
                    <td>{{ $deudor->cuenta_abonado }}</td>
                    <td>{{ $deudor->titular_responsable }}</td>
                    <td>{{ $deudor->calle }} {{$deudor->numero}}</td>
                    <td>{{ $deudor->telefono }} {{$deudor->celular}}</td>
                    <td>${{ $deudor->a_pagar - $deudor->pagado }}</td>
                </tr>
                <?php $total += $deudor->a_pagar - $deudor->pagado; ?>
            @endforeach
                <tr>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><b>TOTAL DEUDA</b></td>
                  <td>$<?php echo $total; ?></td>
                </tr>
          @endif
          
        </table>

    </body>
</html>