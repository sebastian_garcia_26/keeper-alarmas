<!DOCTYPE html>

<head>

    <style>
        @page {
            margin: 0in;
        }
        body {
            background-image: url(<?php echo '/pictures/venta-bg.jpg'; ?>);
            background-position: top left;
            background-repeat: no-repeat;
            width: 100%;
            height: 100%;
            font-size: 16px !important;
            line-height: 20px !important;
        }
    </style>
</head>

<body>

    <table style="padding-top: 110px;">
        <tr>
            <td width="300">
                <br />
            </td>
        </tr>
        <tr>
            <td width="300">
                <br />
            </td>
        </tr>
    </table>
    <table style="padding-top: 60px;">
        <tr>
            @if($resumen->cliente)
            <td width="300" style="padding-left: 40px;">

                {{mb_strtoupper($resumen->cliente->titular_responsable)}}<br>
                {{mb_strtoupper($resumen->cliente->calle. ' '.$resumen->cliente->numero) }}<br>
                {{mb_strtoupper($resumen->cliente->codigo_postal. ' - '.$resumen->cliente->ciudad.' - ' .$resumen->cliente->provincia) }}<br>
                {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->cliente->fecha_alta), "d/m/Y")}} - {{mb_strtoupper($resumen->cliente->cuenta_abonado)}}
                @else
                <br>
            </td>
            @endif
            @if($resumen->comprador)
            <td width="300" style="padding-left: 40px;">
                {{mb_strtoupper($resumen->comprador->titular_responsable)}}<br>
                {{mb_strtoupper($resumen->comprador->calle. ' '.$resumen->comprador->numero) }}<br>
                {{mb_strtoupper($resumen->comprador->codigo_postal. ' - '.$resumen->comprador->ciudad.' - ' .$resumen->comprador->provincia) }}<br>
                {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->comprador->fecha_alta), "d/m/Y")}} - {{mb_strtoupper($resumen->comprador->cuenta_abonado)}}
                @else
                <br>
                <br>
            </td>
            @endif
            <td style="padding-left: 80px; padding-top:-58px;">
                @if($resumen->cliente)
                <strong>{{$resumen->cliente->id}}</strong>
                @endif
            </td>

        </tr>

    </table>
    <table>
        <tr>
            <td width="300">
            </td>
            <td style="padding-left: 180px; padding-top:-56px;">
                <strong>{{$resumen->id}}</strong>
            </td>

        </tr>

    </table>
    <table style="padding-top: 12px;">
        <tr>
            <td width="300">

            </td>
            <td style="padding-left: 225px; ">
                <strong>${{number_format($resumen->monto,2)}}</strong>
            </td>

        </tr>

    </table>
    <table style="padding-top: 60px; padding-left: 40px;">

        <tr>
            <td width="100"><strong style="text-transform: uppercase;">Cantidad</strong></td>
            <td width="300"><strong style="text-transform: uppercase;">Detalle</strong></td>
            <td width="100"><strong style="text-transform: uppercase;">Importe</strong></td>
        </tr>
        @foreach($resumen->lineas() as $serie)
        <tr>
            <td width="100">{{number_format($serie->cantidad,2)}}</td>
            <td width="300">{{mb_strtoupper($serie->descripcion)}}</td>
            <td width="100"><strong>${{number_format($serie->monto,2)}}</strong></td>
        </tr>
        @endforeach

    </table>

    <div style="position:absolute;width:150px;top:876px;left:170px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->primer_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:877px;left:270px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:920px;left:40px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:935px;left:276px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto,2)}}</strong>
    </div>
    <div style="position:absolute;top:940px;left:40px;text-align: left; width: 224px;">
        Nro. venta {{$resumen->id}}
    </div>
    <div style="position:absolute;top:1034px;left:80px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras}}
    </div>


    <div style="position:absolute;width:150px;top:876px;left:540px;">
        {{date_format(\DateTime::createFromFormat('Y-m-d', $resumen->primer_vencimiento), "d/m/Y")}}
    </div>
    <div style="position:absolute;top:877px;left:640px;text-align: center; width: 82px;">
        @if($resumen->cliente){{$resumen->cliente->id}}@endif
    </div>
    <div style="position:absolute;top:920px;left:410px;text-align: left; width: 224px;">
        @if($resumen->cliente){{$resumen->cliente->titular_responsable}}@endif
    </div>
    <div style="position:absolute;top:935px;left:646px;text-align: center; width: 82px;">
        <strong>${{number_format($resumen->monto,2)}}</strong>
    </div>
    <div style="position:absolute;top:940px;left:410px;text-align: left; width: 224px;">
        Nro. venta {{$resumen->id}}
    </div>
    <div style="position:absolute;top:1034px;left:450px;text-align: left; width: 306px; font-size: 12px !important; line-height: 14px !important;">
        {{$montoLetras}}
    </div>


</body>

</html>