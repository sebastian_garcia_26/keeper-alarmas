<!DOCTYPE html>
<html>
    <head>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            table.striped tr:nth-child(even){background-color: #f2f2f2}

            th {
                background-color: #4CAF50;
                color: white;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td width="100">
                    <img src="{{public_path()}}/pictures/logo.jpg" width="100">
                </td>
                <td>
                    <h2>Clientes - KEEPER, Seguridad Privada</h2><b>Generado: {{$date}}</b>
                </td>
                
            </tr>
            @if(count($params))
                @foreach($params as $index => $value)
                    @if($value != '')
                    <tr>
                        <td colspan="2">
                            @if($index == 'abono_id')
                            <b>{{$parseSearch[$index]}}: {{$abonoSearch}}</b>
                            @else
                            <b>{{$parseSearch[$index]}}: {{$value}}</b>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endforeach
            @endif
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
        
        <table class="striped">
          <tr>
            <th>ID</th>
            <th>Cuenta Abonado</th>
            <th>Nombre - Razón Social</th>
            <th>Calle</th>
            <th>Numero</th>
            
            <!-- <th>Código Postal</th> -->
            <th>Tel</th>
            <th>Celular</th>
            <th>CUIT</th>
            <th>Fecha Ingreso</th>
            <th>Abono</th>
          </tr>
          @if(count($clientes))
            @foreach($clientes as $cliente)
                <tr>
                  <td>{{$cliente->id}}</td>
                  <td>{{$cliente->cuenta_abonado}}</td>
                  <td>{{$cliente->nombre_razon_social}}</td>
                  <td>{{$cliente->calle}}</td>
                  <td>{{$cliente->numero}}</td>
                  <!-- <td>{{$cliente->codigo_postal}}</td> -->
                  <td>{{$cliente->telefono}}</td>
                  <td>{{$cliente->celular}}</td>
                  <td>{{$cliente->cuit}}</td>
                  <td>{{date_format(\DateTime::createFromFormat('Y-m-d', $cliente->fecha_alta), "d/m/Y")}}</td>
                  <td>{{$cliente->abono->name}}</td>
                </tr>
            @endforeach
          @endif
          
        </table>

    </body>
</html>