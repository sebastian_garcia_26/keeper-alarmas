@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Crear cobro de venta de producto</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('cobros')}}"><i class="fa fa-list"></i> Ver Listado</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        {{Form::open(array('url' => '/cobros', 'files' => true, 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Nro. venta</label>
                                <input  value="{{ old('venta_id') }}" type="text" name="venta_id" id="venta_id" class="form-control" >
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <br>
                                <button class="btn btn-primary" id="btn-verify"><i class="fa fa-check-circle-o"></i> Verificar</button>
                            </div>

                        </div>
                        <div class="row resumen-wrapper">

                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Fecha *</label>
                                <input value="{{ old('fecha') }}" type="text" class="form-control has-feedback-left" required="required" id="fecha" name="fecha"  aria-describedby="fechaIngreso">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaIngreso" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row" >
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Comentarios</label>
                                    <input value="{{ old('observacion') }}" type="text" name="observacion" id="observacion" class="form-control" >
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago en efectivo</label>
                                <input value="{{ old('monto_efectivo') }}" type="text" name="monto_efectivo" id="monto_efectivo" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con transferencia bancaria</label>
                                <input value="{{ old('monto_cheque') }}" type="text" name="monto_cheque" id="monto_cheque" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con debito</label>
                                <input value="{{ old('monto_debito') }}" type="text" name="monto_debito" id="monto_debito" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con tarjeta</label>
                                <input value="{{ old('monto_tarjeta') }}" type="text" name="monto_tarjeta" id="monto_tarjeta" class="form-control" >
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
        </div>


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>

$("#fecha").daterangepicker({
    singleDatePicker: !0,
    singleClasses: "picker_3",
    locale: {
        format: "DD/MM/YYYY",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "May",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(a, b, c) {
    console.log(a.toISOString(), b.toISOString(), c)
});

$('#btn-verify').on('click', function (e){
    e.preventDefault();
    $ventaID = $('#venta_id').val();
    $.ajax({
            type: "POST",
            url: '{{URL::to("resumenAjaxVenta/get")}}/'+$ventaID,
            data: {
                id: $ventaID,
                _token: "{{ csrf_token() }}"
            },
            success: function( msg ) {
                $('.resumen-wrapper').html(msg);
            }
        });
});
</script>
@endpush
