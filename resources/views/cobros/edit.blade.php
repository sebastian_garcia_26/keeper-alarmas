@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Editar pago por venta de un producto</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('cobros')}}"><i class="fa fa-list"></i> Ver Listado</a>
                <a class="btn btn-primary" href="{{URL::to('cobros/create')}}"><i class="fa fa-plus"></i> Crear Pago</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         {{Form::model($cobro, array('route' => array('cobros.update', $cobro->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Nro. Resumen</label>
                                <input  value="{{ $cobro->resumen_id }}"  type="text" name="resumen_id" id="resumen_id" class="form-control " disabled>
                            </div>


                        </div>
                        <div class="row resumen-wrapper">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Total a pagar: </label>
                                <input  value="{{$cobro->resumen->monto}}" type="text" disabled class="form-control" >
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Titular - Subscriptor: </label>
                                <input  value="{{$cobro->resumen->cliente->titular_responsable}}" type="text" disabled class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Cuenta Abonado: </label>
                                <input  value="{{$cobro->resumen->cliente->cuenta_abonado}}" type="text" disabled class="form-control" >
                            </div>
                            <?php
                            $pagado = 0;

                            ?>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pagado: </label>
                                <input  value="<?php echo $pagado; ?>" type="text" disabled class="form-control" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Fecha *</label>
                                <input value="{{ date('d/m/Y', strtotime($cobro->fecha)) }}" type="text" class="form-control has-feedback-left" required="required" id="fecha" name="fecha"  aria-describedby="fechaIngreso">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaIngreso" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row" >
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label>Comentarios</label>
                                    <input value="{{ $cobro->observacion }}" type="text" name="observacion" id="observacion" class="form-control" >
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago en efectivo</label>
                                <input value="{{ $cobro->monto_efectivo }}" type="text" name="monto_efectivo" id="monto_efectivo" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con transferencia bancaria</label>
                                <input value="{{ $cobro->monto_cheque }}" type="text" name="monto_cheque" id="monto_cheque" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con debito</label>
                                <input value="{{ $cobro->monto_debito }}" type="text" name="monto_debito" id="monto_debito" class="form-control" >
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pago con tarjeta</label>
                                <input value="{{ $cobro->monto_tarjeta }}" type="text" name="monto_tarjeta" id="monto_tarjeta" class="form-control" >
                            </div>

                        </div>
                         <input name="id" type="hidden" value="{{$cobro->id}}" />

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div class="divider"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
        </div>


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>

$("#fecha").daterangepicker({
    singleDatePicker: !0,
    singleClasses: "picker_3",
    locale: {
        format: "DD/MM/YYYY",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "May",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(a, b, c) {
    console.log(a.toISOString(), b.toISOString(), c)
});

$('#btn-verify').on('click', function (e){
    e.preventDefault();
    $ventaID = $('#resumen_id').val();
    $.ajax({
            type: "POST",
            url: '{{URL::to("resumenAjax/get")}}/'+$ventaID,
            data: {
                id: $ventaID,
                _token: "{{ csrf_token() }}"
            },
            success: function( msg ) {
                $('.resumen-wrapper').html(msg);
            }
        });
});
</script>
@endpush
