@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Edit Student</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('students')}}"><i class="fa fa-search"></i> Search Students</a>
                <a class="btn btn-primary" href="{{URL::to('students/create')}}"><i class="fa fa-user-plus"></i> New Student</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>    
            </div>    
        @endif

        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>    
            </div>    
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Student</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                    {{ Form::model($student, array('route' => array('students.update', $student->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal form-label-left input_mask')) }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="name" name="name" required="required" value="{{ $student->name }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="lastname" name="lastname" required="required" value="{{ $student->lastname }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="student_id">Student ID <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="student_id" name="student_id" required="required" value="{{ $student->student_id }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="email" class="form-control col-md-7 col-xs-12" type="email" value="{{ $student->email }}" name="email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Phone</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="phone" class="form-control col-md-7 col-xs-12" value="{{ $student->phone }}" type="text" name="phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="birthday" name="birthday" value="{{ date('Y-m-d', strtotime($student->birthday)) }}" class="date-picker form-control col-md-7 col-xs-12"  type="date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Picture </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="btn btn-primary" for="my-file-selector">
                                        <input id="my-file-selector" name="picture" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
                                        Browse
                                    </label>
                                    <span class='label label-info' id="upload-file-info"></span>
                                    <div class="clearfix"></div>
                                    @if($student->picture && File::exists('pictures/'.$student->picture))
                                    <div style="width:100px;margin-top:20px;">
                                        <img src="{{URL::to('pictures/'.$student->picture)}}" class="img-thumbnail" id="img-thumbnail" style="max-width:100%;max-height:100%;float:left;" >
                                        <a class="btn btn-danger pull-left" id="removeImage" style="margin-top: 10px;"><i class="fa fa-trash"></i></a>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $student->id }}">
                            <input type="hidden" name="delete-image" id="delete-image" value="0" />
                        {{ Form::close() }}
                    </div>
                      
                </div>
            </div>  
        </div>    


    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Students App - Demo
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
@endsection
@push('scripts')
<script>
$('#removeImage').on('click', function (e){
        var $this = $(this);
            $this.hide();
            $('#delete-image').val(1);
            $('#img-thumbnail').hide();
        
});

</script> 
@endpush