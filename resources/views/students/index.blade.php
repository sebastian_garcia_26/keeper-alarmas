@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Search Students</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('students/create')}}"><i class="fa fa-user-plus"></i> New Student</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Search Students</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <form method="GET" class="form-horizontal form-label-left input_mask">

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <input type="text" name="name" class="form-control" value="{{$request->get('name')}}" id="name" placeholder="Name">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <input type="text" class="form-control" name="lastname" id="lastname" value="{{$request->get('lastname')}}" placeholder="Last Name">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <input type="text" class="form-control" name="email" id="email" value="{{$request->get('email')}}" placeholder="Email">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <input type="text" class="form-control" name="student_id" id="student_id" value="{{$request->get('student_id')}}" placeholder="Student ID">
                            </div>

                          <div class="form-group">
                            <div class="pull-right">
                               <button class="btn btn-primary reset-btn" type="reset">Reset</button>
                               <button type="submit" class="btn btn-success">Search</button>
                            </div>
                          </div>

                        </form>
                    </div>
                      
                </div>
            </div>  
        </div>    

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Name </th>
                            <th class="column-title">Last Name </th>
                            <th class="column-title">Student ID </th>
                            <th class="column-title">Email </th>
                            <th class="column-title">Phone </th>
                            <th class="column-title no-link last"><span class="nobr">Actions</span></th>
                            
                          </tr>
                        </thead>

                        <tbody>
                        @if(count($students))
                            @foreach($students as $key => $student)
                              <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">{{ $student->name }} </td>
                                <td class=" ">{{ $student->lastname }} </td>
                                <td class=" ">{{ $student->student_id }}</td>
                                <td class=" ">@if($student->email){{ $student->email }} @else - @endif</td>
                                <td class="a-right a-right ">@if($student->email){{ $student->phone }}@else - @endif</td>
                                <td class=" last">
                                    <a class="btn btn-primary btn-xs" href="{{URL::to('students/'.$student->id.'/edit')}}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger btn-xs delete-modal-trigger" data-student="{{$student->id}}" href="#"><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                            @endforeach

                        @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> There were not any results that match your search criteria.</div>
                                </td>
                            </tr>
                        @endif  


                          
                          
                        </tbody>
                </table>
                @if(count($students))
                <div class="pull-right">
                    {{ $students->render()}}
                </div>
                @endif
            </div>      
        </div>

    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Students App - Demo
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
    @include('students/partials/modalDelete')
@endsection
@push('scripts')
<script>

$('.reset-btn').on('click', function (e){
    var $formObj = $(this).closest('form');
    $formObj.find('input').each(function( index ) {
        $( this ).val('');
    });
    $formObj.submit();
});

$('.delete-modal-trigger').on('click', function (e){
    var $modalObj = $('#delete-modal');
        $student = $(this).data('student');
    $modalObj.find('#btnDelete').data('student',$student);
    $modalObj.modal('show');
});

$('#btnDelete').on('click', function (e){
        var $this = $(this);
            $student = $this.data('student');
            $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("students")}}/' + $student,
            data: {
                id: $student,
                _token: "{{ csrf_token() }}"
            },
            success: function( msg ) {
                location.reload();
            }
        });
});


</script> 
@endpush