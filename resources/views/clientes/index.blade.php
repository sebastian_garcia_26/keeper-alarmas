@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de Clientes Abonados</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('clientes/create')}}"><i class="fa fa-plus"></i> Crear Cliente Abonado</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Buscar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="GET" class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="col-xs-1 form-group">
                                <input type="text" name="id" class="form-control" value="{{$request->get('id')}}" id="id" placeholder="ID">
                            </div>
                            <div class="col-xs-3 form-group">
                                <input type="text" name="cuenta_abonado" class="form-control" value="{{$request->get('cuenta_abonado')}}" id="cuenta_abonado" placeholder="Cuenta Abonado">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 form-group">
                                <input type="text" name="titular_responsable" class="form-control" value="{{$request->get('titular_responsable')}}" id="titular_responsable" placeholder="Titular - Subscriptor">
                            </div>
                            <div class="col-xs-4 form-group">
                                <input type="text" name="nombre_razon_social" class="form-control" value="{{$request->get('nombre_razon_social')}}" id="nombre_razon_social" placeholder="Nombre - Razón Social">
                            </div>

                            <div class="col-xs-4 form-group">
                                <select name='abono_id' id="abono_id" class="form-control">
                                    <option value="">- Filtrar por Abono mensual -</option>
                                    @foreach(App\Abono::withTrashed()->get() as $abono)
                                    <option value="{{ $abono->id }}" @if($request->get('abono_id') == $abono->id) selected @endif>{{ $abono->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <input value="{{ $request->get('fecha_ingreso') }}" type="text" class="form-control has-feedback-left" id="fecha_ingreso" name="fecha_ingreso" placeholder="Fecha Ingreso" aria-describedby="fechaIngreso">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaIngreso" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <div class="pull-left">
                                    <button id="export-excel" class="btn btn-warning"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Crear Excel</button>
                                    <button id="export-pdf" class="btn btn-info"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Crear PDF</button>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-primary reset-btn" type="reset"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">

                    <!-- <pre> -->
                    {{-- var_dump($clientes) --}}
                    <!-- </pre> -->

                    <h2>Resultados ({{$clientes->total()}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Titular - Subscriptor </th>
                                <th class="column-title">ID </th>
                                <th class="column-title">Cuenta Abonado </th>
                                <th class="column-title">Tipo de Abono Mensual </th>
                                <th class="column-title">Razón Social </th>
                                <th class="column-title">Provincia - Ciudad </th>
                                <th class="column-title">Dirección </th>
                                <th class="column-title">Teléfono </th>
                                <th class="column-title">Celular </th>
                                <th class="column-title">Fecha Ingreso </th>
                                <th class="column-title">Mes </th>
                                <th class="column-title no-link last"><span class="nobr"><i class="fa fa-cog" aria-hidden="true"></i></span></th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($clientes))
                            @foreach($clientes as $key => $cliente)

                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" "><i class="fa fa-user" aria-hidden="true"></i> {{ $cliente->titular_responsable }}</td>
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $cliente->id }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue">{{ $cliente->cuenta_abonado }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-green" style="white-space: normal; max-width: 128px;">{{ $cliente->abono->name }}</div>
                                </td>
                                <td class=" "><i class="fa fa-building-o" aria-hidden="true"></i> {{ $cliente->nombre_razon_social }}</td>
                                <td class=" "><i class="fa fa-map" aria-hidden="true"></i> {{ $cliente->provincia }} {{ $cliente->ciudad }}</td>
                                <td class=" "><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $cliente->calle }} {{$cliente->numero}}</td>
                                <td class=" "><i class="fa fa-phone" aria-hidden="true"></i> {{ $cliente->telefono }}</td>
                                <td class=" "><i class="fa fa-mobile" aria-hidden="true"></i> {{ $cliente->celular }}</td>
                                <td class=" "><i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($cliente->fecha_alta)) }}</td>
                                <td class="text-uppercase">
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='01')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Enero
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='02')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Febrero
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='03')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Marzo
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='04')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Abril
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='05')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Mayo
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='06')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Junio
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='07')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Julio
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='08')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Agosto
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='09')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Septiembre
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='10')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Octubre
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='11')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Noviembre
                                    @endif
                                    @if(date('m', strtotime($cliente->fecha_alta)) =='12')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Diciembre
                                    @endif
                                </td>

                                <td class=" last">
                                    <a class="btn btn-primary btn-xs" href="{{URL::to('clientes/'.$cliente->id.'/edit')}}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger btn-xs delete-modal-trigger" data-cliente="{{$cliente->id}}" href="#"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif




                        </tbody>
                    </table>
                </div>
                @if(count($clientes))
                <div class="pull-right">
                    {{ $clientes->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>

</div>
<!-- /page content -->
@include('clientes/partials/modalDelete')
@endsection
@push('scripts')
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.find('select').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $cliente = $(this).data('cliente');
        $modalObj.find('#btnDelete').data('cliente', $cliente);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $cliente = $this.data('cliente');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("clientes")}}/' + $cliente,
            data: {
                id: $cliente,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });



    $("#fecha_ingreso").daterangepicker({
        singleDatePicker: 0,
        singleClasses: "picker_3",
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c)
    });

    $('#fecha_ingreso').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('#fecha_ingreso').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#export-excel').on('click', function() {
        var query = window.location.search;
        window.open("{{URL::to('clientes/exportar-excel')}}" + query);
    });

    $('#export-pdf').on('click', function() {
        var query = window.location.search;
        window.open("{{URL::to('clientes/exportar-pdf')}}" + query);
    });
</script>
@endpush