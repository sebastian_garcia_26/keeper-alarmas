<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Students App | </title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
				{!! BootForm::open(['url' => url('/login'), 'method' => 'post']) !!}
                    
				<h1>Login Form</h1>
			
				{!! BootForm::text('username', 'Username', old('username'), ['placeholder' => 'Username'] ) !!}
			
				{!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}
				
				<div>
					{!! BootForm::submit('Log in', ['class' => 'btn btn-default submit']) !!}
					<a class="reset_pass" href="{{  url('/password/reset') }}">Lost your password ?</a>
				</div>
                    
				<div class="clearfix"></div>
                    
				<div class="separator">
					<p class="change_link">New to site?
						<a href="{{ url('/register') }}" class="to_register"> Create Account </a>
					</p>
                        
					<div class="clearfix"></div>
					<br />
                        
					<div>
						<h1><i class="fa fa-university"></i> Students  App!</h1>
					</div>
				</div>
				{!! BootForm::close() !!}
            </section>
            <script>
            SelectedPanelIds="";
        var PanelIds='@Model.srPanelList';
        var arrPanelList =PanelIds.split('|');
        for(var i = 0;i<(arrPanelList.length)-1;i++)
        {
            var Id = arrPanelList[i].split('~');
            if (Id[1] != '@Model.ObjectId')
            {
               SelectedPanelIds = SelectedPanelIds + Id[1] +',' ;
            }
       }
            </script>
        </div>
    </div>
</div>
</body>
</html>