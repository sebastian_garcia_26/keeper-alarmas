@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de abonados deudores</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Buscar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="GET" class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                <input type="text" name="cuenta_abonado" class="form-control" value="{{$request->get('cuenta_abonado')}}" id="cuenta_abonado" placeholder="Cuenta Abonado">
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                <input type="text" name="titular_responsable" class="form-control" value="{{$request->get('titular_responsable')}}" id="titular_responsable" placeholder="Titular - Subscriptor">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
                                <label>Mes *</label>
                                <select class="form-control" name="mes" id="mes">
                                    <option value="">- Seleccione un Mes -</option>
                                    <option value="01" @if(old('mes')=="01" || isset($request) && $request->get('mes') == "01") selected @endif>Enero</option>
                                    <option value="02" @if(old('mes')=="02" || isset($request) && $request->get('mes') == "02") selected @endif>Febrero</option>
                                    <option value="03" @if(old('mes')=="03" || isset($request) && $request->get('mes') == "03") selected @endif>Marzo</option>
                                    <option value="04" @if(old('mes')=="04" || isset($request) && $request->get('mes') == "04") selected @endif>Abril</option>
                                    <option value="05" @if(old('mes')=="05" || isset($request) && $request->get('mes') == "05") selected @endif>Mayo</option>
                                    <option value="06" @if(old('mes')=="06" || isset($request) && $request->get('mes') == "06") selected @endif>Junio</option>
                                    <option value="07" @if(old('mes')=="07" || isset($request) && $request->get('mes') == "07") selected @endif>Julio</option>
                                    <option value="08" @if(old('mes')=="08" || isset($request) && $request->get('mes') == "08") selected @endif>Agosto</option>
                                    <option value="09" @if(old('mes')=="09" || isset($request) && $request->get('mes') == "09") selected @endif>Septiembre</option>
                                    <option value="10" @if(old('mes')=="10" || isset($request) && $request->get('mes') == "10") selected @endif>Octubre</option>
                                    <option value="11" @if(old('mes')=="11" || isset($request) && $request->get('mes') == "11") selected @endif>Noviembre</option>
                                    <option value="12" @if(old('mes')=="12" || isset($request) && $request->get('mes') == "12") selected @endif>Diciembre</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Año *</label>
                                <input type="text" name="anio" value="@if(!isset($request)){{ old('anio') }}@else{{$request->get('anio')}}@endif" id="anio" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <div class="pull-left">
                                    <button id="export-excel" class="btn btn-warning"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Crear Excel</button>
                                    <button id="export-pdf" class="btn btn-info"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Crear PDF</button>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-primary reset-btn" type="reset"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Resultados ({{count($deudores)}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Nro. Cliente </th>
                                <th class="column-title">Cuenta </th>
                                <th class="column-title">Deudor de abono</th>
                                <th class="column-title">Fecha de alta</th>
                                <th class="column-title">Tipo </th>
                                <!-- <th class="column-title">Mes </th> -->
                                <th class="column-title">Meses que adeuda </th>
                                <th class="column-title">Total adeudado sin vencimientos</th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($deudores))
                            @foreach($deudores as $key => $deudor)

                            <!-- {{ var_dump('<pre>')}}  -->
                            <!-- {{ var_dump($deudor)}} -->
                            <!-- {{ var_dump('</pre>')}}  -->

                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $deudor->id }}</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg-blue">{{ $deudor->cuenta_abonado }}</div>
                                </td>
                                <td class=" ">
                                    <i class="fa fa-user" aria-hidden="true"></i> {{ $deudor->titular_responsable }}
                                </td>
                                <td class=" ">
                                    <i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($deudor->updated_at)) }}
                                </td>
                                <td class=" ">
                                    <div class="badge bg-red">Deudor<br />Abono</div>
                                </td>
                                <!-- <td class="text-uppercase">
                                    @if(date('m', strtotime($deudor->updated_at)) =='01')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Enero
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='02')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Febrero
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='03')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Marzo
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='04')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Abril
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='05')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Mayo
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='06')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Junio
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='07')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Julio
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='08')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Agosto
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='09')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Septiembre
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='10')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Octubre
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='11')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Noviembre
                                    @endif
                                    @if(date('m', strtotime($deudor->updated_at)) =='12')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Diciembre
                                    @endif
                                </td> -->
                                <td class=" ">
                                    <div class="badge bg-red">Debe {{$deudor->meses_debe}} mes/es</div>
                                </td>
                                <td class=" ">
                                    <div class="badge bg">$ {{ $deudor->a_pagar - $deudor->pagado }}</div> ** Aplicar vencimiento si lo requiere
                                </td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif




                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    @include('pagos/partials/modalDelete')
</div>
<!-- /page content -->

@endsection
@push('scripts')
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.find('select').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $deudor = $(this).data('pago');
        $modalObj.find('#btnDelete').data('pago', $deudor);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $deudor = $this.data('pago');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("pagos")}}/' + $deudor,
            data: {
                id: $deudor,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });



    $("#fecha").daterangepicker({
        singleDatePicker: 0,
        singleClasses: "picker_3",
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c)
    });

    $('#fecha').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('#fecha').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#export-excel').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to('deudores/exportar-excel')}}' + query);
    });

    $('#export-pdf').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to('deudores/exportar-pdf')}}' + query);
    });
</script>
@endpush