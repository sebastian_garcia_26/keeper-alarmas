  <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content panel-danger">
        <div class="modal-header panel-heading">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirmar</h4>
        </div>
        
        <div class="modal-body">
            Est&aacute; seguro que desea eliminar el pago?
        </div>
        <div class="modal-footer">
          <button type="button" data-student="-1" id="btnDelete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> &nbsp;Eliminar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->