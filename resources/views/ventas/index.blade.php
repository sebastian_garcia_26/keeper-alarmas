@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Listado de todas las ventas de productos</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('ventas/create')}}"><i class="fa fa-plus"></i> Crear Venta</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Buscar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="GET" class="form-horizontal form-label-left input_mask">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                <input type="text" name="id" class="form-control" value="{{$request->get('id')}}" id="id" placeholder="Numero de Resumen">
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                <input value="{{ $request->get('fecha') }}" type="text" class="form-control has-feedback-left" id="fecha" name="fecha" placeholder="Fecha resumen" aria-describedby="fechaResumen">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="fechaResumen" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                <input type="text" name="cuenta_abonado" class="form-control" value="{{$request->get('cuenta_abonado')}}" id="cuenta_abonado" placeholder="Cuenta Comprador Venta">
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                <input type="text" name="titular_responsable" class="form-control" value="{{$request->get('titular_responsable')}}" id="titular_responsable" placeholder="Titular - Subscriptor">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                <select name="pagado" id="pagado" class="form-control">
                                    <option value=""> - Estado - </option>
                                    <option value="1" @if($request->get('pagado') == '1') selected @endif> Pagado </option>
                                    <option value="0" @if($request->get('pagado') == '0') selected @endif> No pagado </option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary reset-btn" type="reset"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Limpiar</button>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;&nbsp;Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Resultados ({{$ventas->total()}})</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Nro. Venta </th>
                                <th class="column-title">Persona</th>
                                <th class="column-title">Titular - Comprador</th>
                                <th class="column-title">Nombre del producto</th>
                                <th class="column-title">Tipo </th>
                                <th class="column-title">Total </th>
                                <th class="column-title">Fecha </th>
                                <th class="column-title">Mes </th>
                                <th class="column-title">Pagado </th>
                                <th class="column-title no-link last"><span class="nobr"><i class="fa fa-cog" aria-hidden="true"></i></span></th>

                            </tr>
                        </thead>

                        <tbody>
                            @if(count($ventas))
                            @foreach($ventas as $key => $venta)
                            <tr class="@if($key % 2 == 0) even @else odd @endif pointer">
                                <td class=" ">
                                    <div class="badge bg-blue-sky">{{ $venta->id }}</div>
                                </td>
                                <td class=" ">@if($venta->cliente_id != null) <div class="badge bg-orange">Cliente</div> @else <div class="badge bg-purple">Comprador</div> @endif</td>
                                <td class=" "><i class="fa fa-user"></i> @if($venta->cliente_id != null){{ $venta->cli_titu}} @else {{ $venta->com_titu}} @endif</td>
                                <td class=" "><i class="fa fa-product-hunt"></i> {{ $venta->name }}</td>
                                <td class=" ">@if($venta->venta) <div class="badge bg-green">Venta<br />Producto</div> @else <div class="badge bg-red">No</div>@endif</td>
                                <td class=" ">
                                    <div class="badge">${{ $venta->monto }}</div>
                                </td>
                                <td class=" "><i class="fa fa-calendar" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($venta->fecha)) }}</td>
                                <td class="text-uppercase">
                                    @if(date('m', strtotime($venta->fecha)) =='01')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Enero
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='02')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Febrero
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='03')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Marzo
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='04')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Abril
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='05')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Mayo
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='06')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Junio
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='07')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Julio
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='08')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Agosto
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='09')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Septiembre
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='10')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Octubre
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='11')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Noviembre
                                    @endif
                                    @if(date('m', strtotime($venta->fecha)) =='12')
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i> Diciembre
                                    @endif
                                </td>
                                <td class=" ">@if($venta->pagado) <div class="badge bg-green">Si</div> @else <div class="badge bg-red">No</div>@endif</td>
                                <td class=" last">
                                    <!-- <a class="btn btn-primary btn-xs" href="{{URL::to('ventas/'.$venta->id.'/edit')}}"><i class="fa fa-edit"></i></a> -->
                                    <a class="btn btn-danger btn-xs delete-modal-trigger" data-venta="{{$venta->id}}" href="#"><i class="fa fa-trash"></i></a>
                                    <a class="btn btn-warning btn-xs" href="{{URL::to('ventas/'.$venta->id.'/pdf')}}" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                                    <a class="btn btn-success btn-xs" href="{{URL::to('ventas/'.$venta->id.'/imprimir')}}" target="_blank"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan="12">
                                    <div class="alert alert-warning"><i class="fa fa-warning"></i> &nbsp;&nbsp;No existen registros para la b&uacute;squeda solicitada.</div>
                                </td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                @if(count($ventas))
                <div class="pull-right">
                    {{ $ventas->render()}}
                </div>
                @endif
            </div>

        </div>
    </div>
    @include('ventas/partials/modalDelete')
</div>
<!-- /page content -->

@endsection
@push('scripts')
<script>
    $('.reset-btn').on('click', function(e) {
        var $formObj = $(this).closest('form');
        $formObj.find('input').each(function(index) {
            $(this).val('');
        });
        $formObj.find('select').each(function(index) {
            $(this).val('');
        });
        $formObj.submit();
    });

    $('.delete-modal-trigger').on('click', function(e) {
        var $modalObj = $('#delete-modal');
        $venta = $(this).data('venta');
        $modalObj.find('#btnDelete').data('venta', $venta);
        $modalObj.modal('show');
    });

    $('#btnDelete').on('click', function(e) {
        var $this = $(this);
        $venta = $this.data('venta');
        $this.text('Eliminando..');
        $.ajax({
            type: "DELETE",
            url: '{{URL::to("resumenes")}}/' + $venta,
            data: {
                id: $venta,
                _token: "{{ csrf_token() }}"
            },
            success: function(msg) {
                location.reload();
            }
        });
    });



    $("#fecha").daterangepicker({
        singleDatePicker: 0,
        singleClasses: "picker_3",
        autoUpdateInput: false,
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c)
    });

    $('#fecha').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('#fecha').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#export-excel').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to("ventas/exportar-excel")}}' + query);
    });

    $('#export-pdf').on('click', function() {
        var query = window.location.search;
        window.open('{{URL::to("ventas/exportar-pdf")}}' + query);
    });
</script>
@endpush