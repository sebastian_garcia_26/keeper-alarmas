@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
            <div class="title_left">
               <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Generar Resúmenes Venta Productos</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{URL::to('ventas')}}"><i class="fa fa-list"></i> Ver Listado</a>
            </div>

        </div>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4><i class="fa fa-warning"></i> Error</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>    
            </div>    
        @endif

        @if (session()->has('info'))
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-info alert-dismissible fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <i class="fa fa-info-circle"></i>
                    {{ session()->get('info') }}
                </div>
            </div>    
        </div>    
        @endif
        
        @if (session()->has('message'))
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <i class="fa fa-check-circle-o"></i>
                        {{ session()->get('message') }}
                    </div>
                </div>    
            </div>    
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        {{Form::open(array('url' => '/ventas/generar', 'files' => true, 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                            <div class="row">
                                <div class="col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-md-3 col-sm-3 col-xs-12 has-feedback">
                                    <label>Mes *</label>
                                    <select class="form-control" name="mes" id="mes">
                                        <option value="">- Seleccione un Mes -</option>
                                        <option value="01" @if(old('mes') == "01") selected @endif>Enero</option>
                                        <option value="02" @if(old('mes') == "02") selected @endif>Febrero</option>
                                        <option value="03" @if(old('mes') == "03") selected @endif>Marzo</option>
                                        <option value="04" @if(old('mes') == "04") selected @endif>Abril</option>
                                        <option value="05" @if(old('mes') == "05") selected @endif>Mayo</option>
                                        <option value="06" @if(old('mes') == "06") selected @endif>Junio</option>
                                        <option value="07" @if(old('mes') == "07") selected @endif>Julio</option>
                                        <option value="08" @if(old('mes') == "08") selected @endif>Agosto</option>
                                        <option value="09" @if(old('mes') == "09") selected @endif>Septiembre</option>
                                        <option value="10" @if(old('mes') == "10") selected @endif>Octubre</option>
                                        <option value="11" @if(old('mes') == "11") selected @endif>Noviembre</option>
                                        <option value="12" @if(old('mes') == "12") selected @endif>Diciembre</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                    <label>Año *</label>
                                    <input  type="text" name="anio" value="{{ old('anio') }}" id="anio" class="form-control" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-md-3 col-sm-3 col-xs-12 has-feedback">
                                    <label>Fecha primer vencimiento *</label>
                                    <input value="{{ old('primer_vencimiento') }}" type="text" class="form-control has-feedback-left" required="required" id="primer_vencimiento" name="primer_vencimiento"  aria-describedby="primerVencimiento">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="primerVencimiento" class="sr-only">(success)</span>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                    <label>Fecha segundo vencimiento *</label>
                                    <input value="{{ old('segundo_vencimiento') }}" type="text" class="form-control has-feedback-left" required="required" id="segundo_vencimiento" name="segundo_vencimiento"  aria-describedby="segundoVencimiento">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="segundoVencimiento" class="sr-only">(success)</span>
                                </div>
                            </div>  

                            <div class="form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">Generar</button>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{ Form::close() }}
                    </div>
                      
                </div>
            </div>  
        </div>    


    </div>
    <!-- /page content -->
@endsection
@push('scripts')
<script>

$("#primer_vencimiento,#segundo_vencimiento").daterangepicker({
    singleDatePicker: !0,
    singleClasses: "picker_3",
    locale: {
        format: "DD/MM/YYYY",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "May",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(a, b, c) {
    console.log(a.toISOString(), b.toISOString(), c)
})
</script> 
@endpush