@extends('layouts.blank')

@push('stylesheets')
<!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Editar Comprador</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to('compradores')}}"><i class="fa fa-list"></i> Ver Listado</a>
            <a class="btn btn-primary" href="{{URL::to('compradores/create')}}"><i class="fa fa-plus"></i> Crear Comprador</a>
        </div>

    </div>
    @if (count($errors) > 0)
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4><i class="fa fa-warning"></i> Error</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    @if (session()->has('message'))
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-check-circle-o"></i>
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Datos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    {{Form::model($compradore, array('route' => array('compradores.update', $compradore->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal form-label-left input_mask' ))}}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Fecha Ingreso *</label>
                            <input value="{{ date_format(\DateTime::createFromFormat("Y-m-d", trim($compradore->fecha_alta)), 'd/m/Y') }}" type="text" class="form-control has-feedback-left" required="required" id="fecha_ingreso" name="fecha_ingreso" aria-describedby="fechaIngreso">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            <span id="fechaIngreso" class="sr-only">(success)</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label>Cuenta comprador *</label>
                            <input style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" value="{{ $compradore->cuenta_abonado }}" type="text" name="cuenta_abonado" id="cuenta_abonado" class="form-control">
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Abono Mensual *</label>
                            <select name="abono_id" id="abono_id" class="form-control">
                                @foreach(App\Abono::all() as $abono)
                                <option value="{{ $abono->id }}" @if($compradore->abono_id == $abono->id) selected @endif>{{ $abono->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div class="divider"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Titular - Subscriptor *</label>
                            <input value="{{ $compradore->titular_responsable }}" type="text" name="titular_responsable" id="titular_responsable" class="form-control">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Nombre - Razón Social *</label>
                            <input value="{{ $compradore->nombre_razon_social }}" type="text" name="nombre_razon_social" id="nombre_razon_social" class="form-control">
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Documento</label>
                            <input value="{{ $compradore->documento }}" type="text" name="documento" id="documento" class="form-control">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>CUIT </label>
                            <input value="{{ $compradore->cuit }}" type="text" name="cuit" id="cuit" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Email</label>
                            <input value="{{ $compradore->email }}" type="email" id="email" name="email" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div class="divider"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Ciudad *</label>
                            <input value="{{ $compradore->ciudad }}" type="text" name="ciudad" id="ciudad" class="form-control">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Provincia *</label>
                            <input value="{{ $compradore->provincia }}" type="text" name="provincia" id="provincia" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                            <label>Calle *</label>
                            <input value="{{ $compradore->calle }}" type="text" name="calle" id="calle" class="form-control">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Nro *</label>
                            <input value="{{ $compradore->numero }}" type="text" name="numero" id="numero" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Piso</label>
                            <input value="{{ $compradore->piso }}" type="text" name="piso" id="piso" class="form-control">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Depto</label>
                            <input value="{{ $compradore->depto }}" type="text" name="depto" id="depto" class="form-control">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Código Postal *</label>
                            <input value="{{ $compradore->codigo_postal }}" type="text" name="codigo_postal" id="codigo_postal" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <label>Observaciones</label>
                            <input value="{{ $compradore->resto }}" type="text" name="resto" id="resto" class="form-control">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Teléfono</label>
                            <input value="{{ $compradore->telefono }}" type="text" name="telefono" id="telefono" class="form-control">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <label>Celular</label>
                            <input value="{{ $compradore->celular }}" type="text" name="celular" id="celular" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div class="divider"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $compradore->id }}">
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>


</div>
<!-- /page content -->
@endsection
@push('scripts')
<script>
    $("#fecha_ingreso").daterangepicker({
        singleDatePicker: !0,
        singleClasses: "picker_3",
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(a, b, c) {
        console.log(a.toISOString(), b.toISOString(), c)
    })
</script>
@endpush