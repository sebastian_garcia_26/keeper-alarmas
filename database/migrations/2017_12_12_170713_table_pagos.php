<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto_tarjeta',8,2);
            $table->float('monto_efectivo',8,2);
            $table->float('monto_debito',8,2);
            $table->float('monto_cheque',8,2);
            $table->string('observacion');
            $table->date('fecha');
            $table->integer('resumen_id')->unsigned();
            $table->foreign('resumen_id')->references('id')->on('resumenes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
