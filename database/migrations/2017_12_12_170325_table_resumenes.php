<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableResumenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumenes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto',8,2);
            $table->string('observacion');
            $table->date('primer_vencimiento');
            $table->date('segundo_vencimiento');
            $table->date('fecha');
            $table->tinyInteger('pagado');
            $table->tinyInteger('venta');
            $table->integer('cliente_id')->nullable()->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->softDeletes();
            $table->timestamps();
        });
        
        DB::update("ALTER TABLE resumenes AUTO_INCREMENT = 1150;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumenes');
    }
}
