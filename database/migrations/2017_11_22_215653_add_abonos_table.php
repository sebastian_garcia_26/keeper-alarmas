<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAbonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('price',8,2);
            $table->float('iva',8,2);
            $table->softDeletes();
            $table->timestamps();
        });
        
        DB::update("INSERT INTO `abonos` (`id`,  `name`, `price`, `iva`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Abonados con IVA', 350.00, 0.00, NULL, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abonos');
    }
}
