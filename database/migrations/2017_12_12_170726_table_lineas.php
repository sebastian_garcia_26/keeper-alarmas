<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableLineas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lineas', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto',8,2);
            $table->string('descripcion');
            $table->integer('cantidad');
            $table->integer('resumen_id')->unsigned();
            $table->foreign('resumen_id')->references('id')->on('resumenes');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lineas');
    }
}
