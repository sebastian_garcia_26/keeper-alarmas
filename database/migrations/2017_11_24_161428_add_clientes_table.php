<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cuenta_abonado');
            $table->string('nombre_razon_social');
            $table->string('titular_responsable');
            $table->string('calle');
            $table->string('numero');
            $table->string('piso');
            $table->string('depto');
            $table->string('resto');
            $table->string('codigo_postal');
            $table->string('telefono');
            $table->string('celular');
            $table->string('email');
            $table->string('documento');
            $table->string('cuit');
            $table->string('ciudad');
            $table->string('provincia');
            $table->date('fecha_alta');
            $table->date('fecha_baja');
            $table->integer('abono_id')->unsigned();
            $table->foreign('abono_id')->references('id')->on('abonos');
            $table->softDeletes();
            $table->timestamps();
        });
        
        DB::update("ALTER TABLE clientes AUTO_INCREMENT = 1257;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
