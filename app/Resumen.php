<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resumen extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'resumenes';

    function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id')->withTrashed();
    }

    function comprador()
    {
        return $this->belongsTo(Compradore::class, 'comprador_id')->withTrashed();
    }

    /**
     * Obtener lineas del resumen
     */
    public function lineas()
    {
        return $this->hasMany('App\Linea')->get();
    }

    /**
     * Obtener pagos del resumen
     */
    public function pagos()
    {
        return $this->hasMany('App\Pago')->get();
    }

}
