<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venta extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ventas';

    function compradore()
    {
        return $this->belongsTo(Compradore::class, 'compradore_id')->withTrashed();
    }

    function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id')->withTrashed();
    }

    /**
     * Obtener series del venta
     */
    public function series()
    {
        return $this->hasMany('App\Serie')->get();
    }

    /**
     * Obtener pagos del venta
     */
    public function pagos()
    {
        return $this->hasMany('App\Pago')->get();
    }
}
