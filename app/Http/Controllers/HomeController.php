<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\CobroRepository;
use App\Repositories\PagoRepository;
use App\Repositories\VentaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repoCobros = new CobroRepository();
        $repoPagos = new PagoRepository();

        $current_date =  date('d/m/Y');

        // var_dump($current_date);

        $cobros = $repoCobros->searchIndex(['fecha' => $current_date . ' - ' . $current_date]);
   
        // echo '<pre>';
        // var_dump($cobros);
        // echo '</pre>';


        $pagos = $repoPagos->searchIndex(['fecha' => $current_date . ' - ' . $current_date]);

        // echo '<pre>';
        // var_dump($pagos[0]);
        // echo '</pre>';

        return view('home')->with('cobros', $cobros->appends(Input::except('page')))
            ->with('pagos', $pagos->appends(Input::except('page')));
    }
}
