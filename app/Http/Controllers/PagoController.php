<?php

namespace App\Http\Controllers;
use App\Repositories\ResumenRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\PagoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Pago;
class PagoController extends Controller
{

    private $repoResumen;

    private $repoCliente;

    private $repoPago;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ResumenRepository $repoResumen, ClienteRepository $repoCliente,PagoRepository $repoPago)
    {
        $this->repoResumen = $repoResumen;
        $this->repoCliente = $repoCliente;
        $this->repoPago = $repoPago;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagos = $this->repoPago->searchIndex($request->all());
        return view('pagos.index')
                ->with('pagos',$pagos->appends(Input::except('page')))
                ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pagos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'fecha.required' => 'La fecha del pago es obligatoria.',
            'resumen_id.integer' => 'El número del resumen debe ser un número entero positivo.',
            'monto_efectivo.numeric' => 'El pago en efectivo debe ser numérico.',
            'monto_debito.numeric' => 'El pago con debito debe ser numérico.',
            'monto_cheque.numeric' => 'El pago con transferencia bancaria debe ser numérico.',
            'monto_tarjeta.numeric' => 'El pago con tarjeta debe ser numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'monto_efectivo' => 'numeric',
            'monto_debito' => 'numeric',
            'monto_cheque' => 'numeric',
            'monto_tarjeta' => 'numeric',
            'resumen_id' => 'integer',
        ], $messages);

        $resumen = \App\Resumen::find($request->resumen_id);

        if(!$resumen){
            return redirect()->to($this->getRedirectUrl())
                        ->withInput($request->input())
                        ->withErrors(['El número de resumen no es válido.']);
        }else{
            try {
                DB::beginTransaction();
                $pago = new Pago;
                $pago->resumen_id = $request->resumen_id;
                $pago->observacion = $request->observacion;
                $pago->monto_efectivo = $request->monto_efectivo;
                $pago->monto_debito = $request->monto_debito;
                $pago->monto_cheque = $request->monto_cheque;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
                $pago->save();

                $resumen = \App\Resumen::findOrFail($request->resumen_id);

                $montoPagos = 0;
                foreach ($resumen->pagos() as $pagor) {
                    $montoPagos += $pagor->monto_efectivo + $pagor->monto_debito + $pagor->monto_tarjeta + $pagor->monto_cheque;
                }

                $montoLineas = 0;
                foreach ($resumen->lineas() as $linea) {
                    $montoLineas += $linea->monto;
                }

                if($montoLineas > $montoPagos){
                    $resumen->pagado = 0;
                    $resumen->save();
                }else{
                    $resumen->pagado = 1;
                    $resumen->save();
                }


                DB::commit();
                return redirect()->back()->with('message', 'Se ha guardado el pago de manera exitosa.');
            } catch ( \Exception $e){
                DB::rollback();
                return redirect()->back()
                        ->withErrors([ 'Hubo un error inesperado al intentar guardar el pago, intente nuevamente y si el problema persiste contacte al desarrollador.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pago = $this->repoPago->find($id);

        return view('pagos.edit')->with('pago',$pago);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'fecha.required' => 'La fecha del pago es obligatoria.',
            'monto_efectivo.numeric' => 'El pago en efectivo debe ser numérico.',
            'monto_debito.numeric' => 'El pago con debito debe ser numérico.',
            'monto_cheque.numeric' => 'El pago con transferencia bancaria debe ser numérico.',
            'monto_tarjeta.numeric' => 'El pago con tarjeta debe ser numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'monto_efectivo' => 'numeric',
            'monto_debito' => 'numeric',
            'monto_cheque' => 'numeric',
            'monto_tarjeta' => 'numeric',
        ], $messages);

        try {
                DB::beginTransaction();
                $pago = $this->repoPago->find($id);
                $pago->observacion = $request->observacion;
                $pago->monto_efectivo = $request->monto_efectivo;
                $pago->monto_debito = $request->monto_debito;
                $pago->monto_cheque = $request->monto_cheque;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
                $pago->save();

                $resumen = \App\Resumen::findOrFail($pago->resumen_id);
                $montoPagos = 0;
                foreach ($resumen->pagos() as $pagor) {
                    $montoPagos += $pagor->monto_efectivo + $pagor->monto_debito + $pagor->monto_tarjeta + $pagor->monto_cheque;
                }

                $montoLineas = 0;
                foreach ($resumen->lineas() as $linea) {
                    $montoLineas += $linea->monto;
                }

                if($montoLineas > $montoPagos){
                    $resumen->pagado = 0;
                    $resumen->save();
                }else{
                    $resumen->pagado = 1;
                    $resumen->save();
                }


                DB::commit();
                return redirect()->back()->with('message', 'Se ha guardado el pago de manera exitosa.');
            } catch ( \Exception $e){
                DB::rollback();
                return redirect()->back()
                        ->withErrors([ 'Hubo un error inesperado al intentar guardar el pago, intente nuevamente y si el problema persiste contacte al desarrollador.']);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repoPago->deletePago($id);
    }

    public function indexDeudores(Request $request)
    {
        $deudores = $this->repoPago->searchDeudores($request->all());
        return view('pagos.index-deudores')
                ->with('deudores',$deudores)
                ->with('request',$request);
    }
}
