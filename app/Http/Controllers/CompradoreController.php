<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\CompradoreRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class CompradoreController extends Controller
{

    private $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CompradoreRepository $repo)
    {

        $this->repo = $repo;
    }

    /**
     * Mostrar la lista de compradores es y permitir la búsqueda.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $compradores = $this->repo->search($request->all());

        return view('compradores.index')
            ->with('compradores', $compradores->appends(Input::except('page')))
            ->with('request', $request);
    }


    /**
     * Mostrar el formulario para agregar un Compradore .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('compradores.create');
    }

    /**
     * Show compradore edit form
     */
    public function edit($id)
    {
        $compradore = $this->repo->find($id);

        return view('compradores.edit')->with('compradore', $compradore);
    }

    public function update(Request $request)
    {

        $messages = [
            'fecha_ingreso.required' => 'La fecha de ingreso es obligatoria.',
            'cuenta_abonado.required' => 'La cuenta de abonado es obligatoria.',
            'cuenta_abonado.unique' => 'La cuenta de abonado no puede repetirse.',
            'abono_id.required' => 'El abono mensual es obligatorio.',
            'nombre_razon_social.required' => 'El nombre o razon social es obligatorio.',
            'titular_responsable.required' => 'El titular o subscriptor es obligatorio.',
            'calle.required' => 'La calle del domicilio es obligatoria.',
            'numero.required' => 'El numero del domicilio es obligatorio.',
            'ciudad.required' => 'La ciudad del comprador es obligatoria.',
            'provincia.required' => 'La provincia del comprador es obligatorio.',
            'codigo_postal.required' => 'El código postal es obligatorio.',
        ];

        $this->validate($request, [
            'cuenta_abonado' => 'required|unique:compradores,cuenta_abonado,' . $request->get('id') . ',id,deleted_at,NULL',
            'fecha_ingreso' => 'required',
            'abono_id' => 'required',
            'nombre_razon_social' => 'required',
            'calle' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'numero' => 'required',
            'codigo_postal' => 'required',
            'titular_responsable' => 'required',
        ], $messages);

        try {
            $this->repo->saveCompradore($request, $request->get('id'));
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['El comprador no pudo ser actualizado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'El comprador ha sido actualizado exitosamente.');
    }

    /**
     * Delete compradore
     */
    public function destroy($id)
    {
        $this->repo->deleteCompradore($id);
    }


    /**
     * Store a new compradore  post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $messages = [
            'fecha_ingreso.required' => 'La fecha de ingreso es obligatoria.',
            'cuenta_abonado.required' => 'La cuenta de abonado es obligatoria.',
            'cuenta_abonado.unique' => 'La cuenta de abonado no puede repetirse.',
            'abono_id.required' => 'El abono mensual es obligatorio.',
            'nombre_razon_social.required' => 'El nombre o razon social es obligatorio.',
            'calle.required' => 'La calle del domicilio es obligatoria.',
            'numero.required' => 'El numero del domicilio es obligatorio.',
            'ciudad.required' => 'La ciudad del comprador es obligatoria.',
            'provincia.required' => 'La provincia del comprador es obligatorio.',
            'codigo_postal.required' => 'El código postal es obligatorio.',
            'titular_responsable.required' => 'El titular o subscriptor es obligatorio.',
        ];

        $this->validate($request, [
            'cuenta_abonado' => 'required|unique:compradores,cuenta_abonado,NULL,id,deleted_at,NULL',
            'fecha_ingreso' => 'required',
            'abono_id' => 'required',
            'nombre_razon_social' => 'required',
            'calle' => 'required',
            'numero' => 'required',
            'codigo_postal' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'titular_responsable' => 'required',
        ], $messages);

        try {
            $this->repo->saveCompradore($request);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['El comprador no pudo ser creado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'Comprador creado exitosamente.');
    }
}
