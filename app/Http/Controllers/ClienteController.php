<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\ClienteRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    
    private $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClienteRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Mostrar la lista de clientes es y permitir la búsqueda.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = $this->repo->search($request->all());
        
        return view('clientes.index')
                    ->with('clientes',$clientes->appends(Input::except('page')))
                    ->with('request',$request);
    }


    /**
     * Mostrar el formulario para agregar un Cliente .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Show cliente edit form
     */
    public function edit($id)
    {
        $cliente = $this->repo->find($id);

        return view('clientes.edit')->with('cliente',$cliente);
    }

    public function update(Request $request)
    {
        
        $messages = [
            'fecha_ingreso.required' => 'La fecha de ingreso es obligatoria.',
            'cuenta_abonado.required' => 'La cuenta de abonado es obligatoria.',
            'cuenta_abonado.unique' => 'La cuenta de abonado no puede repetirse.',
            'abono_id.required' => 'El abono mensual es obligatorio.',
            'nombre_razon_social.required' => 'El nombre o razon social es obligatorio.',
            'titular_responsable.required' => 'El titular o subscriptor es obligatorio.',
            'calle.required' => 'La calle del domicilio es obligatoria.',
            'numero.required' => 'El numero del domicilio es obligatorio.',
            'ciudad.required' => 'La ciudad del cliente es obligatoria.',
            'provincia.required' => 'La provincia del cliente es obligatorio.',
            'codigo_postal.required' => 'El código postal es obligatorio.',
        ];

        $this->validate($request, [
            'cuenta_abonado' => 'required|unique:clientes,cuenta_abonado,'.$request->get('id').',id,deleted_at,NULL',
            'fecha_ingreso' => 'required',
            'abono_id' => 'required',
            'nombre_razon_social' => 'required',
            'calle' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'numero' => 'required',
            'codigo_postal' => 'required',
            'titular_responsable' => 'required',
                ], $messages);

        try{
            $this->repo->saveCliente($request,$request->get('id'));
        } catch ( \Exception $e){
            return redirect()->back()->withErrors([ 'El cliente no pudo ser actualizado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'El cliente ha sido actualizado exitosamente.');
    }

    /**
     * Delete cliente
     */
    public function destroy($id)
    {
        $this->repo->deleteCliente($id);
    }


    /**
     * Store a new cliente  post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $messages = [
            'fecha_ingreso.required' => 'La fecha de ingreso es obligatoria.',
            'cuenta_abonado.required' => 'La cuenta de abonado es obligatoria.',
            'cuenta_abonado.unique' => 'La cuenta de abonado no puede repetirse.',
            'abono_id.required' => 'El abono mensual es obligatorio.',
            'nombre_razon_social.required' => 'El nombre o razon social es obligatorio.',
            'calle.required' => 'La calle del domicilio es obligatoria.',
            'numero.required' => 'El numero del domicilio es obligatorio.',
            'ciudad.required' => 'La ciudad del cliente es obligatoria.',
            'provincia.required' => 'La provincia del cliente es obligatorio.',
            'codigo_postal.required' => 'El código postal es obligatorio.',
            'titular_responsable.required' => 'El titular o subscriptor es obligatorio.',
        ];
        
        $this->validate($request, [
            'cuenta_abonado' => 'required|unique:clientes,cuenta_abonado,NULL,id,deleted_at,NULL',
            'fecha_ingreso' => 'required',
            'abono_id' => 'required',
            'nombre_razon_social' => 'required',
            'calle' => 'required',
            'numero' => 'required',
            'codigo_postal' => 'required',
            'ciudad' => 'required',
            'provincia' => 'required',
            'titular_responsable' => 'required',
        ], $messages);

        try{
            $this->repo->saveCliente($request);
        } catch ( \Exception $e){
            return redirect()->back()->withErrors([ 'El cliente no pudo ser creado, algo ocurrió.']);
        }
        
        return redirect()->back()->with('message', 'Cliente creado exitosamente.');
    }

}