<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\ClienteRepository;
use App\Repositories\AbonoRepository;
use App\Repositories\PagoRepository;

use App\Repositories\CompradoreRepository;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ExcelController extends Controller
{

    private $clienteRepo;
    private $abonoRepo;
    private $pagoRepo;
    private $compradoreRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClienteRepository $clienteRepo, AbonoRepository $abonoRepo, PagoRepository $pagoRepo, CompradoreRepository $compradoreRepo)
    {
        $this->clienteRepo = $clienteRepo;
        $this->abonoRepo = $abonoRepo;
        $this->pagoRepo = $pagoRepo;
        $this->compradoreRepo = $compradoreRepo;
    }

    public function exportarClientes(Request $request)
    {
        $params = $request->all();

        $clientes = $this->clienteRepo->get($params);

        \Excel::create('Clientes', function ($excel) use ($clientes, $params) {

            $excel->sheet('Clientes', function ($sheet) use ($clientes, $params) {
                //Fecha de generación
                $sheet->appendRow(['Generado ' . date('d/m/Y H:i')]);

                $parseSearch = [
                    'id' => 'ID',
                    'cuenta_abonado' => 'Cuenta Abonado', 'nombre_razon_social' => 'Nombre - Razón Social', 'abono_id' => 'Abono Mensual',
                    'fecha_ingreso' => 'Fecha de Ingreso entre', 'titular_responsable' => 'Titular - Subscriptor'
                ];

                foreach ($params as $column => $value) {
                    if ($value != '' && $column != 'page') {
                        if ($column == 'abono_id') {
                            $abono = $this->abonoRepo->get(['id' => $value]);
                            $sheet->appendRow([$parseSearch[$column], $abono[0]->name]);
                        } else {
                            $sheet->appendRow([$parseSearch[$column], $value]);
                        }
                    }
                }

                $sheet->appendRow(['']);
                $sheet->appendRow([
                    'ID', 'Cuenta Abonado', 'Nombre - Razón Social', 'Titular - Subscriptor',
                    'Calle', 'Numero', 'Piso', 'Depto', 'Observaciones', 'Código Postal', 'Teléfono', 'Celular', 'Email', 'Documento', 'CUIT', 'Fecha Ingreso', 'Abono', 'Ciudad', 'Provincia'
                ]);

                foreach ($clientes as $index => $cliente) {
                    $fecha_ingreso = \DateTime::createFromFormat('Y-m-d', $cliente->fecha_alta);
                    $fecha_ingreso = date_format($fecha_ingreso, "d/m/Y");
                    $sheet->appendRow([
                        $cliente->id, $cliente->cuenta_abonado,
                        $cliente->nombre_razon_social, $cliente->titular_responsable,
                        $cliente->calle, $cliente->numero, $cliente->piso, $cliente->depto, $cliente->resto,
                        $cliente->codigo_postal, $cliente->telefono, $cliente->celular, $cliente->email,
                        $cliente->documento, $cliente->cuit, $fecha_ingreso, $cliente->abono->name, $cliente->ciudad, $cliente->provincia
                    ]);
                }
            });
        })->export('xlsx');
    }

    public function exportarPagos(Request $request)
    {
        $params = $request->all();
        $pagos = $this->pagoRepo->get($params);

        \Excel::create('Pagos', function ($excel) use ($pagos, $params) {

            $excel->sheet('Pagos', function ($sheet) use ($pagos, $params) {
                //Fecha de generación
                $sheet->appendRow(['Generado ' . date('d/m/Y H:i')]);

                $parseSearch = [
                    'id' => 'Número de Resumen',
                    'cuenta_abonado' => 'Cuenta Abonado',
                    'fecha' => 'Fecha de pago entre', 'titular_responsable' => 'Titular - Subscriptor'
                ];

                foreach ($params as $column => $value) {
                    if ($value != '' && $column != 'page') {

                        $sheet->appendRow([$parseSearch[$column], $value]);
                    }
                }

                $sheet->appendRow(['']);
                $sheet->appendRow([
                    'Nro. Resumen', 'Cuenta Abonado', 'Titular - Subscriptor',
                    'Total', 'Fecha', 'Pago en efectivo', 'Pago con transferencia bancaria', 'Pago con debito', 'Pago con tarjeta'
                ]);
                $total = 0;
                foreach ($pagos as $index => $pago) {
                    $fecha_ingreso = \DateTime::createFromFormat('Y-m-d', $pago->fecha);
                    $fecha_ingreso = date_format($fecha_ingreso, "d/m/Y");
                    $total += $pago->monto_efectivo + $pago->monto_cheque + $pago->monto_debito + $pago->monto_tarjeta;
                    $sheet->appendRow([
                        $pago->resumen_id,
                        $pago->resumen->cliente->cuenta_abonado, $pago->resumen->cliente->titular_responsable,
                        $pago->monto_efectivo + $pago->monto_cheque + $pago->monto_debito + $pago->monto_tarjeta,
                        $fecha_ingreso, $pago->monto_efectivo, $pago->monto_cheque, $pago->monto_debito, $pago->monto_tarjeta
                    ]);
                }
                $sheet->appendRow([
                    '',
                    '', '',
                    '',
                    '', '', '', 'Total: ', $total
                ]);
            });
        })->export('xlsx');
    }

    public function exportarDeudores(Request $request)
    {
        $params = $request->all();
        $deudores = $this->pagoRepo->searchDeudores($params);

        // die('holaaaaaaaaaaaaaaa');

        \Excel::create('Deudores', function ($excel) use ($deudores, $params) {

            $excel->sheet('Deudores', function ($sheet) use ($deudores, $params) {
                //Fecha de generación
                $sheet->appendRow(['Generado ' . date('d/m/Y H:i')]);

                $parseSearch = [
                    'cuenta_abonado' => 'Cuenta Abonado',
                    'titular_responsable' => 'Titular - Subscriptor'
                ];

                foreach ($params as $column => $value) {
                    if ($value != '' && $column != 'page') {

                        $sheet->appendRow([$parseSearch[$column], $value]);
                    }
                }

                $sheet->appendRow(['']);
                $sheet->appendRow([
                    'Nro. Cliente', 'Cuenta Abonado', 'Titular - Subscriptor',
                    'Dirección', 'Teléfono', 'Debe'
                ]);
                $total = 0;
                foreach ($deudores as $index => $deudor) {

                    // var_dump('<pre>');
                    // var_dump($deudores);
                    // var_dump('</pre>');
                    // die();


                    $total += $deudor->a_pagar - $deudor->pagado;
                    $sheet->appendRow([
                        $deudor->id,
                        $deudor->cuenta_abonado,
                        $deudor->titular_responsable,
                        $deudor->calle . ' ' . $deudor->numero,
                        $deudor->telefono . ' ' . $deudor->celular,
                        $deudor->a_pagar - $deudor->pagado
                    ]);
                }
                $sheet->appendRow([
                    '', '',
                    '',
                    '', 'Total deuda: ', $total
                ]);
            });
        })->export('xlsx');
    }

    public function exportarDeudoresProductos(Request $request)
    {
        $params = $request->all();
        $deudores = $this->pagoRepo->searchDeudoresProductos($params);

        // die('holaaaaaaaaaaaaaaa');

        \Excel::create('Deudores', function ($excel) use ($deudores, $params) {

            $excel->sheet('Deudores', function ($sheet) use ($deudores, $params) {
                //Fecha de generación
                $sheet->appendRow(['Generado ' . date('d/m/Y H:i')]);

                $parseSearch = [
                    'cuenta_abonado' => 'Cuenta Abonado',
                    'titular_responsable' => 'Titular - Subscriptor'
                ];

                foreach ($params as $column => $value) {
                    if ($value != '' && $column != 'page') {

                        $sheet->appendRow([$parseSearch[$column], $value]);
                    }
                }

                $sheet->appendRow(['']);
                $sheet->appendRow([
                    'Nro. Cliente', 'Cuenta Abonado', 'Titular - Subscriptor',
                    'Dirección', 'Teléfono', 'Debe'
                ]);
                $total = 0;
                foreach ($deudores as $index => $deudor) {

                    // var_dump('<pre>');
                    // var_dump($deudores);
                    // var_dump('</pre>');
                    // die();


                    $total += $deudor->a_pagar - $deudor->pagado;
                    $sheet->appendRow([
                        $deudor->id,
                        $deudor->cuenta_abonado,
                        $deudor->titular_responsable,
                        $deudor->calle . ' ' . $deudor->numero,
                        $deudor->telefono . ' ' . $deudor->celular,
                        $deudor->a_pagar - $deudor->pagado
                    ]);
                }
                $sheet->appendRow([
                    '', '',
                    '',
                    '', 'Total deuda: ', $total
                ]);
            });
        })->export('xlsx');
    }

    public function exportarCompradores(Request $request)
    {
        $params = $request->all();

        $compradores = $this->compradoreRepo->get($params);

        \Excel::create('Compradores', function ($excel) use ($compradores, $params) {

            $excel->sheet('Compradores', function ($sheet) use ($compradores, $params) {
                //Fecha de generación
                $sheet->appendRow(['Generado ' . date('d/m/Y H:i')]);

                $parseSearch = [
                    'id' => 'ID',
                    'cuenta_abonado' => 'Cuenta Abonado', 'nombre_razon_social' => 'Nombre - Razón Social', 'abono_id' => 'Abono Mensual',
                    'fecha_ingreso' => 'Fecha de Ingreso entre', 'titular_responsable' => 'Titular - Subscriptor'
                ];

                foreach ($params as $column => $value) {
                    if ($value != '' && $column != 'page') {
                        if ($column == 'abono_id') {
                            $abono = $this->abonoRepo->get(['id' => $value]);
                            $sheet->appendRow([$parseSearch[$column], $abono[0]->name]);
                        } else {
                            $sheet->appendRow([$parseSearch[$column], $value]);
                        }
                    }
                }

                $sheet->appendRow(['']);
                $sheet->appendRow([
                    'ID', 'Cuenta Abonado', 'Nombre - Razón Social', 'Titular - Subscriptor',
                    'Calle', 'Numero', 'Piso', 'Depto', 'Observaciones', 'Código Postal', 'Teléfono', 'Celular', 'Email', 'Documento', 'CUIT', 'Fecha Ingreso', 'Abono', 'Ciudad', 'Provincia'
                ]);

                foreach ($compradores as $index => $compradore) {
                    $fecha_ingreso = \DateTime::createFromFormat('Y-m-d', $compradore->fecha_alta);
                    $fecha_ingreso = date_format($fecha_ingreso, "d/m/Y");
                    $sheet->appendRow([
                        $compradore->id, $compradore->cuenta_abonado,
                        $compradore->nombre_razon_social, $compradore->titular_responsable,
                        $compradore->calle, $compradore->numero, $compradore->piso, $compradore->depto, $compradore->resto,
                        $compradore->codigo_postal, $compradore->telefono, $compradore->celular, $compradore->email,
                        $compradore->documento, $compradore->cuit, $fecha_ingreso, $compradore->abono->name, $compradore->ciudad, $compradore->provincia
                    ]);
                }
            });
        })->export('xlsx');
    }
}
