<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\ResumenRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\ArticuloRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ResumenController extends Controller
{

    private $repoResumen;

    private $repoCliente;

    private $repoArticulo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ResumenRepository $repoResumen, ClienteRepository $repoCliente, ArticuloRepository $repoArticulo)
    {
        $this->repoResumen = $repoResumen;
        $this->repoCliente = $repoCliente;
        $this->repoArticulo = $repoArticulo;
    }

    /**
     * Mostrar la lista de clientes es y permitir la búsqueda.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $resumenes = $this->repoResumen->search($request->all());

        // var_dump('<pre>');
        // var_dump($resumenes->items()[0]);
        // var_dump('</pre>');


        return view('resumenes.index')
            ->with('resumenes', $resumenes->appends(Input::except('page')))
            ->with('request', $request);
    }


    /**
     * Mostrar el formulario para agregar una Venta .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = $this->repoCliente->get(array());
        $articulos = $this->repoArticulo->get(array());
        $articulosArr = array();
        foreach ($articulos as $articulo) {
            $articulosArr[$articulo->id] = $articulo;
        }
        return view('resumenes.create')
            ->with('clientes', $clientes)
            ->with('articulos', $articulos)
            ->with('articulosArr', $articulosArr);
    }

    /**
     * Mostrar el formulario para generar Resumenes.
     *
     * @return \Illuminate\Http\Response
     */
    public function generarMostrarFormulario()
    {
        return view('resumenes.generar');
    }

    /**
     * Show cliente edit form
     */
    public function edit($id)
    {
        $resumen = $this->repoResumen->find($id);

        $clientes = $this->repoCliente->get(array());

        $articulos = $this->repoArticulo->get(array());

        $articulosArr = array();
        foreach ($articulos as $articulo) {
            $articulosArr[$articulo->id] = $articulo;
        }
        return view('resumenes.edit')
            ->with('resumen', $resumen)
            ->with('clientes', $clientes)
            ->with('articulos', $articulos)
            ->with('articulosArr', $articulosArr);
    }

    public function update(Request $request)
    {
        $valid = $this->validarPostUpdate($request);
        if ($valid !== true) {
            return $valid;
        }

        $valid = $this->repoResumen->updateResumen($request);
        if ($valid !== true) {
            return $valid;
        }

        return redirect()->back()->with('message', 'El resumen ha sido editado exitosamente.');
    }

    /**
     * Delete cliente
     */
    public function destroy($id)
    {

        $this->repoResumen->deleteResumen($id);
    }


    /**
     * Store a new cliente  post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = $this->validarPost($request);
        if ($valid !== true) {
            return $valid;
        }

        $valid = $this->repoResumen->saveResumen($request);
        if (!is_numeric($valid)) {
            return $valid;
        }

        return redirect()->back()->with('message', 'Resumen creado exitosamente.')->with('id', $valid);
    }

    public function generarResumenes(Request $request)
    {
        $messages = [
            'mes.required' => 'El mes a generar es obligatorio.',
            'anio.required' => 'El año a generar es obligatorio.',
            'anio.integer' => 'El año a generar debe ser un número.',
            'primer_vencimiento.required' => 'La fecha del primer vencimiento es obligatoria.',
            'segundo_vencimiento.required' => 'La fecha del segundo vencimiento es obligatoria.',
        ];

        $this->validate($request, [
            'mes' => 'required',
            'anio' => 'required|integer',
            'primer_vencimiento' => 'required',
            'segundo_vencimiento' => 'required',
        ], $messages);

        $desdeFecha = '01/' . $request->get('mes') . '/' . $request->get('anio');
        $hastaFecha = date("t/m/Y", strtotime($request->get('anio') . '-' . $request->get('mes') . '-01'));
        $paramsResumen = array('fecha_ingreso' => $desdeFecha . ' - ' . $hastaFecha);

        $clientes = $this->repoResumen->getSinResumen($paramsResumen);
        $cantidadGenerada = 0;

        foreach ($clientes as $cliente) {

            $valid = $this->repoResumen->generarResumen($cliente, $paramsResumen, $request->get('primer_vencimiento'), $request->get('segundo_vencimiento'), $cantidadGenerada);

            if ($valid !== true) {
                return $valid;
            }

            $cantidadGenerada++;
        }

        if ($cantidadGenerada) {
            return redirect()->back()->with('message', 'Se han generado exitosamente los resúmenes del período.');
        } else {
            return redirect()->back()->with('info', 'Los resúmenes para el período solicitado ya fueron generados en caso de querer realizar modificaciones vaya al listado de resúmenes y realice los cambios. O no existen clientes cargados en ese período.');
        }
    }

    private function validarPost($request)
    {
        $messages = [
            'fecha.required' => 'La fecha del resumen es obligatoria.',
            'primer_vencimiento.required' => 'La fecha del primer vencimiento es obligatoria.',
            'segundo_vencimiento.required' => 'La fecha del segundo vencimiento es obligatoria.',
            'detalle.*.required' => 'Debe ingresar el detalle del artículo en el resumen.',
            'artid.required' => 'Debe ingresar al menos un artículo en el resumen.',
            'monto.*.required' => 'Debe ingresar el monto del artículo en el resumen.',
            'cantidad.*.required' => 'Debe ingresar la cantidad del artículo en el resumen.',
            'monto.*.numeric' => 'El monto debe ser numérico.',
            'cantidad.*.numeric' => 'La cantidad debe ser un numérico.',
            'pagado.required' => 'Debe indicar si el resumen fue pagado o no.',
            'fecha_pago.required_if' => 'La fecha del pago es obligatoria.',
            'monto_efectivo.numeric' => 'El pago en efectivo debe ser numérico.',
            'monto_debito.numeric' => 'El pago con debito debe ser numérico.',
            'monto_cheque.numeric' => 'El pago con transferencia bancaria debe ser numérico.',
            'monto_tarjeta.numeric' => 'El pago con tarjeta debe ser numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'artid' => 'required',
            'primer_vencimiento' => 'required',
            'segundo_vencimiento' => 'required',
            'detalle.*' => 'required',
            'monto.*' => 'required',
            'cantidad.*' => 'required',
            'monto.*' => 'numeric',
            'cantidad.*' => 'numeric',
            'pagado' => 'required',
            'fecha_pago' => 'required_if:pagado,1',
            'monto_efectivo' => 'numeric',
            'monto_debito' => 'numeric',
            'monto_cheque' => 'numeric',
            'monto_tarjeta' => 'numeric',
        ], $messages);

        if ($request->get('pagado') == '1') {
            $montoPagado = (float)$request->get('monto_efectivo') + (float)$request->get('monto_debito') + (float)$request->get('monto_cheque') + (float)$request->get('monto_tarjeta');

            $montoAPagar = 0;
            foreach ($request->get('monto') as $i => $monto) {
                $montoAPagar += $monto;
            }

            if ($montoAPagar != $montoPagado) {
                return redirect()->to($this->getRedirectUrl())
                    ->withInput($request->input())
                    ->withErrors(['El monto pagado debe ser igual al total a pagar del resumen.']);
            }
        }

        return true;
    }

    private function validarPostUpdate($request)
    {

        $messages = [
            'fecha.required' => 'La fecha del resumen es obligatoria.',
            'primer_vencimiento.required' => 'La fecha del primer vencimiento es obligatoria.',
            'segundo_vencimiento.required' => 'La fecha del segundo vencimiento es obligatoria.',
            'detalle.*.required' => 'Debe ingresar el detalle del artículo en el resumen.',
            'artid.required' => 'Debe ingresar al menos un artículo en el resumen.',
            'monto.*.required' => 'Debe ingresar el monto del artículo en el resumen.',
            'cantidad.*.required' => 'Debe ingresar la cantidad del artículo en el resumen.',
            'monto.*.numeric' => 'El monto debe ser numérico.',
            'cantidad.*.numeric' => 'La cantidad debe ser un numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'artid' => 'required',
            'primer_vencimiento' => 'required',
            'segundo_vencimiento' => 'required',
            'detalle.*' => 'required',
            'monto.*' => 'required',
            'cantidad.*' => 'required',
            'monto.*' => 'numeric',
            'cantidad.*' => 'numeric',
        ], $messages);

        return true;
    }

    public function generarPDF($id)
    {
        $resumen = $this->repoResumen->find($id);

        $view = \View::make('pdf.resumenes-pdf')
            ->with('resumen', $resumen)
            ->with('montoLetras', $this->numtoletras($resumen->monto))
            ->with('montoLetras1', $this->numtoletras($resumen->monto_1_vencimento))
            ->with('montoLetras2', $this->numtoletras($resumen->monto_2_vencimento));

        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('resumen-' . $id);
    }

    public function generarImprimir($id)
    {
        $resumen = $this->repoResumen->find($id);

        $view = \View::make('pdf.resumenes-imprimir')
            ->with('resumen', $resumen)
            ->with('montoLetras', $this->numtoletras($resumen->monto))
            ->with('montoLetras1', $this->numtoletras($resumen->monto_1_vencimento))
            ->with('montoLetras2', $this->numtoletras($resumen->monto_2_vencimento));

        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('resumen-' . $id);
    }


    //------    CONVERTIR NUMEROS A LETRAS         ---------------
    //------    Máxima cifra soportada: 18 dígitos con 2 decimales
    //------    999,999,999,999,999,999.99
    // NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE BILLONES
    // NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE MILLONES
    // NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE PESOS 99/100 M.N.
    //------    Creada por:                        ---------------
    //------             ULTIMINIO RAMOS GALÁN     ---------------
    //------            uramos@gmail.com           ---------------
    //------    10 de junio de 2009. México, D.F.  ---------------
    //------    PHP Version 4.3.1 o mayores (aunque podría funcionar en versiones anteriores, tendrías que probar)
    public function numtoletras($xcifra)
    {
        $xarray = array(
            0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );
        //
        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                            } else {
                                $key = (int) substr($xaux, 0, 3);
                                if (TRUE === array_key_exists($key, $xarray)) {  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                    $xseek = $xarray[$key];
                                    $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                } else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                    $key = (int) substr($xaux, 0, 1) * 100;
                                    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {
                            } else {
                                $key = (int) substr($xaux, 1, 2);
                                if (TRUE === array_key_exists($key, $xarray)) {
                                    $xseek = $xarray[$key];
                                    $xsub = $this->subfijo($xaux);
                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                } else {
                                    $key = (int) substr($xaux, 1, 1) * 10;
                                    $xseek = $xarray[$key];
                                    if (20 == substr($xaux, 1, 1) * 10)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
                            } else {
                                $key = (int) substr($xaux, 2, 1);
                                $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = $this->subfijo($xaux);
                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena .= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena .= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN BILLON ";
                        else
                            $xcadena .= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN MILLON ";
                        else
                            $xcadena .= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO PESOS CON $xdecimales/100";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN PESO CON $xdecimales/100";
                        }
                        if ($xcifra >= 2) {
                            $xcadena .= " PESOS  CON $xdecimales/100"; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR ($xz)
        return trim($xcadena);
    }

    // END FUNCTION

    public function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        //
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        //
        return $xsub;
    }

    // END FUNCTION

    public function imprimirResumenes(Request $request)
    {
        $resumenes = $this->repoResumen->reposImpresion($request);

        return view('resumenes.imprimir')
            ->with('request', $request)
            ->with('resumenes', $resumenes);
    }

    public function imprimirResumenesPOST(Request $request)
    {
        $messages = [
            'mes.required' => 'El mes a imprimir es obligatorio.',
            'anio.required' => 'El año a imprimir es obligatorio.',
            'anio.integer' => 'El año a imprimir debe ser un número.',
        ];

        $this->validate($request, [
            'mes' => 'required',
            'anio' => 'required|integer',
        ], $messages);

        $resumenes = $this->repoResumen->reposImpresion($request);

        return view('resumenes.imprimir')
            ->with('request', $request)
            ->with('resumenes', $resumenes);
    }

    public function imprimirResumenesPreview(Request $request)
    {
        $viewfinal = '';
        $resumenes = $request->get('check');

        if ($request->get('iva') == '0')
            $plantilla = 'pdf.resumenes-imprimir';
        else
            $plantilla = 'pdf.resumenes-pdf';

        foreach ($resumenes as $i => $idResumen) {
            $resumen = $this->repoResumen->find($idResumen);
            $view = \View::make($plantilla)
                ->with('resumen', $resumen)
                ->with('montoLetras', $this->numtoletras($resumen->monto))
                ->with('montoLetras1', $this->numtoletras($resumen->monto_1_vencimento))
                ->with('montoLetras2', $this->numtoletras($resumen->monto_2_vencimento))->render();
            $viewfinal .= $view;
        }

        $view = \View::make('pdf.final-resumenes')->with('viewfinal', $viewfinal)->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('resumenes');
    }

    public function getAjax($id, $fechaquepaga)
    {
        //        "id" => 24034
        //    "monto" => 1000.0
        //    "monto_1_vencimento" => 1250.0
        //    "monto_2_vencimento" => 1500.0
        //    "observacion" => ""
        //    "primer_vencimiento" => "2020-12-17"
        //    "segundo_vencimiento" => "2020-09-24"
        //    "fecha" => "2020-12-31"
        //    "pagado" => 0
        //    "venta" => 0
        if (is_numeric($id)) {
            $resumen = \App\Resumen::find($id);
            // dd($resumen);

            $current_date = date('Y-m-d', strtotime($fechaquepaga));

            $vencimiento_text = '';
            $monto_determinado = 0;
            $esta_pagandp_el_mes_de = '';

            if (date('m', strtotime($resumen->fecha)) == '01') {
                $esta_pagandp_el_mes_de = 'Enero';
            } elseif (date('m', strtotime($resumen->fecha)) == '02') {
                $esta_pagandp_el_mes_de = 'Febrero';
            } elseif (date('m', strtotime($resumen->fecha)) == '03') {
                $esta_pagandp_el_mes_de = 'Marzo';
            } elseif (date('m', strtotime($resumen->fecha)) == '04') {
                $esta_pagandp_el_mes_de = 'Abril';
            } elseif (date('m', strtotime($resumen->fecha)) == '05') {
                $esta_pagandp_el_mes_de = 'Mayo';
            } elseif (date('m', strtotime($resumen->fecha)) == '06') {
                $esta_pagandp_el_mes_de = 'Junio';
            } elseif (date('m', strtotime($resumen->fecha)) == '07') {
                $esta_pagandp_el_mes_de = 'Julio';
            } elseif (date('m', strtotime($resumen->fecha)) == '08') {
                $esta_pagandp_el_mes_de = 'Agosto';
            } elseif (date('m', strtotime($resumen->fecha)) == '09') {
                $esta_pagandp_el_mes_de = 'Septiembre';
            } elseif (date('m', strtotime($resumen->fecha)) == '10') {
                $esta_pagandp_el_mes_de = 'Octubre';
            } elseif (date('m', strtotime($resumen->fecha)) == '11') {
                $esta_pagandp_el_mes_de = 'Noviembre';
            } else {
                $esta_pagandp_el_mes_de = 'Diciembre';
            }

            if ($resumen->segundo_vencimiento <= $current_date) {
                $monto_determinado = $resumen->monto_2_vencimento;
                $vencimiento_text = "Se aplica el 2do vencimiento : " . $resumen->segundo_vencimiento;
            } elseif ($resumen->primer_vencimiento <= $current_date) {
                $monto_determinado = $resumen->monto_1_vencimento;
                $vencimiento_text = "Se aplica el 1er Vencimiento : " . $resumen->primer_vencimiento;
            } else {
                $monto_determinado = $resumen->monto;
                $vencimiento_text = "No aplica vencimiento";
            }

            if ($resumen) {
                $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Total a pagar: </label>
                                <input  value="' . $monto_determinado . '" type="text" disabled class="form-control" >
                                <label style="color:green;border: 1px solid green;margin-top: 15px;padding: 5px 10px;"><i class="fa fa-money"></i> Monto inicial: $' . $resumen->monto . ' </label><br/>
                                <label style="color: green;border: 1px solid green;margin-top: 5px;padding: 5px 10px;"><i class="fa fa-calendar"></i> Mes a pagar: ' . $esta_pagandp_el_mes_de . ' </label><br/>
                                <label style="color: grey;border: 1px solid grey;margin-top: 5px;padding: 5px 10px;"><i class="fa fa-calendar"></i> Fecha que realiza el pago: ' . $fechaquepaga . ' </label><br/>
                                <label style="color:red;border: 1px solid red;margin-top: 5px;padding: 5px 10px;"><i class="fa fa-warning"></i> ' . $vencimiento_text . ' </label><br/>
                                <label style="color:blue;border: 1px solid blue;margin-top: 5px;padding: 5px 10px;"><i class="fa fa-money"></i> Monto a pagar: $' . $monto_determinado . ' </label>
                            </div>';
                if ($resumen->cliente) {
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Titular - Subscriptor: </label>
                                <input  value="' . $resumen->cliente->titular_responsable . '" type="text" disabled class="form-control" >
                            </div>';
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Cuenta Abonado: </label>
                                <input  value="' . $resumen->cliente->cuenta_abonado . '" type="text" disabled class="form-control" >
                            </div>';
                    $pagado = 0;
                    foreach ($resumen->pagos() as $pago) {
                        $pagado += $pago->monto_efectivo + $pago->monto_tarjeta + $pago->monto_debito + $pago->monto_cheque;
                    }
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pagado: </label>
                                <input  value="' . $pagado . '" type="text" disabled class="form-control" >
                            </div>';
                }
                echo $output;
            } else {
                $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>No válido o inexistente</label>
                            </div>';
                echo $output;
            }
        } else {
            $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>No válido o inexistente</label>
                            </div>';
            echo $output;
        }
    }
    public function getAjaxVenta($id)
    {

        if (is_numeric($id)) {
            $resumen = \App\Resumen::where('venta', 1)->find($id);

            if ($resumen) {
                $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Total a pagar: </label>
                                <input  value="' .  $resumen->monto . '" type="text" disabled class="form-control" >

                            </div>';
                if ($resumen->cliente) {
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Titular - Subscriptor: </label>
                                <input  value="' . $resumen->cliente->titular_responsable . '" type="text" disabled class="form-control" >
                            </div>';
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Cuenta Abonado: </label>
                                <input  value="' . $resumen->cliente->cuenta_abonado . '" type="text" disabled class="form-control" >
                            </div>';
                    $pagado = 0;
                    foreach ($resumen->pagos() as $pago) {
                        $pagado += $pago->monto_efectivo + $pago->monto_tarjeta + $pago->monto_debito + $pago->monto_cheque;
                    }
                    $output .= '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>Pagado: </label>
                                <input  value="' . $pagado . '" type="text" disabled class="form-control" >
                            </div>';
                }
                echo $output;
            } else {
                $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>No válido o inexistente</label>
                            </div>';
                echo $output;
            }
        } else {
            $output = '<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label>No válido o inexistente</label>
                            </div>';
            echo $output;
        }
    }
}
