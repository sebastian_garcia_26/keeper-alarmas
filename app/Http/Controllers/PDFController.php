<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\ClienteRepository;
use App\Repositories\AbonoRepository;
use App\Repositories\PagoRepository;

use App\Repositories\CompradoreRepository;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    
    private $clienteRepo;
    private $abonoRepo;
    private $pagoRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClienteRepository $clienteRepo, AbonoRepository $abonoRepo, PagoRepository $pagoRepo, CompradoreRepository $compradoreRepo)
    {
        $this->clienteRepo = $clienteRepo;
        $this->abonoRepo = $abonoRepo;
        $this->pagoRepo = $pagoRepo;

        $this->compradoreRepo = $compradoreRepo;
    }
    
    public function exportarClientes(Request $request){
        $params = $request->all();
        $clientes = $this->clienteRepo->get($params);
        $parseSearch = ['id' => 'ID', 'cuenta_keeper' => 'Cuenta Keeper',
                    'cuenta_abonado' => 'Cuenta Abonado', 'nombre_razon_social' => 'Nombre - Razón Social',
                    'codigo_postal' => 'Código Postal','titular_responsable' => 'Titular - Subscriptor', 'abono_id' => 'Abono Mensual',
                    'fecha_ingreso' => 'Fecha de Ingreso entre'];
        
     
        $view = \View::make('pdf.clientes')
                ->with('date',date('d/m/Y H:i'))
                ->with('params', $params)
                ->with('clientes', $clientes)
                ->with('parseSearch', $parseSearch);
        
        if(array_key_exists("abono_id",$params)){
            $abono = $this->abonoRepo->get(['id' => $params['abono_id']]);
            $view->with('abonoSearch',$abono[0]->name);
        }
                
        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('clientes');
    }
    
    public function exportarPagos(Request $request){
        $params = $request->all();
        $pagos = $this->pagoRepo->get($params);
        $parseSearch = ['id' => 'Número de Resumen', 
                    'cuenta_abonado' => 'Cuenta Abonado', 
                    'fecha' => 'Fecha de pago entre','titular_responsable' => 'Titular - Subscriptor'];
        
     
        $view = \View::make('pdf.pagos')
                ->with('date',date('d/m/Y H:i'))
                ->with('params', $params)
                ->with('pagos', $pagos)
                ->with('total',0)
                ->with('parseSearch', $parseSearch);
        
        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('pagos');
    }
    
    public function exportarDeudores(Request $request){
        $params = $request->all();
        $deudores = $this->pagoRepo->searchDeudores($params);
        $parseSearch = [
                    'cuenta_abonado' => 'Cuenta Abonado', 
                    'titular_responsable' => 'Titular - Subscriptor'];
        
     
        $view = \View::make('pdf.deudores')
                ->with('date',date('d/m/Y H:i'))
                ->with('params', $params)
                ->with('deudores', $deudores)
                ->with('total',0)
                ->with('parseSearch', $parseSearch);
        
        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('deudores');
    }

    public function exportarDeudoresProductos(Request $request){
        $params = $request->all();
        $deudores = $this->pagoRepo->searchDeudoresProductos($params);
        $parseSearch = [
                    'cuenta_abonado' => 'Cuenta Abonado', 
                    'titular_responsable' => 'Titular - Subscriptor'];
        
     
        $view = \View::make('pdf.deudores')
                ->with('date',date('d/m/Y H:i'))
                ->with('params', $params)
                ->with('deudores', $deudores)
                ->with('total',0)
                ->with('parseSearch', $parseSearch);
        
        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4');
        return $pdf->stream('deudores');
    }

    public function exportarCompradores(Request $request){
        $params = $request->all();
        $compradores = $this->compradoreRepo->get($params);
        $parseSearch = ['id' => 'ID', 'cuenta_keeper' => 'Cuenta Keeper',
                    'cuenta_abonado' => 'Cuenta Abonado', 'nombre_razon_social' => 'Nombre - Razón Social',
                    'codigo_postal' => 'Código Postal','titular_responsable' => 'Titular - Subscriptor', 'abono_id' => 'Abono Mensual',
                    'fecha_ingreso' => 'Fecha de Ingreso entre'];
        
     
        $view = \View::make('pdf.compradores')
                ->with('date',date('d/m/Y H:i'))
                ->with('params', $params)
                ->with('compradores', $compradores)
                ->with('parseSearch', $parseSearch);
        
        if(array_key_exists("abono_id",$params)){
            $abono = $this->abonoRepo->get(['id' => $params['abono_id']]);
            $view->with('abonoSearch',$abono[0]->name);
        }
                
        $view->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('compradores');
    }
}