<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\ArticuloRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ArticuloController extends Controller
{

    private $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ArticuloRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Mostrar la lista de articulos es y permitir la búsqueda.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $articulos = $this->repo->search($request->all());

        return view('articulos.index')
            ->with('articulos', $articulos->appends(Input::except('page')))
            ->with('request', $request);
    }


    /**
     * Mostrar el formulario para agregar un Articulo .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articulos.create');
    }

    /**
     * Show articulo edit form
     */
    public function edit($id)
    {
        $articulo = $this->repo->find($id);

        return view('articulos.edit')->with('articulo', $articulo);
    }

    public function update(Request $request)
    {
        $messages = [
            'name.unique' => 'El detalle no puede repetirse.',
            'name.required' => 'El detalle es obligatoria.',
            'code.required' => 'El código es obligatorio.',
            'code.unique' => 'El código no puede repetirse.',
            'price.required' => 'El importe es obligatorio.',
            'price.numeric' => 'El importe debe ser numérico.',
            'iva.numeric' => 'La alícuota debe ser numérica.'
        ];

        $this->validate($request, [
            'name' => 'required|unique:articulos,name,' . $request->get('id') . ',id,deleted_at,NULL',
            'code' => 'required|unique:articulos,code,' . $request->get('id') . ',id,deleted_at,NULL',
            'price' => 'required|numeric',
            'iva' => 'numeric'
        ], $messages);

        try {
            $this->repo->saveArticulo($request, $request->get('id'));
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['El articulo no pudo ser actualizado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'El articulo ha sido actualizado exitosamente.');
    }

    /**
     * Delete articulo
     */
    public function destroy($id)
    {
        $this->repo->deleteArticulo($id);
    }


    /**
     * Store a new articulo  post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $messages = [
            'name.unique' => 'La descripción no puede repetirse.',
            'name.required' => 'La descripción es obligatoria.',
            'code.required' => 'El código es obligatorio.',
            'code.unique' => 'El código no puede repetirse.',
            'price.required' => 'El importe es obligatorio.',
            'price.numeric' => 'El importe debe ser numérico.',
            'iva.numeric' => 'La alícuota debe ser numérica.'
        ];

        $this->validate($request, [
            'name' => 'required|unique:articulos,name,NULL,id,deleted_at,NULL',
            'code' => 'required|unique:articulos,code,NULL,id,deleted_at,NULL',
            'price' => 'required|numeric',
            'iva' => 'numeric'
        ], $messages);

        try {
            $this->repo->saveArticulo($request);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['El articulo no pudo ser creado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'Articulo creado exitosamente.');
    }
}
