<?php

namespace App\Http\Controllers;

use App\Pago;
use App\Repositories\PagoRepository;
use App\Repositories\VentaRepository;
use App\Repositories\CompradoreRepository;
use App\Repositories\CobroRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Cobro;

class CobroController extends Controller
{

    private $repoVenta;

    private $repoCompradore;

    private $repoCobro;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VentaRepository $repoVenta, CompradoreRepository $repoCompradore, CobroRepository $repoCobro)
    {
        $this->repoVenta = $repoVenta;
        $this->repoCompradore = $repoCompradore;
        $this->repoCobro = $repoCobro;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cobros = $this->repoCobro->searchIndex($request->all());
        return view('cobros.index')
            ->with('cobros', $cobros->appends(Input::except('page')))
            ->with('request', $request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cobros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'fecha.required' => 'La fecha del cobro es obligatoria.',
            'venta_id.integer' => 'El número del venta debe ser un número entero positivo.',
            'monto_efectivo.numeric' => 'El pago en efectivo debe ser numérico.',
            'monto_debito.numeric' => 'El pago con debito debe ser numérico.',
            'monto_cheque.numeric' => 'El pago con transferencia bancaria debe ser numérico.',
            'monto_tarjeta.numeric' => 'El pago con tarjeta debe ser numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'monto_efectivo' => 'numeric',
            'monto_debito' => 'numeric',
            'monto_cheque' => 'numeric',
            'monto_tarjeta' => 'numeric',
            'venta_id' => 'integer',
        ], $messages);

        $venta = \App\Resumen::find($request->venta_id);
        if (!$venta) {
            return redirect()->to($this->getRedirectUrl())
                ->withInput($request->input())
                ->withErrors(['El número de venta no es válido.']);
        } else {
            try {
                DB::beginTransaction();
                $pago = new Pago;
                $pago->resumen_id = $request->venta_id;
                $pago->observacion = $request->observacion;
                $pago->monto_efectivo = $request->monto_efectivo;
                $pago->monto_debito = $request->monto_debito;
                $pago->monto_cheque = $request->monto_cheque;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->monto_tarjeta = $request->monto_tarjeta;
                $pago->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
                $pago->save();


                $resumen = \App\Resumen::findOrFail($request->venta_id);

                $resumen->pagado = 1;
                $resumen->save();
                DB::commit();
                return redirect()->back()->with('message', 'Se ha guardado el cobro de manera exitosa.');
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                    ->withErrors(['Hubo un error inesperado al intentar guardar el cobro, intente nuevamente y si el problema persiste contacte al desarrollador.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pagos = new PagoRepository();
        $cobro = $pagos->find($id);
        return view('cobros.edit')->with('cobro', $cobro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'fecha.required' => 'La fecha del cobro es obligatoria.',
            'monto_efectivo.numeric' => 'El pago en efectivo debe ser numérico.',
            'monto_debito.numeric' => 'El pago con debito debe ser numérico.',
            'monto_cheque.numeric' => 'El pago con transferencia bancaria debe ser numérico.',
            'monto_tarjeta.numeric' => 'El pago con tarjeta debe ser numérico.',
        ];

        $this->validate($request, [
            'fecha' => 'required',
            'monto_efectivo' => 'numeric',
            'monto_debito' => 'numeric',
            'monto_cheque' => 'numeric',
            'monto_tarjeta' => 'numeric',
        ], $messages);

        try {
            DB::beginTransaction();
            $cobro = $this->repoCobro->find($id);
            $cobro->observacion = $request->observacion;
            $cobro->monto_efectivo = $request->monto_efectivo;
            $cobro->monto_debito = $request->monto_debito;
            $cobro->monto_cheque = $request->monto_cheque;
            $cobro->monto_tarjeta = $request->monto_tarjeta;
            $cobro->monto_tarjeta = $request->monto_tarjeta;
            $cobro->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $cobro->save();

            $venta = \App\Venta::findOrFail($cobro->venta_id);
            $montoCobros = 0;
            foreach ($venta->cobros() as $cobror) {
                $montoCobros += $cobror->monto_efectivo + $cobror->monto_debito + $cobror->monto_tarjeta + $cobror->monto_cheque;
            }

            $montoLineas = 0;
            foreach ($venta->series() as $serie) {
                $montoLineas += $serie->monto;
            }

            if ($montoLineas > $montoCobros) {
                $venta->pagado = 0;
                $venta->save();
            } else {
                $venta->pagado = 1;
                $venta->save();
            }


            DB::commit();
            return redirect()->back()->with('message', 'Se ha guardado el cobro de manera exitosa.');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors(['Hubo un error inesperado al intentar guardar el cobro, intente nuevamente y si el problema persiste contacte al desarrollador.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repoCobro->deleteCobro($id);
    }

    public function indexDeudores(Request $request)
    {

        $deudores = $this->repoCobro->searchDeudores($request->all());
        return view('cobros.index-deudores')
            ->with('deudores', $deudores)
            ->with('request', $request);
    }
}
