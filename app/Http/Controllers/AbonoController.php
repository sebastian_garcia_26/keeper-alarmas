<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\AbonoRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AbonoController extends Controller
{
    
    private $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AbonoRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Mostrar la lista de abonos mensuales y permitir la búsqueda.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $abonos = $this->repo->search($request->all());
        
        return view('abonos.index')
                    ->with('abonos',$abonos->appends(Input::except('page')))
                    ->with('request',$request);
    }


    /**
     * Mostrar el formulario para agregar un Abono Mensual.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('abonos.create');
    }

    /**
     * Show abono edit form
     */
    public function edit($id)
    {
        $abono = $this->repo->find($id);

        return view('abonos.edit')->with('abono',$abono);
    }

    public function update(Request $request)
    {
        $messages = [
            'name.unique' => 'La descripción no puede repetirse.',
            'name.required' => 'La descripción es obligatoria.',
            'price.required' => 'El importe es obligatorio.',
            'price.numeric' => 'El importe debe ser numérico.',
            'price_vencimiento_1.required' => 'El importe adicional por vencimiento es obligatorio.',
            'price_vencimiento_1.numeric' => 'El importe adicional por vencimiento debe ser numérico.',
            'price_vencimiento_2.required' => 'El importe adicional por vencimiento es obligatorio.',
            'price_vencimiento_2.numeric' => 'El importe adicional por vencimiento debe ser numérico.',
            'iva.numeric' => 'La alícuota debe ser numérica.'
        ];

        $this->validate($request, [
            'name' => 'required|unique:abonos,name,'.$request->get('id').',id,deleted_at,NULL',
            'price' => 'required|numeric',
            'price_vencimiento_1' => 'required|numeric',
            'price_vencimiento_2' => 'required|numeric',
            'iva' => 'numeric'
        ], $messages);

        try{
            $this->repo->saveAbono($request,$request->get('id'));
        } catch ( \Exception $e){
            return redirect()->back()->withErrors([ 'El abono mensual no pudo ser actualizado, algo ocurrió.']);
        }

        return redirect()->back()->with('message', 'El abono mensual ha sido actualizado exitosamente.');
    }

    /**
     * Delete abono
     */
    public function destroy($id)
    {
        $this->repo->deleteAbono($id);
    }


    /**
     * Store a new abono mensual post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $messages = [
            'name.unique' => 'La descripción no puede repetirse.',
            'name.required' => 'La descripción es obligatoria.',
            'price.required' => 'El importe es obligatorio.',
            'price.numeric' => 'El importe debe ser numérico.',
            'price_vencimiento_1.required' => 'El importe adicional por vencimiento es obligatorio.',
            'price_vencimiento_1.numeric' => 'El importe adicional por vencimiento debe ser numérico.',
            'price_vencimiento_2.required' => 'El importe adicional por vencimiento es obligatorio.',
            'price_vencimiento_2.numeric' => 'El importe adicional por vencimiento debe ser numérico.',
            'iva.numeric' => 'La alícuota debe ser numérica.'
        ];
        
        $this->validate($request, [
            'name' => 'required|unique:abonos,name,NULL,id,deleted_at,NULL',
            'price' => 'required|numeric',
            'price_vencimiento_1' => 'required|numeric',
            'price_vencimiento_2' => 'required|numeric',
            'iva' => 'numeric'
        ], $messages);

        try{
            $this->repo->saveAbono($request);
        } catch ( \Exception $e){
            return redirect()->back()->withErrors(['El abono no pudo ser creado, algo ocurrió.']);
        }
        
        return redirect()->back()->with('message', 'Abono Mensual creado exitosamente.');
    }

}