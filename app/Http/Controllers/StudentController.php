<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\StudentRepository;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    
    private $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StudentRepository $repo)
    {
        //$this->middleware('auth');
        $this->repo = $repo;
    }

    /**
     * Show the students list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $students = $this->repo->search($request->all());
        
        return view('students.index')
                    ->with('students',$students)
                    ->with('request',$request);
    }


    /**
     * Show the students add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Show student edit form
     */
    public function edit($id)
    {
        $student = $this->repo->find($id);

        return view('students.edit')->with('student',$student);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'student_id' => 'required|unique:students,id,'.$request->get('id'),
            'email' => 'email',
            'birthday' => 'date'
        ]);

        //try{
            $this->repo->saveStudent($request,$request->get('id'));
        //} catch ( \Exception $e){
            //return redirect()->back()->withErrors(['msg', 'The student couldn\'t be updated.']);
        //}

        return redirect()->back()->with('message', 'Student updated successfully.');
    }

    /**
     * Delete student
     */
    public function destroy($id)
    {
        $this->repo->deleteStudent($id);
    }


    /**
     * Store a new student post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'student_id' => 'required|unique:students',
            'email' => 'email',
            'birthday' => 'date'
        ]);

        //try{
            $this->repo->saveStudent($request);
        //} catch ( \Exception $e){
            //return redirect()->back()->withErrors(['msg', 'The student couldn\'t be created.']);
        //}
        
        return redirect()->back()->with('message', 'Student added successfully.');
    }
}