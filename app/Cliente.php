<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;
    
    function abono()
    {
        return $this->belongsTo(Abono::class, 'abono_id')->withTrashed();
    }
}
