<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cobro extends Model
{
    use SoftDeletes;
    
    function venta()
    {
        return $this->belongsTo(Venta::class, 'venta_id');
    }
}
