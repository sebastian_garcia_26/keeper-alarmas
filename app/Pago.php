<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends Model
{
    use SoftDeletes;
    
    function resumen()
    {
        return $this->belongsTo(Resumen::class, 'resumen_id');
    }
}
