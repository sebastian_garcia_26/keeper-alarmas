<?php

namespace App\Repositories;

use App\Cobro;
use App\Compradore;
use App\Pago;
use App\Venta;
use DB;

class CobroRepository
{

    public function saveCobro($request, $id = null)
    {
        if ($id) {
            $cobro = $this->find($id);
        } else {
            $cobro = new Cobro;
        }

        $cobro->name = $request->get('name');
        $cobro->price = $request->get('price');
        $cobro->iva = $request->get('iva');

        $cobro->save();
    }

    public function search($params)
    {
        $cobros = Cobro::select()->orderBy('fecha', 'desc');
        foreach ($params as $column => $value) {
            if ($value && $column != 'page') {
                $cobros->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $cobros->paginate(env('APP_PAGINATION'));
    }

    /*public function get($params){
            $cobros = Cobro::select();
            foreach ($params as $column => $value) {
                if ($value && $column == 'id') {
                    $cobros->where($column, '=', $value );
                }elseif ($value && $column != 'page') {
                    $cobros->where($column, 'LIKE', '%' . $value . '%');
                }
            }
            return $cobros->get();
        }*/

    public function deleteCobro($id)
    {
        try {
            DB::beginTransaction();
            $cobro = Pago::findOrFail($id);
            $cobro->delete();
//
//            $venta = \App\Venta::findOrFail($cobro->venta_id);
//            dd($venta);
//            $montoCobros = 0;
//            foreach ($venta->cobros() as $cobror) {
//                $montoCobros += $cobror->monto_efectivo + $cobror->monto_debito + $cobror->monto_tarjeta + $cobror->monto_cheque;
//            }
//
//            $montoLineas = 0;
//            foreach ($venta->series() as $serie) {
//                $montoLineas += $serie->monto;
//            }
//
//            if ($montoLineas > $montoCobros) {
//                $venta->pagado = 0;
//                $venta->save();
//            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    public function find($id)
    {
        return Pag::findOrFail($id);
    }

    public function searchIndex($params)
    {
//        $cobros = Cobro::select('cobros.*')->orderBy('fecha', 'desc');
//
//        if (count($params) >= 2) {
//            $cobros->leftJoin('ventas as ven', 'ven.id', '=', 'cobros.venta_id');
//
//            if ($params['cuenta_abonado'] || $params['titular_responsable']) {
//                $cobros->leftJoin('compradores as cli', 'cli.id', '=', 'ven.compradore_id');
//                if ($params['cuenta_abonado']) {
//                    $cobros->where('cli.cuenta_abonado', 'LIKE', '%' . $params['cuenta_abonado'] . '%');
//                }
//
//                if ($params['titular_responsable']) {
//                    $cobros->where('cli.titular_responsable', 'LIKE', '%' . $params['titular_responsable'] . '%');
//                }
//            }
//
//            if ($params['id']) {
//                $cobros->where('cobros.venta_id', '=', $params['id']);
//            }
//
//            if ($params['fecha']) {
//                $fechas = explode('-', $params['fecha']);
//                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
//                $fechas[0] = date_format($fechas[0], "Y-m-d");
//
//                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
//                $fechas[1] = date_format($fechas[1], "Y-m-d");
//
//                $cobros->whereBetween('cobros.fecha', [$fechas[0], $fechas[1]]);
//            }
//        }
//
//        return $cobros->paginate(env('APP_PAGINATION'));
        $pagos = Pago::select('pagos.*','res.cliente_id','res.comprador_id','cli.cuenta_abonado as cli_abo','cli.titular_responsable as cli_titu','com.cuenta_abonado as com_abo','com.titular_responsable as com_titu');
        $pagos->leftJoin('resumenes as res', 'res.id', '=', 'pagos.resumen_id');
        $pagos->leftJoin('clientes as cli', 'cli.id', '=', 'res.cliente_id');
        $pagos->leftJoin('compradores as com', 'com.id', '=', 'res.comprador_id');
        $pagos->where('res.tipo_transaccion', '=', 2)->orderBy('fecha', 'desc');
        // var_dump(count($params));

        if (count($params) >= 2) {
            //$pagos->leftJoin('resumenes as res', 'res.id', '=', 'pagos.resumen_id');

            if ($params['cuenta_abonado'] || $params['titular_responsable']) {
               // $pagos->leftJoin('clientes as cli', 'cli.id', '=', 'res.cliente_id');
                if ($params['cuenta_abonado']) {
                    $pagos->where('cli.cuenta_abonado', 'LIKE', '%' . $params['cuenta_abonado'] . '%');
                }

                if ($params['titular_responsable']) {
                    $pagos->where('cli.titular_responsable', 'LIKE', '%' . $params['titular_responsable'] . '%');
                }
            }

            if ($params['id']) {
                $pagos->where('pagos.resumen_id', '=', $params['id']);
            }


        }
        if (isset($params['fecha']) &&  $params['fecha']) {

            $fechas = explode('-', $params['fecha']);
            $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
            $fechas[0] = date_format($fechas[0], "Y-m-d");

            $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
            $fechas[1] = date_format($fechas[1], "Y-m-d");

            $pagos->whereBetween('pagos.fecha', [$fechas[0], $fechas[1]]);
        }
        $pagos->where('res.tipo_transaccion', '=', 2);

        return $pagos->paginate(env('APP_PAGINATION'));
    }

    public function get($params)
    {
        $cobros = Cobro::select('cobros.*')->orderBy('fecha', 'desc');

        if (count($params) >= 2) {
            $cobros->leftJoin('ventas as ven', 'ven.id', '=', 'cobros.venta_id');

            if ($params['cuenta_abonado'] || $params['titular_responsable']) {
                $cobros->leftJoin('compradores as cli', 'cli.id', '=', 'ven.compradore_id');
                if ($params['cuenta_abonado']) {
                    $cobros->where('cli.cuenta_abonado', 'LIKE', '%' . $params['cuenta_abonado'] . '%');
                }

                if ($params['titular_responsable']) {
                    $cobros->where('cli.titular_responsable', 'LIKE', '%' . $params['titular_responsable'] . '%');
                }
            }

            if ($params['id']) {
                $cobros->where('cobros.venta_id', '=', $params['id']);
            }

            if ($params['fecha']) {
                $fechas = explode('-', $params['fecha']);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $cobros->whereBetween('cobros.fecha', [$fechas[0], $fechas[1]]);
            }
        }

        return $cobros->get();
    }


    public function searchDeudores($params)
    {
//        $cuenta = '%%';
//        $titular = '%%';
//        if (count($params)) {
//            if ($params['cuenta_abonado']) {
//                $cuenta = '%' . $params['cuenta_abonado'] . '%';
//            }
//
//            if ($params['titular_responsable']) {
//                $titular = '%' . $params['titular_responsable'] . '%';
//            }
//        }
//
//        return \DB::select('SELECT cli.*, SUM(aux.pagado) as pagado, SUM(aux.monto) as a_pagar '
//            . 'FROM compradores cli INNER JOIN '
//            . '      (SELECT ven.id as ven_id,ven.compradore_id, '
//            . '       SUM(IF( pa.deleted_at IS NULL, pa.monto_tarjeta+pa.monto_efectivo+pa.monto_debito+pa.monto_cheque, 0)) as pagado, ven.monto '
//            . '       FROM ventas ven LEFT JOIN cobros pa ON (pa.venta_id = ven.id) '
//            . '       WHERE ven.deleted_at IS NULL AND ven.compradore_id IS NOT NULL GROUP BY ven.id) as aux '
//            . 'ON (cli.id = aux.compradore_id) '
//            . 'WHERE cli.deleted_at IS NULL AND cli.cuenta_abonado LIKE ? AND cli.titular_responsable LIKE ? GROUP BY cli.id ORDER BY id DESC', [$cuenta, $titular]);
        $cuenta = '%%';
        $titular = '%%';
        if (count($params)) {
            if ($params['cuenta_abonado']) {
                $cuenta = '%' . $params['cuenta_abonado'] . '%';
            }

            if ($params['titular_responsable']) {
                $titular = '%' . $params['titular_responsable'] . '%';
            }
        }
        $a= \DB::select('SELECT cli.*, SUM(aux.pagado) as pagado, SUM(aux.monto) as a_pagar '
            . 'FROM clientes cli INNER JOIN '
            . '      (SELECT res.id as res_id,res.cliente_id, '
            . '       SUM(IF( pa.deleted_at IS NULL, pa.monto_tarjeta+pa.monto_efectivo+pa.monto_debito+pa.monto_cheque, 0)) as pagado, res.monto '
            . '       FROM resumenes res LEFT JOIN pagos pa ON (pa.resumen_id = res.id) '
            . '       WHERE res.deleted_at IS NULL AND res.tipo_transaccion=2 GROUP BY res.id) as aux '
            . 'ON (cli.id = aux.cliente_id) '
            . 'WHERE cli.deleted_at IS NULL AND cli.cuenta_abonado LIKE ? AND cli.titular_responsable LIKE ? GROUP BY cli.id having (a_pagar <> pagado or pagado is null) ORDER BY id DESC', [$cuenta, $titular]);

        $b= \DB::select('SELECT cli.*, SUM(aux.pagado) as pagado, SUM(aux.monto) as a_pagar '
            . 'FROM compradores cli INNER JOIN '
            . '      (SELECT res.id as res_id,res.comprador_id, '
            . '       SUM(IF( pa.deleted_at IS NULL, pa.monto_tarjeta+pa.monto_efectivo+pa.monto_debito+pa.monto_cheque, 0)) as pagado, res.monto '
            . '       FROM resumenes res LEFT JOIN pagos pa ON (pa.resumen_id = res.id) '
            . '       WHERE res.deleted_at IS NULL AND res.tipo_transaccion=2 GROUP BY res.id) as aux '
            . 'ON (cli.id = aux.comprador_id) '
            . 'WHERE cli.deleted_at IS NULL AND cli.cuenta_abonado LIKE ? AND cli.titular_responsable LIKE ? GROUP BY cli.id having (a_pagar <> pagado or pagado is null) ORDER BY id DESC', [$cuenta, $titular]);
    return array_merge($a,$b);
    }
}
