<?php

namespace App\Repositories;

use App\Resumen;
use App\Linea;
use App\Cliente;
use App\Pago;
use Illuminate\Support\Facades\Input;
use DB;
use Exception;


class ResumenRepository
{
    public function updateResumen($request)
    {
        try {
            DB::beginTransaction();
            $montoTotal = 0;
            $montos = $request->get('monto');
            $cantidades = $request->get('cantidad');
            $detalles = $request->get('detalle');

            foreach ($montos as $i => $monto) {
                $montoTotal += $monto;
            }

            $resumen = $this->find($request->get('id'));
            $montoPagos = 0;

            foreach ($resumen->pagos() as $pago) {
                $montoPagos += $pago->monto_efectivo + $pago->monto_debito + $pago->monto_tarjeta + $pago->monto_cheque;
            }


            $resumen->monto = $montoTotal;
            $resumen->observacion = $request->get('observacion');
            $resumen->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('primer_vencimiento'))), "Y-m-d");
            $resumen->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('segundo_vencimiento'))), "Y-m-d");
            $resumen->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $resumen->pagado = $montoPagos >= $montoTotal ? 1 : 0;
            $resumen->cliente_id = $request->get('cliente_id') ? $request->get('cliente_id') : null;
            $resumen->abono_id = $request->get('abono_id');
            $resumen->save();

            $lineasID = array();
            foreach ($resumen->lineas() as $linea) {
                $lineasID[] = $linea->id;
            }
            Linea::destroy($lineasID);

            foreach ($request->get('artid') as $i => $artid) {
                $linea = new Linea;
                $linea->monto = $montos[$i];
                $linea->descripcion = $detalles[$i];
                $linea->cantidad = $cantidades[$i];
                $linea->articulo_id = $artid ? $artid : null;
                $linea->resumen_id = $resumen->id;
                $linea->save();
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar actualizar el resumen, intente nuevamente y si el problema persiste contacte al desarrollador.']);
        }
    }

    public function saveResumen($request)
    {
        try {
            DB::beginTransaction();
            $montoTotal = 0;
            $montos = $request->get('monto');
            $cantidades = $request->get('cantidad');
            $detalles = $request->get('detalle');
            foreach ($montos as $i => $monto) {
                $montoTotal += $monto;
            }

            $resumen = new Resumen;
            $resumen->monto = $montoTotal;
            $resumen->observacion = $request->get('observacion');
            $resumen->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('primer_vencimiento'))), "Y-m-d");
            $resumen->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('segundo_vencimiento'))), "Y-m-d");
            $resumen->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $resumen->pagado = $request->get('pagado');
            $resumen->venta = 1;
            $resumen->cliente_id = $request->get('cliente_id') ? $request->get('cliente_id') : null;
            $resumen->save();


            foreach ($request->get('artid') as $i => $artid) {
                $linea = new Linea;
                $linea->monto = $montos[$i];
                $linea->descripcion = $detalles[$i];
                $linea->cantidad = $cantidades[$i];
                $linea->articulo_id = $artid;
                $linea->resumen_id = $resumen->id;
                $linea->save();
            }

            if ($request->get('pagado') == '1') {
                $pago = new Pago;
                $pago->monto_tarjeta = $request->get('monto_tarjeta');
                $pago->monto_efectivo = $request->get('monto_efectivo');
                $pago->monto_debito = $request->get('monto_debito');
                $pago->monto_cheque = $request->get('monto_cheque');
                $pago->observacion = $request->get('observaciones_pago');
                $pago->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha_pago'))), "Y-m-d");
                $pago->resumen_id = $resumen->id;
                $pago->save();
            }

            DB::commit();
            return $resumen->id;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar guardar el resumen, intente nuevamente y si el problema persiste contacte al desarrollador.']);
        }
    }

    //Devuelve los clientes que le falta resumen de ese periodo
    public function getSinResumen($params)
    {
        $resumenes = Resumen::select();
        foreach ($params as $column => $value) {
            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");
                $resumenes->whereBetween('fecha', [$fechas[0], $fechas[1]]);
            }
        }
        $resumenes->where('venta', '=', 0);
        $resumenes->where('tipo_transaccion', '=', 1);
        $results = $resumenes->get();

        $clientesYaGenerados = array();

        foreach ($results as $i => $resumen) {
            if ($resumen->cliente_id) {
                $clientesYaGenerados[] = $resumen->cliente_id;
            }
            unset($results[$i]);
        }

        $clientes = Cliente::select();
        $clientes->where('fecha_alta', '<=', $fechas[1]);
        $clientes->whereNotIn('id', $clientesYaGenerados);

        return $clientes->get();
    }

    public function generarResumen($cliente, $params, $primer_vencimiento, $segundo_vencimiento, $generados)
    {
        try {
            DB::beginTransaction();

            foreach ($params as $column => $value) {
                if ($column == 'fecha_ingreso' && $value != '') {
                    $fechas = explode('-', $value);

                    $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                    $fechas[1] = date_format($fechas[1], "Y-m-d");
                }
            }
            /*****************************************/

            /***************************
             * Obtengo los montos que le corresponden al mes por abono.
             */
            if ($cliente->fecha_alta <= date('Y-m-01',  strtotime($fechas[1]))) {
                //Si fue dado de alta antes del periodo se le cobra completo
                $montoAbono = $cliente->abono->price;
                $montoAbono_1_vencimiento = $cliente->abono->price_vencimiento_1;
                $montoAbono_2_vencimiento = $cliente->abono->price_vencimiento_2;
            } else {
                // Si fue dado de alta en el medio del periodo se le cobran los días
                $totalDias = date('t',  strtotime($fechas[1]));
                $diaDelMes = date('j',  strtotime($cliente->fecha_alta));
                $montoAbono = (($totalDias - $diaDelMes + 1) / $totalDias) * $cliente->abono->price;
                $montoAbono = round($montoAbono, 2);
                $montoAbono_1_vencimiento = $cliente->abono->price_vencimiento_1;
                $montoAbono_2_vencimiento = $cliente->abono->price_vencimiento_2;
            }

            /*****
             * Creo el nuevo resumen
             */
            $resumen = new Resumen;
            $resumen->monto = $montoAbono /* + $montoVentas*/;
            $resumen->monto_1_vencimento = $montoAbono + $montoAbono_1_vencimiento /* + $montoVentas*/;
            $resumen->monto_2_vencimento = $montoAbono + $montoAbono_2_vencimiento /* + $montoVentas*/;
            $resumen->observacion = /*implode(', ', $observacionesVentas)*/ '';
            $resumen->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($primer_vencimiento)), "Y-m-d");
            $resumen->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($segundo_vencimiento)), "Y-m-d");
            $resumen->fecha = $fechas[1];
            $resumen->pagado = 0;
            $resumen->venta = 0;
            $resumen->cliente_id = $cliente->id;
            $resumen->save();

            /**
             * Agrego la linea al resumen por abono mensual.
             */
            $lineaAbono = new Linea;
            $lineaAbono->monto = $montoAbono;
            $lineaAbono->monto_1_vencimento = $montoAbono + $montoAbono_1_vencimiento;
            $lineaAbono->monto_2_vencimento = $montoAbono + $montoAbono_2_vencimiento;
            $lineaAbono->descripcion = $cliente->abono->name;
            $lineaAbono->cantidad = 1;
            $lineaAbono->resumen_id = $resumen->id;
            $lineaAbono->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar generar un resumen, intente nuevamente y si el problema persiste contacte al desarrollador. Se han generado ' . $generados . ' resúmenes antes del error.']);
        }
    }

    public function search($params)
    {
        $clienteInputs = array('cuenta_abonado', 'titular_responsable');
        $resumenes = Resumen::select('resumenes.*')->where('tipo_transaccion','=',1);
        $resumenes->leftJoin('clientes as cli', 'resumenes.cliente_id', '=', 'cli.id');

        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id' || $column == 'pagado' || $column == 'venta') && $value != '') {
                $resumenes->where('resumenes.' . $column, '=', $value);
            }

            if ($column == 'fecha' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $resumenes->whereBetween('resumenes.fecha', [$fechas[0], $fechas[1]]);
            }

            if ($value != '' && in_array($column, $clienteInputs)) {
                $resumenes->where('cli.' . $column, 'LIKE', '%' . $value . '%');
            }
        }

        return $resumenes->orderBy('resumenes.fecha', 'DESC')->orderBy('cli.titular_responsable', 'ASC')->paginate(env('APP_PAGINATION'));
    }

    public function deleteResumen($id)
    {
        $resumen = Resumen::findOrFail($id);
        $resumen->delete();
    }

    public function find($id)
    {
        return Resumen::findOrFail($id);
    }

    public function reposImpresion($request)
    {
        $resumenes = Resumen::select('resumenes.*');
        $resumenes->leftJoin('clientes as cli', 'resumenes.cliente_id', '=', 'cli.id');
        $resumenes->where('tipo_transaccion','=',1);
        if ($request->get('con_iva')) {
            $resumenes->where('cli.abono_id', '=', 1);
        }
        if ($request->has('abono_id')) {
            $resumenes->where('cli.abono_id', '=',$request->get('abono_id'));
        }
        $desde = $request->get('anio') . '-' . $request->get('mes') . '-01';
        $hasta = date("Y-m-t", strtotime($desde));
        $resumenes->whereBetween('resumenes.fecha', [$desde, $hasta]);


        return $resumenes->orderBy('resumenes.fecha', 'DESC')->orderBy('cli.titular_responsable', 'ASC')->get();
    }
}
