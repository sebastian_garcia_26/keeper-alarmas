<?php 
namespace App\Repositories;

use App\Student;


class StudentRepository
{
	
	public function saveStudent($request, $id = null)
	{
		if($id){
			$student = $this->find($id);
		}else{
			$student = new Student;
		}

		$student->name = $request->get('name');
		$student->lastname = $request->get('lastname');
		$student->email = $request->get('email');
		$student->phone = $request->get('phone');
		$student->birthday = $request->get('birthday');
		$student->student_id = $request->get('student_id');
		

		if($request->get('delete-image') && $student->picture && App\File::exists('pictures/'.$student->picture)){
			App\File::delete('pictures/'.$student->picture);
			$student->picture = '';
		}

		$student->save();

		if( $request->hasFile('picture') ) {
			$file = $request->file('picture');
			$fileName = $student->id.".".$file->extension();
	        $file->move('pictures', $fileName);
	        $student->picture = $fileName;
	        $student->save();
	    }
	}

	public function search($params)
	{
		$students = Student::select();
		foreach($params as $column => $value)
		{
			if($value){
				$students->where($column, 'LIKE', '%'.$value.'%');
			}
		}
		return $students->paginate(env('APP_PAGINATION'));
	}

	public function deleteStudent($id)
	{
		$item = Student::findOrFail($id);
        $item->delete();
	}

	public function find($id){
		return Student::findOrFail($id);
	}
}