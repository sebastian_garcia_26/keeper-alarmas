<?php

namespace App\Repositories;

use App\Linea;
use App\Resumen;
use App\Venta;
use App\Serie;
use App\Compradore;
use App\Pago;
use Illuminate\Support\Facades\Input;
use DB;
use Exception;


class VentaRepository
{
    public function updateVenta($request)
    {
        try {
            DB::beginTransaction();
            $montoTotal = 0;
            $montos = $request->get('monto');
            $cantidades = $request->get('cantidad');
            $detalles = $request->get('detalle');
            foreach ($montos as $i => $monto) {
                $montoTotal += $monto;
            }

            $venta = $this->find($request->get('id'));
            $montoPagos = 0;
            foreach ($venta->pagos() as $pago) {
                $montoPagos += $pago->monto_efectivo + $pago->monto_debito + $pago->monto_tarjeta + $pago->monto_cheque;
            }


            $venta->monto = $montoTotal;
            $venta->observacion = $request->get('observacion');
            $venta->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('primer_vencimiento'))), "Y-m-d");
            $venta->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('segundo_vencimiento'))), "Y-m-d");
            $venta->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $venta->pagado = $montoPagos >= $montoTotal ? 1 : 0;
            $venta->compradore_id = $request->get('compradore_id') ? $request->get('compradore_id') : null;
            $venta->abono_id = $request->get('abono_id');
            $venta->save();

            $serieID = array();
            foreach ($venta->series() as $serie) {
                $serieID[] = $serie->id;
            }
            Serie::destroy($serieID);

            foreach ($request->get('artid') as $i => $artid) {
                $serie = new Serie;
                $serie->monto = $montos[$i];
                $serie->descripcion = $detalles[$i];
                $serie->cantidad = $cantidades[$i];
                $serie->articulo_id = $artid ? $artid : null;
                $serie->venta_id = $venta->id;
                $serie->save();
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar actualizar la venta, intente nuevamente y si el problema persiste contacte al desarrollador.']);
        }
    }

    public function saveVenta($request)
    {
        try {
            DB::beginTransaction();
            $montoTotal = 0;
            $montos = $request->get('monto');
            $cantidades = $request->get('cantidad');
            $detalles = $request->get('detalle');
            foreach ($montos as $i => $monto) {
                $montoTotal += $monto;
            }

//            $venta = new Venta;
//            $venta->monto = $montoTotal;
//            $venta->observacion = $request->get('observacion');
//            $venta->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
//            $venta->pagado = $request->get('pagado');
//            $venta->venta = 1;
//            $venta->compradore_id = $request->get('compradore_id') ? $request->get('compradore_id') : null;
//            $venta->cliente_id = $request->get('cliente_id') ? $request->get('cliente_id') : null;
//            $venta->save();

/*
            foreach ($request->get('artid') as $i => $artid) {
                $serie = new Serie;
                $serie->monto = $montos[$i];
                $serie->descripcion = $detalles[$i];
                $serie->cantidad = $cantidades[$i];
                $serie->articulo_id = $artid;
                $serie->venta_id = $venta->id;
                $serie->save();
            }

            if ($request->get('pagado') == '1') {
                $pago = new Pago;
                $pago->monto_tarjeta = $request->get('monto_tarjeta');
                $pago->monto_efectivo = $request->get('monto_efectivo');
                $pago->monto_debito = $request->get('monto_debito');
                $pago->monto_cheque = $request->get('monto_cheque');
                $pago->observacion = $request->get('observaciones_pago');
                $pago->fecha = date_format(\DateTime::createromFormat('d/m/Y', trim($request->get('fecha_pago'))), "Y-m-d");
                $pago->venta_id = $venta->id;
                $pago->save();
            }

            DB::commit();
            return $venta->id;*/
            $resumen = new Resumen;
            $resumen->monto = $montoTotal;
            $resumen->observacion = $request->get('observacion');
            $resumen->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $resumen->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $resumen->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha'))), "Y-m-d");;
            $resumen->pagado = $request->get('pagado');
            $resumen->venta = 1;
            $resumen->tipo_transaccion = 2;
            $resumen->cliente_id = $request->get('clientes_id') ? $request->get('clientes_id') : null;
            $resumen->comprador_id = $request->get('comprador_id') ? $request->get('comprador_id') : null;
            $resumen->save();


            foreach ($request->get('artid') as $i => $artid) {
                $linea = new Linea;
                $linea->monto = $montos[$i];
                $linea->descripcion = $detalles[$i];
                $linea->cantidad = $cantidades[$i];
                $linea->articulo_id = $artid;
                $linea->resumen_id = $resumen->id;
                $linea->save();
            }

            if ($request->get('pagado') == '1') {
                $pago = new Pago;
                $pago->monto_tarjeta = $request->get('monto_tarjeta');
                $pago->monto_efectivo = $request->get('monto_efectivo');
                $pago->monto_debito = $request->get('monto_debito');
                $pago->monto_cheque = $request->get('monto_cheque');
                $pago->observacion = $request->get('observaciones_pago');
                $pago->fecha = date_format(\DateTime::createFromFormat('d/m/Y', trim($request->get('fecha_pago'))), "Y-m-d");
                $pago->resumen_id = $resumen->id;
                $pago->save();
            }

            DB::commit();
            return $resumen->id;
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar guardar la venta, intente nuevamente y si el problema persiste contacte al desarrollador.']);
        }
    }

    //Devuelve los clientes que le falta ventas de ese periodo
    public function getSinVenta($params)
    {
        $ventas = Venta::select();
        foreach ($params as $column => $value) {
            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $ventas->whereBetween('fecha', [$fechas[0], $fechas[1]]);
            }
        }
        $ventas->where('venta', '=', 0);
        $results = $ventas->get();

        $compradoresYaGenerados = array();

        foreach ($results as $i => $venta) {
            if ($venta->compradore_id) {
                $compradoresYaGenerados[] = $venta->compradore_id;
            }
            unset($results[$i]);
        }

        $compradores = Compradore::select();
        $compradores->where('fecha_alta', '<=', $fechas[1]);
        $compradores->whereNotIn('id', $compradoresYaGenerados);

        return $compradores->get();
    }

    public function generarVenta($cliente, $params, $primer_vencimiento, $segundo_vencimiento, $generados)
    {
        try {
            DB::beginTransaction();

            foreach ($params as $column => $value) {
                if ($column == 'fecha_ingreso' && $value != '') {
                    $fechas = explode('-', $value);

                    $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                    $fechas[1] = date_format($fechas[1], "Y-m-d");
                }
            }
            /*****************************************/

            /***************************
             * Obtengo los montos que le corresponden al mes por abono.
             */
            if ($cliente->fecha_alta <= date('Y-m-01',  strtotime($fechas[1]))) { //Si fue dado de alta antes del periodo se le cobra completo
                $montoAbono = $cliente->abono->price;
            } else { // Si fue dado de alta en el medio del periodo se le cobran los días
                $totalDias = date('t',  strtotime($fechas[1]));
                $diaDelMes = date('j',  strtotime($cliente->fecha_alta));
                $montoAbono = (($totalDias - $diaDelMes + 1) / $totalDias) * $cliente->abono->price;
                $montoAbono = round($montoAbono, 2);
            }

            /*****
             * Creo el nuevo venta
             */
            $venta = new Venta;
            $venta->monto = $montoAbono/* + $montoVentas*/;
            $venta->observacion = /*implode(', ', $observacionesVentas)*/ '';
            $venta->primer_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($primer_vencimiento)), "Y-m-d");
            $venta->segundo_vencimiento = date_format(\DateTime::createFromFormat('d/m/Y', trim($segundo_vencimiento)), "Y-m-d");
            $venta->fecha = $fechas[1];
            $venta->pagado = 0;
            $venta->venta = 0;
            $venta->compradore_id = $cliente->id;
            $venta->save();

            /**
             * Agrego la serie al venta por abono mensual.
             */
            $serieAbono = new Serie;
            $serieAbono->monto = $montoAbono;
            $serieAbono->descripcion = $cliente->abono->name;
            $serieAbono->cantidad = 1;
            $serieAbono->venta_id = $venta->id;
            $serieAbono->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['Hubo un error inesperado al intentar generar la venta, intente nuevamente y si el problema persiste contacte al desarrollador. Se han generado ' . $generados . ' resúmenes antes del error.']);
        }
    }

    public function search($params)
    {
        /*
        $clienteInputs = array('cuenta_abonado', 'titular_responsable');
        $ventas = Venta::select('ventas.*');
        $ventas->leftJoin('compradores as cli', 'ventas.compradore_id', '=', 'cli.id');
        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id' || $column == 'pagado' || $column == 'venta') && $value != '') {
                $ventas->where('ventas.' . $column, '=', $value);
            }

            if ($column == 'fecha' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $ventas->whereBetween('ventas.fecha', [$fechas[0], $fechas[1]]);
            }

            if ($value != '' && in_array($column, $clienteInputs)) {
                $ventas->where('cli.' . $column, 'LIKE', '%' . $value . '%');
            }
        }

        return $ventas->orderBy('ventas.fecha', 'DESC')->orderBy('cli.titular_responsable', 'ASC')->paginate(env('APP_PAGINATION'));
   */

        $clienteInputs = array('cuenta_abonado', 'titular_responsable');
        $resumenes = Resumen::select('resumenes.*','com.cuenta_abonado as cuenta_abo_com','cli.cuenta_abonado as cuenta_abo_cli','art.name','cli.cuenta_abonado as cli_abo','cli.titular_responsable as cli_titu','com.cuenta_abonado as com_abo','com.titular_responsable as com_titu')
            ->where('tipo_transaccion','=',2);
        $resumenes->leftJoin('clientes as cli', 'resumenes.cliente_id', '=', 'cli.id');
        $resumenes->leftJoin('compradores as com', 'resumenes.comprador_id', '=', 'com.id');
        $resumenes->leftJoin('lineas as lineas', 'resumenes.id', '=', 'lineas.resumen_id');
        $resumenes->leftJoin('articulos as art', 'art.id', '=', 'lineas.articulo_id');

        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id' || $column == 'pagado' || $column == 'venta') && $value != '') {
                $resumenes->where('resumenes.' . $column, '=', $value);
            }

            if ($column == 'fecha' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $resumenes->whereBetween('resumenes.fecha', [$fechas[0], $fechas[1]]);
            }

            if ($value != '' && in_array($column, $clienteInputs)) {
               // $resumenes->where('cli.' . $column, 'LIKE', '%' . $value . '%');
            }
        }

        return $resumenes->orderBy('resumenes.fecha', 'DESC')->paginate(env('APP_PAGINATION'));
    }

    public function deleteVenta($id)
    {
        Pago::where('venta_id', '=', $id)->delete();
        $venta = Venta::findOrFail($id);
        $venta->delete();
    }

    public function find($id)
    {
        return Venta::findOrFail($id);
    }

    public function reposImpresion($request)
    {
        $ventas = Venta::select('ventas.*');
        $ventas->leftJoin('compradores as cli', 'ventas.compradore_id', '=', 'cli.id');
        if ($request->get('con_iva')) {
            $ventas->where('cli.abono_id', '=', 1);
        } else {
            $ventas->where('ventas.venta', '=', 0);
            $ventas->where('cli.abono_id', '!=', 1);
        }
        $desde = $request->get('anio') . '-' . $request->get('mes') . '-01';
        $hasta = date("Y-m-t", strtotime($desde));
        $ventas->whereBetween('ventas.fecha', [$desde, $hasta]);


        return $ventas->orderBy('ventas.fecha', 'DESC')->orderBy('cli.titular_responsable', 'ASC')->get();
    }
}
