<?php

namespace App\Repositories;

use App\Compradore;
use Illuminate\Support\Facades\Input;


class CompradoreRepository
{

    public function saveCompradore($request, $id = null)
    {
        if ($id) {
            $compradore = $this->find($id);
        } else {
            $compradore = new Compradore;
        }

        $compradore->cuenta_abonado = $request->get('cuenta_abonado');
        $compradore->nombre_razon_social = $request->get('nombre_razon_social');
        $compradore->titular_responsable = $request->get('titular_responsable');
        $compradore->calle = $request->get('calle');
        $compradore->numero = $request->get('numero');
        $compradore->piso = $request->get('piso');
        $compradore->depto = $request->get('depto');
        $compradore->resto = $request->get('resto');
        $compradore->codigo_postal = $request->get('codigo_postal');
        $compradore->telefono = $request->get('telefono');
        $compradore->celular = $request->get('celular');
        $compradore->email = $request->get('email');
        $compradore->documento = $request->get('documento');
        $compradore->ciudad = $request->get('ciudad');
        $compradore->provincia = $request->get('provincia');
        $compradore->cuit = $request->get('cuit');

        $fecha_ingreso = \DateTime::createFromFormat('d/m/Y', Input::get('fecha_ingreso'));
        $fecha_ingreso = date_format($fecha_ingreso, "Y-m-d H:i:s");
        $compradore->fecha_alta = $fecha_ingreso;

        $compradore->abono_id = $request->get('abono_id');
        $compradore->save();
    }

    public function search($params)
    {
        $compradores = Compradore::select()->orderBy('fecha_alta', 'desc');
        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id') && $value != '') {
                $compradores->where($column, '=', $value);
            }

            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $compradores->whereBetween('fecha_alta', [$fechas[0], $fechas[1]]);
            }

            if ($value && $column != 'page' && $column != 'id' && $column != 'abono_id' && $column != 'fecha_ingreso') {
                $compradores->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $compradores->paginate(env('APP_PAGINATION'));
    }

    public function get($params)
    {
        $compradores = Compradore::select()->orderBy('fecha_alta', 'desc');
        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id') && $value != '') {
                $compradores->where($column, '=', $value);
            }

            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $compradores->whereBetween('fecha_alta', [$fechas[0], $fechas[1]]);
            }

            if ($value && $column != 'page' && $column != 'id' && $column != 'abono_id' && $column != 'fecha_ingreso') {
                $compradores->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $compradores->orderBy('titular_responsable', 'ASC')->get();
    }

    public function deleteCompradore($id)
    {
        $item = Compradore::findOrFail($id);
        $item->delete();
    }

    public function find($id)
    {
        return Compradore::findOrFail($id);
    }
}
