<?php

namespace App\Repositories;

use App\Pago;
use App\Cliente;
use App\Resumen;
use DB;

class PagoRepository
{

    public function savePago($request, $id = null)
    {
        if ($id) {
            $pago = $this->find($id);
        } else {
            $pago = new Pago;
        }

        $pago->name = $request->get('name');
        $pago->price = $request->get('price');
        $pago->iva = $request->get('iva');

        $pago->save();
    }

    public function search($params)
    {
        $pagos = Pago::select()->orderBy('fecha', 'desc');
        foreach ($params as $column => $value) {
            if ($value && $column != 'page') {
                $pagos->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $pagos->paginate(env('APP_PAGINATION'));
    }

    /*public function get($params){
            $pagos = Pago::select();
            foreach ($params as $column => $value) {
                if ($value && $column == 'id') {
                    $pagos->where($column, '=', $value );
                }elseif ($value && $column != 'page') {
                    $pagos->where($column, 'LIKE', '%' . $value . '%');
                }
            }
            return $pagos->get();
        }*/

    public function deletePago($id)
    {
        try {
            DB::beginTransaction();
            $pago = Pago::findOrFail($id);
            $pago->delete();

            $resumen = \App\Resumen::findOrFail($pago->resumen_id);
            $montoPagos = 0;
            foreach ($resumen->pagos() as $pagor) {
                $montoPagos += $pagor->monto_efectivo + $pagor->monto_debito + $pagor->monto_tarjeta + $pagor->monto_cheque;
            }

            $montoLineas = 0;
            foreach ($resumen->lineas() as $linea) {
                $montoLineas += $linea->monto;
            }

            if ($montoLineas > $montoPagos) {
                $resumen->pagado = 0;
                $resumen->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    public function find($id)
    {
        return Pago::findOrFail($id);
    }

    public function searchIndex($params)
    {
        $pagos = Pago::select('pagos.*')->orderBy('fecha', 'desc');

        // var_dump(count($params));

        if (count($params) >= 2) {
            $pagos->leftJoin('resumenes as res', 'res.id', '=', 'pagos.resumen_id');
            $pagos->where('res.cliente_id', '!=', null);

            if ($params['cuenta_abonado'] || $params['titular_responsable']) {
                $pagos->leftJoin('clientes as cli', 'cli.id', '=', 'res.cliente_id');
                if ($params['cuenta_abonado']) {
                    $pagos->where('cli.cuenta_abonado', 'LIKE', '%' . $params['cuenta_abonado'] . '%');
                }

                if ($params['titular_responsable']) {
                    $pagos->where('cli.titular_responsable', 'LIKE', '%' . $params['titular_responsable'] . '%');
                }
            }

            if ($params['id']) {
                $pagos->where('pagos.resumen_id', '=', $params['id']);
            }

            if ($params['fecha']) {
                $fechas = explode('-', $params['fecha']);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $pagos->whereBetween('pagos.fecha', [$fechas[0], $fechas[1]]);
            }
        } else {
            $pagos->leftJoin('resumenes as res', 'res.id', '=', 'pagos.resumen_id');
            $pagos->where('res.cliente_id', '!=', null);
        }
        $pagos->where('res.tipo_transaccion', '=', 1);
        return $pagos->paginate(env('APP_PAGINATION'));
    }

    public function get($params)
    {
        $pagos = Pago::select('pagos.*')->orderBy('fecha', 'desc');

        if (count($params) >= 2) {
            $pagos->leftJoin('resumenes as res', 'res.id', '=', 'pagos.resumen_id');

            if ($params['cuenta_abonado'] || $params['titular_responsable']) {
                $pagos->leftJoin('clientes as cli', 'cli.id', '=', 'res.cliente_id');
                if ($params['cuenta_abonado']) {
                    $pagos->where('cli.cuenta_abonado', 'LIKE', '%' . $params['cuenta_abonado'] . '%');
                }

                if ($params['titular_responsable']) {
                    $pagos->where('cli.titular_responsable', 'LIKE', '%' . $params['titular_responsable'] . '%');
                }
            }

            if ($params['id']) {
                $pagos->where('pagos.resumen_id', '=', $params['id']);
            }

            if ($params['fecha']) {
                $fechas = explode('-', $params['fecha']);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $pagos->whereBetween('pagos.fecha', [$fechas[0], $fechas[1]]);
            }
        }

        return $pagos->get();
    }


    public function searchDeudores($params)
    {
        $cuenta = '%%';
        $titular = '%%';
        $filter_date = '';

        if (count($params)) {
            if ($params['cuenta_abonado']) {
                $cuenta = '%' . $params['cuenta_abonado'] . '%';
            }

            if ($params['titular_responsable']) {
                $titular = '%' . $params['titular_responsable'] . '%';
            }
            // if ($params['anio'] && $params['mes']) {
            //     $filter_date = 'YEAR(res.fecha)='.$params['anio'].' AND MONTH(res.fecha)='.$params['mes'].' AND';
            // }

            // var_dump( $filter_date);
        }

        return \DB::select('SELECT cli.*,count(aux.res_id) as meses_debe, SUM(aux.pagado) as pagado, SUM(aux.monto) as a_pagar '
            . 'FROM clientes cli INNER JOIN '
            . '      (SELECT res.id as res_id,res.cliente_id, '
            . '       SUM(IF( pa.deleted_at IS NULL, pa.monto_tarjeta+pa.monto_efectivo+pa.monto_debito+pa.monto_cheque, 0)) as pagado, res.monto '
            . '       FROM resumenes res LEFT JOIN pagos pa ON (pa.resumen_id = res.id) '
            . '       WHERE ' . $filter_date . '  res.deleted_at IS NULL AND res.tipo_transaccion=1 and pagado = 0 and res.cliente_id IS NOT NULL GROUP BY res.id) as aux '
            . 'ON (cli.id = aux.cliente_id) '
            . 'WHERE cli.deleted_at IS NULL AND cli.cuenta_abonado LIKE ? AND cli.titular_responsable LIKE ? GROUP BY cli.id ORDER BY id DESC', [$cuenta, $titular]);
    }

    public function searchDeudoresProductos($params)
    {
        $cuenta = '%%';
        $titular = '%%';
        $filter_date = '';
        if (count($params)) {
            if ($params['cuenta_abonado']) {
                $cuenta = '%' . $params['cuenta_abonado'] . '%';
            }

            if ($params['titular_responsable']) {
                $titular = '%' . $params['titular_responsable'] . '%';
            }
            // if ($params['anio'] && $params['mes']) {
            //     $filter_date = 'YEAR(res.fecha)='.$params['anio'].' AND MONTH(res.fecha)='.$params['mes'].' AND';
            // }
        }

        return \DB::select('SELECT cli.*,count(aux.res_id) as meses_debe, SUM(aux.pagado) as pagado, SUM(aux.monto) as a_pagar '
            . 'FROM clientes cli INNER JOIN '
            . '      (SELECT res.id as res_id,res.cliente_id, '
            . '       SUM(IF( pa.deleted_at IS NULL, pa.monto_tarjeta+pa.monto_efectivo+pa.monto_debito+pa.monto_cheque, 0)) as pagado, res.monto '
            . '       FROM resumenes res LEFT JOIN pagos pa ON (pa.resumen_id = res.id) '
            . '       WHERE ' . $filter_date . '  res.deleted_at IS NULL AND res.tipo_transaccion=2 and pagado = 0 and res.cliente_id IS NOT NULL GROUP BY res.id) as aux '
            . 'ON (cli.id = aux.cliente_id) '
            . 'WHERE cli.deleted_at IS NULL AND cli.cuenta_abonado LIKE ? AND cli.titular_responsable LIKE ? GROUP BY cli.id ORDER BY id DESC', [$cuenta, $titular]);
    }
}
