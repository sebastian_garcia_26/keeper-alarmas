<?php

namespace App\Repositories;

use App\Cliente;
use Illuminate\Support\Facades\Input;


class ClienteRepository
{

    public function saveCliente($request, $id = null)
    {
        if ($id) {
            $cliente = $this->find($id);
        } else {
            $cliente = new Cliente;
        }

        $cliente->cuenta_abonado = $request->get('cuenta_abonado');
        $cliente->nombre_razon_social = $request->get('nombre_razon_social');
        $cliente->titular_responsable = $request->get('titular_responsable');
        $cliente->calle = $request->get('calle');
        $cliente->numero = $request->get('numero');
        $cliente->piso = $request->get('piso');
        $cliente->depto = $request->get('depto');
        $cliente->resto = $request->get('resto');
        $cliente->codigo_postal = $request->get('codigo_postal');
        $cliente->telefono = $request->get('telefono');
        $cliente->celular = $request->get('celular');
        $cliente->email = $request->get('email');
        $cliente->documento = $request->get('documento');
        $cliente->ciudad = $request->get('ciudad');
        $cliente->provincia = $request->get('provincia');
        $cliente->cuit = $request->get('cuit');

        $fecha_ingreso = \DateTime::createFromFormat('d/m/Y', Input::get('fecha_ingreso'));
        $fecha_ingreso = date_format($fecha_ingreso, "Y-m-d H:i:s");
        $cliente->fecha_alta = $fecha_ingreso;

        $cliente->abono_id = $request->get('abono_id');
        $cliente->save();
    }

    public function search($params)
    {
        $clientes = Cliente::select()->orderBy('titular_responsable', 'asc');
        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id') && $value != '') {
                $clientes->where($column, '=', $value);
            }

            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $clientes->whereBetween('fecha_alta', [$fechas[0], $fechas[1]]);
            }

            if ($value && $column != 'page' && $column != 'id' && $column != 'abono_id' && $column != 'fecha_ingreso') {
                $clientes->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $clientes->paginate(env('APP_PAGINATION'));
    }

    public function get($params)
    {
        $clientes = Cliente::select()->orderBy('fecha_alta', 'desc');
        foreach ($params as $column => $value) {
            if (($column == 'id' || $column == 'abono_id') && $value != '') {
                $clientes->where($column, '=', $value);
            }

            if ($column == 'fecha_ingreso' && $value != '') {
                $fechas = explode('-', $value);
                $fechas[0] = \DateTime::createFromFormat('d/m/Y', trim($fechas[0]));
                $fechas[0] = date_format($fechas[0], "Y-m-d");

                $fechas[1] = \DateTime::createFromFormat('d/m/Y', trim($fechas[1]));
                $fechas[1] = date_format($fechas[1], "Y-m-d");

                $clientes->whereBetween('fecha_alta', [$fechas[0], $fechas[1]]);
            }

            if ($value && $column != 'page' && $column != 'id' && $column != 'abono_id' && $column != 'fecha_ingreso') {
                $clientes->where($column, 'LIKE', '%' . $value . '%');
            }
        }
        return $clientes->orderBy('titular_responsable', 'ASC')->get();
    }

    public function deleteCliente($id)
    {
        $item = Cliente::findOrFail($id);
        $item->delete();
    }

    public function find($id)
    {
        return Cliente::findOrFail($id);
    }
}
