<?php 
namespace App\Repositories;

use App\Abono;


class AbonoRepository
{
	
	public function saveAbono($request, $id = null)
	{
		if($id){
			$abono = $this->find($id);
		}else{
			$abono = new Abono;
		}

		$abono->name = $request->get('name');
		$abono->price = $request->get('price');
		$abono->price_vencimiento_1 = $request->get('price_vencimiento_1');
		$abono->price_vencimiento_2 = $request->get('price_vencimiento_2');
		$abono->iva = $request->get('iva');

		$abono->save();
	}

	public function search($params)
	{
		$abonos = Abono::select();
		foreach($params as $column => $value)
		{
			if($value && $column != 'page'){
				$abonos->where($column, 'LIKE', '%'.$value.'%');
			}
		}
		return $abonos->paginate(env('APP_PAGINATION'));
	}
        
        public function get($params){
            $abonos = Abono::select();
            foreach ($params as $column => $value) {
                if ($value && $column == 'id') {
                    $abonos->where($column, '=', $value );
                }elseif ($value && $column != 'page') {
                    $abonos->where($column, 'LIKE', '%' . $value . '%');
                }
            }
            return $abonos->get();
        }

	public function deleteAbono($id)
	{
            $item = Abono::findOrFail($id);
            $item->delete();
	}

	public function find($id){
		return Abono::findOrFail($id);
	}
}