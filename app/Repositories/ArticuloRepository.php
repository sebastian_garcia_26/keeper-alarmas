<?php 
namespace App\Repositories;

use App\Articulo;


class ArticuloRepository
{
	
	public function saveArticulo($request, $id = null)
	{
		if($id){
			$articulo = $this->find($id);
		}else{
			$articulo = new Articulo;
		}

		$articulo->name = $request->get('name');
		$articulo->code = $request->get('code');
		$articulo->price = $request->get('price');
		$articulo->iva = $request->get('iva');

		$articulo->save();
	}

	public function search($params)
	{
		$articulos = Articulo::select();
		foreach($params as $column => $value)
		{
			if($value && $column != 'page'){
				$articulos->where($column, 'LIKE', '%'.$value.'%');
			}
		}
		return $articulos->paginate(env('APP_PAGINATION'));
	}

        public function get($params)
	{
		$articulos = Articulo::select();
		foreach($params as $column => $value)
		{
			if($value && $column != 'page'){
				$articulos->where($column, 'LIKE', '%'.$value.'%');
			}
		}
		return $articulos->orderBy('name','ASC')->get();
	}
        
	public function deleteArticulo($id)
	{
            $item = Articulo::findOrFail($id);
            $item->delete();
	}

	public function find($id){
		return Articulo::findOrFail($id);
	}
}