<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
//if(date('Y-m-d') <= '2018-02-20'){

Route::resource('students', 'StudentController');

//ABONOS
Route::resource('abonos', 'AbonoController');

//ARTICULOS
Route::resource('articulos', 'ArticuloController');

//EXPORTAR
Route::get('clientes/exportar-excel', 'ExcelController@exportarClientes');
Route::get('clientes/exportar-pdf', 'PDFController@exportarClientes');

Route::get('compradores/exportar-excel', 'ExcelController@exportarCompradores');
Route::get('compradores/exportar-pdf', 'PDFController@exportarCompradores');

Route::get('pagos/exportar-excel', 'ExcelController@exportarPagos');
Route::get('pagos/exportar-pdf', 'PDFController@exportarPagos');

Route::get('deudores/exportar-excel', 'ExcelController@exportarDeudores');
Route::get('deudores/exportar-pdf', 'PDFController@exportarDeudores');

Route::get('deudores/exportar-excel-productos', 'ExcelController@exportarDeudoresProductos');
Route::get('deudores/exportar-pdf-productos', 'PDFController@exportarDeudoresProductos');

Route::get('cobros/exportar-excel', 'ExcelController@exportarPagos');
Route::get('cobros/exportar-pdf', 'PDFController@exportarPagos');

//CLIENTES
Route::resource('clientes', 'ClienteController');
Route::resource('clientes-ventas', 'ClienteVentasController');
Route::resource('compradores', 'CompradoreController');


//Resumenes
Route::get('resumenes/imprimir', 'ResumenController@imprimirResumenes');
Route::post('resumenes/imprimir', 'ResumenController@imprimirResumenesPOST');
Route::post('resumenes/imprimir-preview', 'ResumenController@imprimirResumenesPreview');
Route::post('resumenAjax/get/{id}/{fechaquepaga}',  'ResumenController@getAjax');
Route::post('resumenAjaxVenta/get/{id}',  'ResumenController@getAjaxVenta');
Route::get('resumenes/{id}/pdf', 'ResumenController@generarPDF');
Route::get('resumenes/{id}/imprimir', 'ResumenController@generarImprimir');
Route::get('resumenes/generar', 'ResumenController@generarMostrarFormulario');
Route::post('resumenes/generar', 'ResumenController@generarResumenes');
Route::resource('resumenes', 'ResumenController');

//VentasProductos
Route::get('ventas/imprimir', 'VentaController@imprimirVentas');
Route::post('ventas/imprimir', 'VentaController@imprimirVentasPOST');
Route::post('ventas/imprimir-preview', 'VentaController@imprimirVentasPreview');
Route::post('ventaAjax/get/{id}',  'VentaController@getAjax');
Route::get('ventas/{id}/pdf', 'VentaController@generarPDF');
Route::get('ventas/{id}/imprimir', 'VentaController@generarImprimir');
Route::get('ventas/generar', 'VentaController@generarMostrarFormulario');
Route::post('ventas/generar', 'VentaController@generarVentas');
Route::resource('ventas', 'VentaController');

//Pagos
Route::get('pagos-deudores', 'PagoController@indexDeudores');
Route::resource('pagos', 'PagoController');

Route::get('cobros-deudores', 'CobroController@indexDeudores');
Route::resource('cobros', 'CobroController');


/*}else{
    die(view('home-trial'));
}*/
